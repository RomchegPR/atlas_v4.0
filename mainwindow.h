#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "AtlasSystem.h"
#include <QTreeWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();
    void CreatingScene();

    void closeEvent(QCloseEvent *event);


private:
    Ui::MainWindow *ui;
    AtlasSystem *m_AtlasSystem;

    QTreeWidget *m_treeWidget_AllObjects;
    QTreeWidget *m_treeWidget_CurrentView;
    QTreeWidget *m_treeWidget_Main;

    QString mStyleQSS;

};

#endif // MAINWINDOW_H
