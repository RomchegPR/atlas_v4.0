#include "AtlasSystem.h"
#include "QFileDialog"

AtlasSystem::AtlasSystem()
{

}

void AtlasSystem::start()
{
    m_TreeWidgetManager.start();
    m_DataStorage = DataStorage_test::getInstance();

    m_Render.setDataStorage(m_DataStorage);
}

void AtlasSystem::end()
{
    m_Render.stop();
    m_TreeWidgetManager.stop();
    m_DataStorageLoader.stop();
    m_DataStorage->stop();
    m_Render.wait();
    m_TreeWidgetManager.wait();
    m_DataStorageLoader.wait();
    m_DataStorage->wait();
}

void AtlasSystem::startRender()
{
    m_Render.start();
}

void AtlasSystem::stopRender()
{
    m_Render.stop();
}

void AtlasSystem::startDataStorage()
{
    m_DataStorageLoader.setDataStorage(m_DataStorage);
    m_DataStorageLoader.start();

    m_DataStorage->start();
}

void AtlasSystem::TreeWidgetViewSave()
{
    QString fileName = QFileDialog::getOpenFileName(0, "Сохранение режима", "", "");

    if (fileName != ""){

        m_TreeWidgetManager.setFileName(fileName);

        m_TreeWidgetManager.setMode(TreeWidgetManager::SAVE);
        m_TreeWidgetManager.start();

    }
}

void AtlasSystem::TreeWidgetViewLoad()
{
    QString fileName = QFileDialog::getOpenFileName(0, "Выбор режима", "", "");

    if (fileName != ""){

        m_TreeWidgetManager.setFileName(fileName);

        m_TreeWidgetManager.setMode(TreeWidgetManager::LOAD);
        m_TreeWidgetManager.start();

    }
}

void AtlasSystem::TreeWidgetViewUseSettings()
{
    m_TreeWidgetManager.useViewSettings();
}

void AtlasSystem::setTreeViewWidget_AllObjects(QTreeWidget * widget)
{
    m_TreeWidgetManager.setTreeWidgetAllObjects(widget);
//    m_TreeWidgetManager.setFileName(QFileDialog::getOpenFileName(0, "Выбор режима", "", ""));
}

void AtlasSystem::setTreeViewWidget_CurrentView(QTreeWidget *widget)
{
    m_TreeWidgetManager.setTreeWidgetCurrentView(widget);
}

void AtlasSystem::setTreeViewWidget_MainView(QTreeWidget *widget)
{
    m_TreeWidgetManager.setTreeWidgetMainView(widget);
}


