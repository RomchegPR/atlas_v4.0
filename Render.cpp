#include "Render.h"
#include <QDebug>
#include "DataStorage_test.h"

Render::Render()
{
    stopped = false;
}

void Render::run()
{
    while (!stopped){
        for (RenderObject* object : m_pDataStorage->getAllObjects()){
            object->setState(RenderObjectState::STATE_PAINTING);
        }
    }

  stopped = false;

}

void Render::stop()
{
    qDebug() << "stop Render";

    stopped = true;

    for (RenderObject* object : m_pDataStorage->getAllObjects()){
        object->setState(RenderObjectState::STATE_PAINT_READY);
    }
}

void Render::setDataStorage(DataStorage_test *pDataStorage)
{
    m_pDataStorage = pDataStorage;
}
