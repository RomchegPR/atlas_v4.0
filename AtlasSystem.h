#ifndef GRAPHICENGINE_H
#define GRAPHICENGINE_H

#include "Render.h"
#include "TreeWidgetManager.h"
#include "DataStorageLoader.h"
#include "DataStorage_test.h"
#include <QObject>

class AtlasSystem : public QObject
{
    Q_OBJECT
public:
    AtlasSystem();
    void start();
    void end();

    void setTreeViewWidget_AllObjects   (QTreeWidget * widget);
    void setTreeViewWidget_CurrentView  (QTreeWidget * widget);
    void setTreeViewWidget_MainView     (QTreeWidget * widget);

public slots:
    void startRender();
    void stopRender();

    void startDataStorage();

    void TreeWidgetViewSave();
    void TreeWidgetViewLoad();
    void TreeWidgetViewUseSettings();


private:
    Render             m_Render;
    DataStorage_test   *m_DataStorage;
    DataStorageLoader  m_DataStorageLoader;
    TreeWidgetManager  m_TreeWidgetManager;

};

#endif // GRAPHICENGINE_H
