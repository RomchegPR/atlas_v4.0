#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include <QObject>
#define GLEW_STATIC
#include <GL/glew.h>

#include "States/RenderObjectState.h"
#include "States/FlickerState.h"
#include "States/CreatingState.h"
#include "States/InitializationState.h"
#include "States/PaintedState.h"
#include "States/ShinedState.h"
#include "States/ReadyState.h"

class RenderObject
{
//    static FlickerState     s_FlickerState;
//    static NotInitState     s_NotInitState;
//    static NotPaintedState  s_NotPaintedState;
//    static PaintedState     s_PaintedState;
//    static ShinedState      s_ShinedState;

public:
    RenderObject();

    void init();
    void paint();

    void setState(RenderObjectState::State state);

private:
    RenderObjectState* m_State;

    GLuint VBO, VAO;
    
    GLfloat m_Vertices[9];

};

#endif // RENDEROBJECT_H
