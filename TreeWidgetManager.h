#ifndef TREEWIDGETMANAGER_H
#define TREEWIDGETMANAGER_H

#include <QObject>
#include <QThread>
#include <QTreeWidget>

class TreeWidgetManager : public QThread
{
    Q_OBJECT
public:
    TreeWidgetManager();

    enum Mode{
        LOAD,
        SAVE
    };

    void run();
    void stop();

    void setFileName(QString fileName);
    void setMode(Mode mode);

    void useViewSettings();

    void setTreeWidgetCurrentView(QTreeWidget *pTreeWidgetCurrentView);
    void setTreeWidgetAllObjects(QTreeWidget *pTreeWidgetAllObjects);
    void setTreeWidgetMainView(QTreeWidget *pTreeWidgetMainView);



private:

    QString m_FileName;
    volatile bool stopped;
    Mode m_isLoadOperation;

    QTreeWidget *m_pTreeWidgetCurrentView;
    QTreeWidget *m_pTreeWidgetAllObjects;
    QTreeWidget *m_pTreeWidgetMainView;
};

#endif // TREEWIDGETMANAGER_H
