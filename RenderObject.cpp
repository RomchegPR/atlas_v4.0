#include "RenderObject.h"

#include <QDebug>
#include <iostream>
#include <time.h>

RenderObject::RenderObject()
{
    m_State = new CreatingState();

    srand ( time(NULL) );
    int rand = -5 + std::rand() % 10;
    float scale = (1 + std::rand() % 10) / 10.0f;

    m_Vertices[0] = (-0.5f + rand / 10.0f) * scale;
    m_Vertices[1] = (-0.5f + rand / 10.0f) * scale;
    m_Vertices[2] = (0.0f + rand / 10.0f) * scale; // Left

    m_Vertices[3] = (0.5f + rand / 10.0f) * scale;
    m_Vertices[4] = (-0.5f + rand / 10.0f) * scale;
    m_Vertices[5] = (0.0f + rand / 10.0f) * scale; // Right

    m_Vertices[6] = (0.0f + rand / 10.0f) * scale;
    m_Vertices[7] = (0.5f + rand / 10.0f) * scale;
    m_Vertices[8] = (0.0f + rand / 10.0f) * scale;  // Top

}

//function for initialization all vertexes of object can exec too long
void RenderObject::init()
{
    glewExperimental = GL_TRUE;

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cerr << glewGetErrorString(err) << std::endl;
    }
    
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // Bind the Vertex Array Object first, then bind and set vertex buffer(s) and attribute pointer(s).
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(m_Vertices), m_Vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0); // Note that this is allowed, the call to glVertexAttribPointer registered VBO as the currently bound vertex buffer object so afterwards we can safely unbind

    glBindVertexArray(0); // Unbind VAO (it's always a good thing to unbind any buffer/array to prevent strange bugs)
    
    
    qDebug() << VAO;
}

void RenderObject::paint()
{
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);
}

void RenderObject:: setState(RenderObjectState::State state_)
{
    RenderObjectState* state = m_State->handleInput(state_);
    if (state != nullptr) {
       delete m_State;
       m_State = state;

       // Вызов входного действия нового состояния.
       m_State->exec(this);
     }
}

