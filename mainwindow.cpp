#include "MainScene/Scene.h"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCloseEvent>
#include <DataStorage_test.h>
#include <TreeWidget/TreeWidgetView.h>
#include <QDebug>

#include "MainScene/Scene.h"
#include <QGraphicsScene>
#include <QGLWidget>
#include <QGraphicsItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_AtlasSystem = new AtlasSystem();

    m_treeWidget_AllObjects = new TreeWidgetView();
    m_treeWidget_AllObjects->resize(200, 200);
    m_treeWidget_AllObjects->setHeaderLabel("Все объекты");

    m_treeWidget_CurrentView = new TreeWidgetView();
    m_treeWidget_CurrentView->resize(200, 200);
    m_treeWidget_CurrentView->setHeaderLabel("Текущее отображение");

    m_treeWidget_Main = new QTreeWidget();
    m_treeWidget_Main->resize(200, 200);
    m_treeWidget_Main->setHeaderLabel("Основное меню");

    QFile widgetStyle;
    widgetStyle.setFileName("C:/Users/pra/BitBucket/PRA/Atlas_PRA/res/QSS/style.qss");
    widgetStyle.open(QFile::ReadOnly);
    mStyleQSS = widgetStyle.readAll();

    m_treeWidget_Main->setStyleSheet(mStyleQSS);
    m_treeWidget_AllObjects->setStyleSheet(mStyleQSS);
    m_treeWidget_CurrentView->setStyleSheet(mStyleQSS);

    this->setStyleSheet(mStyleQSS);

    connect(ui->pushButton_startDraw, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(startRender()));
    connect(ui->pushButton_stopDraw, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(stopRender()));

    connect(ui->pushButton_loadTreeView, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(TreeWidgetViewLoad()));
    connect(ui->pushButton_saveTreeView, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(TreeWidgetViewSave()));
    connect(ui->pushButton_useTreeViewSettings, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(TreeWidgetViewUseSettings()));

    connect(ui->pushButton_addDataStorage, SIGNAL(clicked()),
                                           m_AtlasSystem, SLOT(startDataStorage()));
}

void MainWindow::init(){

    m_AtlasSystem->setTreeViewWidget_AllObjects(m_treeWidget_AllObjects);
    m_AtlasSystem->setTreeViewWidget_CurrentView(m_treeWidget_CurrentView);
    m_AtlasSystem->setTreeViewWidget_MainView(m_treeWidget_Main);

    CreatingScene();
}

void MainWindow::CreatingScene(){

    QGLWidget *viewPort = new QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::AlphaChannel), ui->graphicsView);
    viewPort->makeCurrent();

    Scene* scene = new Scene();


    m_AtlasSystem->start();

    QGraphicsEllipseItem *ellipse;
    QGraphicsRectItem *rectangle;
    QGraphicsTextItem *text;

    QBrush greenBrush(Qt::green);
    QBrush blueBrush(Qt::blue);
    QPen outlinePen(Qt::black);
    outlinePen.setWidth(2);

    rectangle = scene->addRect(-10, 100,100, 100, outlinePen, blueBrush);

    // addEllipse(x,y,w,h,pen,brush)
    ellipse = scene->addEllipse(0, 100, 300, 60, outlinePen, greenBrush);

    text = scene->addText("Roman Priiskalov", QFont("Arial", 20) );
    // movable text
    text->setFlag(QGraphicsItem::ItemIsMovable);

    m_treeWidget_AllObjects->setWindowOpacity(0.75);
    m_treeWidget_AllObjects->move(0,0);
    m_treeWidget_CurrentView->setWindowOpacity(0.75);
    m_treeWidget_CurrentView->move(200, 0);
    m_treeWidget_Main->setWindowOpacity(0.75);
    m_treeWidget_Main->move(400,0);

    scene->addWidget(m_treeWidget_AllObjects);
    scene->addWidget(m_treeWidget_CurrentView);
    scene->addWidget(m_treeWidget_Main);

    QPushButton *btnuser = new QPushButton();
    btnuser->move(200, 200);
    btnuser->setText("Test User");
    btnuser->setWindowOpacity(0.75);

    scene->addWidget(btnuser);

    ui->graphicsView->setViewport(viewPort);
    ui->graphicsView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event){
    m_AtlasSystem->end();
    event->accept();
}
