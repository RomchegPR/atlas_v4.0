#ifndef DATASTORAGEDLL_H
#define DATASTORAGEDLL_H

#include "datastoragedll_global.h"

#include <QVector>
#include <QMap>

//#include "Builder/Director.h"
class DesignatedPoint;
class Procedure;
class ProcedureTransition;
class Airspace;
class VerticalStructure;
class BaseEntity;
class SegmentLeg;

class DATASTORAGEDLLSHARED_EXPORT DataStorage
{

public:
    DataStorage();

    void addDesignatedPoint(DesignatedPoint* point);
    DesignatedPoint* getDesignatedPoint(QString key);

    void addSegmentLeg(SegmentLeg* segment);
    SegmentLeg* getSegmentLeg(QString key);

    void addProcedure(Procedure* procedure);
    Procedure* getProcedure(QString key);
//    QVector < Airspace* > getAirspacesByType(CodeAirspace type);

    void addAirspace(Airspace* airspace);
    Airspace* getAirspace(QString key);

    void addObstacle(VerticalStructure* obstacle);
    VerticalStructure* getObstacle(QString key);

    BaseEntity* getObject(QString key);
    BaseEntity* getObjectByName(QString name);

    QVector <Airspace*>& getAirspaces();
    QVector <Procedure*>& getProcedures();
    QVector <DesignatedPoint*>& getDesignatedPoints();
    QVector <VerticalStructure*>& getObstacles();
    QMap < QString, BaseEntity* >& getAllObjects();

    void calcRenderDataForAllObjects();
    void getInfo();

private:
    QVector < DesignatedPoint* >     mDesignatedPoints;
    QVector < SegmentLeg* >          mSegmentLegs;
    QVector < Procedure* >           mProcedures;
    QVector < Airspace* >            mAirspaces;
    QVector < VerticalStructure* >   mObstacles;
    QMap < QString, BaseEntity* >    mAllObjects;
//    Director                         mDirectorRenderData;
};



#endif // DATASTORAGE_H
