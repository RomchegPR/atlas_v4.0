#ifndef CONSOLEWRITER_H
#define CONSOLEWRITER_H

#include <Base/Debug.h>
#include "Composite.h"

class ConsoleWriter
{
    int                         tabsCount;
    init(){
        tabsCount = 3;
    }

public:
    ConsoleWriter();
    QString                     getFormat(QString attribute, QString value);
    QList<QString>              getString(Component *info);
    void                        print(Component *info);

    int                         getTabsCount() const;
    void                        setTabsCount(int value);

};

#endif // CONSOLEWRITER_H
