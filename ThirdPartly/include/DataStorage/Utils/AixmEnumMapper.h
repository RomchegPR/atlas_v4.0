#ifndef AIXMENUMMAPPER_H
#define AIXMENUMMAPPER_H

#include "DataTypes/CodeDesignatedPoint.h"
#include "DataTypes/SegmentLegType.h"
#include "DataTypes/CodeSegmentTermination.h"
#include "DataTypes/CodeTrajectory.h"
#include "DataTypes/CodeSegmentPath.h"
#include "DataTypes/CodeCourse.h"
#include "DataTypes/CodeDirectionReference.h"
#include "DataTypes/CodeDirectionTurn.h"
#include "DataTypes/CodeSpeedReference.h"
#include "DataTypes/CodeAltitudeUse.h"
#include "DataTypes/CodeVerticalReference.h"
#include "DataTypes/CodeProcedureFixRole.h"
#include "DataTypes/CodeATCReporting.h"
#include "DataTypes/CodeBearing.h"
#include "DataTypes/CodeCardinalDirection.h"
#include "DataTypes/CodeAircraft.h"
#include "DataTypes/CodeAircraftEngine.h"
#include "DataTypes/CodeAircraftEngineNumber.h"
#include "DataTypes/CodeAircraftCategory.h"
#include "DataTypes/CodeValueInterpretation.h"
#include "DataTypes/CodeAircraftWingspanClass.h"
#include "DataTypes/CodeWakeTurbulence.h"
#include "DataTypes/CodeNavigationEquipment.h"
#include "DataTypes/CodeNavigationSpecification.h"
#include "DataTypes/CodeEquipmentAntiCollision.h"
#include "DataTypes/CodeCommunicationMode.h"
#include "DataTypes/CodeTransponder.h"
#include "DataTypes/CodeApproachPrefix.h"
#include "DataTypes/CodeApproach.h"
#include "DataTypes/CodeApproachEquipmentAdditional.h"
#include "DataTypes/CodeFinalGuidance.h"
#include "DataTypes/CodeApproachGuidance.h"
#include "DataTypes/CodeSide.h"
#include "DataTypes/CodeRelativePosition.h"
#include "DataTypes/CodeMissedApproach.h"
#include <QString>

// Уже ненужный класс

class AixmEnumMapper
{
public:
    AixmEnumMapper();

    static CodeMissedApproach toCodeMissedApproach(QString value);
    static QString toString(CodeMissedApproach value);

    static CodeRelativePosition toCodeRelativePosition(QString value);
    static QString toString(CodeRelativePosition value);

    static CodeSide toCodeSide(QString value);
    static QString toString(CodeSide value);

    static CodeApproachGuidance toCodeApproachGuidance(QString value);
    static QString toString(CodeApproachGuidance value);

    static CodeFinalGuidance toCodeFinalGuidance(QString value);
    static QString toString(CodeFinalGuidance value);

    static CodeApproachEquipmentAdditional toCodeApproachEquipmentAdditional(QString value);
    static QString toString(CodeApproachEquipmentAdditional value);

    static CodeApproach toCodeApproach(QString value);
    static QString toString(CodeApproach value);

    static CodeApproachPrefix toCodeApproachPrefix(QString value);
    static QString toString(CodeApproachPrefix value);

    static CodeEquipmentAntiCollision toCodeEquipmentAntiCollision(QString value);
    static QString toString(CodeEquipmentAntiCollision value);

    static CodeCommunicationMode toCodeCommunicationMode(QString value);
    static QString toString(CodeCommunicationMode value);

    static CodeTransponder toCodeTransponder(QString value);
    static QString toString(CodeTransponder value);

    static CodeWakeTurbulence toCodeWakeTurbulence(QString value);
    static QString toString(CodeWakeTurbulence value);

    static CodeNavigationEquipment toCodeNavigationEquipment(QString value);
    static QString toString(CodeNavigationEquipment value);

    static CodeNavigationSpecification toCodeNavigationSpecification(QString value);
    static QString toString(CodeNavigationSpecification value);

    static CodeDesignatedPoint toCodeDesignatedPoint(QString value);
    static QString toString(CodeDesignatedPoint value);

    static SegmentLegType toSegmentLegType(QString value);
    static QString toString(SegmentLegType value);

    static CodeSegmentTermination toCodeSegmentTermination(QString value);
    static QString toString(CodeSegmentTermination value);

    static CodeTrajectory toCodeTrajectory(QString value);
    static QString toString(CodeTrajectory value);

    static CodeSegmentPath toCodeSegmentPath(QString value);
    static QString toString(CodeSegmentPath value);

    static CodeCourse toCodeCourse(QString value);
    static QString toString(CodeCourse value);

    static CodeDirectionReference toCodeDirectionReference(QString value);
    static QString toString(CodeDirectionReference value);

    static CodeDirectionTurn toCodeDirectionTurn(QString value);
    static QString toString(CodeDirectionTurn value);

    static CodeSpeedReference toCodeSpeedReference(QString value);
    static QString toString(CodeSpeedReference value);

    static CodeAltitudeUse toCodeAltitudeUse(QString value);
    static QString toString(CodeAltitudeUse value);

    static CodeVerticalReference toCodeVerticalReference(QString value);
    static QString toString(CodeVerticalReference value);

    static CodeProcedureFixRole toCodeProcedureFixRole(QString value);
    static QString toString(CodeProcedureFixRole value);

    static CodeATCReporting toCodeATCReporting(QString value);
    static QString toString(CodeATCReporting value);

    static CodeBearing toCodeBearing(QString value);
    static QString toString(CodeBearing value);

    static CodeCardinalDirection toCodeCardinalDirection(QString value);
    static QString toString(CodeCardinalDirection value);

    static CodeAircraft toCodeAircraft(QString value);
    static QString toString(CodeAircraft value);

    static CodeAircraftEngine toCodeAircraftEngine(QString value);
    static QString toString(CodeAircraftEngine value);

    static CodeAircraftEngineNumber toCodeAircraftEngineNumber(QString value);
    static QString toString(CodeAircraftEngineNumber value);

    static CodeAircraftCategory toCodeAircraftCategory(QString value);
    static QString toString(CodeAircraftCategory value);

    static CodeValueInterpretation toCodeValueInterpretation(QString value);
    static QString toString(CodeValueInterpretation value);

    static CodeAircraftWingspanClass toCodeAircraftWingspanClass(QString value);
    static QString toString(CodeAircraftWingspanClass value);
};

#endif // AIXMENUMMAPPER_H
