#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <QString>
#include <QVector>

//class IComposite{
//    virtual QString getValue() = 0;
//};

class Component{
public:
    virtual void                    add(Component *elem){}
    virtual QString                 getValue(){return NULL;}
    virtual bool                    hasChildren() = 0;
    virtual int                     childrenCount() = 0;
    virtual QString                 getName(){return NULL;}
    virtual QVector<Component *>    getChildren(){
        QVector<Component*> vector;
        return vector;
    }
};

class Single : public Component
{
    QString     value;
public:
    Single();
    Single(QString value);

    bool hasChildren(){
        return false;
    }
    int childrenCount(){
        return 0;
    }

    QString                     getValue();
    void                        setValue(const QString &value);
};

//class Pair : public Component{

//    QVector<Component *>        children;
//    void                    add(Component *elem);

//public:

//    Pair(Component* attribute, Component* value);

//    bool hasChildren(){
//        return true;
//    }

//    int childrenCount(){
//        return 2;
//    }

//};

class Composite : public Component
{
    QVector<Component*>        children;
    QString                     name;

public:

    Composite(){}
    Composite(Component* attribute, Component* value);
    Composite(QVector<Component*> value);

    void                    add(Component *elem);

    bool hasChildren(){
        return true;
    }

    int childrenCount(){
        return children.size();
    }

    QVector<Component *>    getChildren();
    void                    setChildren(const QVector<Component *> &value);
    void                    setName(const QString &value);
    QString                 getName();
};

#endif // COMPOSITE_H
