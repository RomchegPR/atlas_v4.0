#ifndef PROCEDUREFIELDS_H
#define PROCEDUREFIELDS_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(ProcedureFields) \
  public:                                      \
    ProcedureFields() = default;
#include "Utils/enum.h"

BETTER_ENUM(
        ProcedureFields,
        char,
        segmentLegType,
        endConditionDesignator,
        legPath,
        legTypeARINC,
        courseType,
        courseDirection,
        turnDirection,
        speedReference,
        speedIntrpretation,
        upperLimitReference,
        lowerLimitReference,
        altitudeInterpretation,
        altitudeOverrideReference,
        startPoint,
        endPoint,
        arcCentre,
        angle,
        distance,
        aircraftCategory,
        holding,
        coords,
        trajectory,
        bankAngle,
        length,
        duration,
        procedureTurnRequired,
        speedLimit,
        course,
        verticalAngle,
        lowerLimitAltitude,
        upperLimitAltitude,
        altitudeOverrideATC
       );

#endif // PROCEDUREFIELDS_H
