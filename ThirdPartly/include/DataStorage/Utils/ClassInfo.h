#ifndef CLASSINFO_H
#define CLASSINFO_H

#include "Entities/VerticalStructure.h"
#include "Entities/Airspace.h"
#include "Entities/Procedure.h"
#include "Composite.h"
#include "BaseEntityVisitor.h"

class ClassInfo
{
public:
    ClassInfo();

    Composite*              getInfo(BaseEntityElement *element);
    Composite*              getInfo(VerticalStructure *verticalStructure);
    Composite*              getInfo(Airspace *airspace);
    Composite*              getInfo(Procedure *procedure);

    QStringList             getVerticalStructureAttributes() const;
    void                    setVerticalStructureAttributes(const QStringList &value);

    QStringList             getVerticalStructurePartAttributes() const;
    void                    setVerticalStructurePartAttributes(const QStringList &value);

    QStringList             getAirspaceAttributes() const;
    void                    setAirspaceAttributes(const QStringList &value);

    QStringList             getAirspaceVolumeAttributes() const;
    void                    setAirspaceVolumeAttributes(const QStringList &value);

    QStringList             getProcedureAttributes() const;
    void                    setProcedureAttributes(const QStringList &value);

    QStringList             getProcedureTransitionAttributes() const;
    void                    setProcedureTransitionAttributes(const QStringList &value);

    QStringList             getSegmentLegAttributes() const;
    void                    setSegmentLegAttributes(const QStringList &value);

private:

    QStringList             airspaceAttributes;
    QStringList             airspaceVolumeAttributes;

    QStringList             verticalStructureAttributes;
    QStringList             verticalStructurePartAttributes;

    QStringList             procedureAttributes;
    QStringList             procedureTransitionAttributes;
    QStringList             segmentLegAttributes;

    void                    initVerticalStructureAttributes();
    void                    initVerticalStructurePartAttributes();
    void                    initAirspaceAttributes();
    void                    initAirspaceVolumeAttributes();
    void                    initProcedureAttributes();
    void                    initProcedureTransitionAttributes();
    void                    initSegmentLegAttributes();

    init(){
        initVerticalStructureAttributes();
        initVerticalStructurePartAttributes();
        initAirspaceAttributes();
        initAirspaceVolumeAttributes();
        initProcedureAttributes();
        initProcedureTransitionAttributes();
        initSegmentLegAttributes();
    }

};

#endif // CLASSINFO_H
