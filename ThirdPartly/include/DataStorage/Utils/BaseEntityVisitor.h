#ifndef BASEENTITYVISITOR_H
#define BASEENTITYVISITOR_H

#include "Entities/VerticalStructure.h"
#include "Entities/Airspace.h"
#include "Entities/Procedure.h"
#include "Composite.h"
#include "ProcedureFields.h"

class BaseEntityElement;
class ProcedureElement;
class AirspaceElement;
class VerticalStructureElement;
class ProcedureTransitionElement;
class AirspaceVolumeElement;
class VerticalStructurePartElement;
class SegmentLegElement;

class BaseEntityVisitor
{
public:
    BaseEntityVisitor();
    virtual void                    visit(BaseEntityElement &ref) = 0;
    virtual void                    visit(ProcedureElement &ref) = 0;
    virtual void                    visit(AirspaceElement &ref) = 0;
    virtual void                    visit(VerticalStructureElement &ref) = 0;

    virtual void                    visit(ProcedureTransitionElement &ref) = 0;
    virtual void                    visit(AirspaceVolumeElement &ref) = 0;
    virtual void                    visit(VerticalStructurePartElement &ref) = 0;
    virtual void                    visit(SegmentLegElement &ref) = 0;

    virtual void                    visit(ProcedureElement &ref, QString &fieldName) = 0;
    virtual void                    visit(AirspaceElement &ref, QString &fieldName) = 0;
    virtual void                    visit(VerticalStructureElement &ref, QString &fieldName) = 0;

    virtual void                    visit(ProcedureTransitionElement &ref, QString &fieldName) = 0;
    virtual void                    visit(AirspaceVolumeElement &ref, QString &fieldName) = 0;
    virtual void                    visit(VerticalStructurePartElement &ref, QString &fieldName) = 0;
    virtual void                    visit(SegmentLegElement &ref, QString &fieldName) = 0;
    virtual ~BaseEntityVisitor() = default;
};

class BaseEntityElement
{
    BaseEntity                      value;

public:
    BaseEntity                      getValue() const;
    void                            setValue(BaseEntity &value);

    virtual void                    accept(BaseEntityVisitor &v) = 0;

    virtual void                    accept(BaseEntityVisitor &v, QString &fieldName) = 0;

    virtual ~BaseEntityElement() = default;
};

class ProcedureElement : public BaseEntityElement
{
    Procedure                       value;

public:
    Procedure                       getValue() const;
    void                            setValue(Procedure &value);

    void                            accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    void                            accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class ProcedureTransitionElement : public BaseEntityElement
{
    ProcedureTransition             value;

public:
    ProcedureTransition             getValue() const;
    void                            setValue(ProcedureTransition &value);

    void                            accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    void                            accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }

};

class AirspaceElement : public BaseEntityElement
{
    Airspace                        value;

public:
    Airspace                        getValue() const;
    void                            setValue(Airspace &value);

    virtual void                    accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    virtual void                    accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class AirspaceVolumeElement : public BaseEntityElement
{
    AirspaceVolume                  value;

public:
    AirspaceVolume                  getValue() const;
    void                            setValue(AirspaceVolume &value);

    virtual void                    accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    virtual void                    accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class VerticalStructureElement : public BaseEntityElement
{
    VerticalStructure               value;

public:
    VerticalStructure               getValue() const;
    void                            setValue(VerticalStructure &value);

    void                            accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    void                            accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class VerticalStructurePartElement : public BaseEntityElement
{
    VerticalStructurePart           value;

public:
    VerticalStructurePart           getValue() const;
    void                            setValue(VerticalStructurePart &value);

    void                            accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    void                            accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class SegmentLegElement : public BaseEntityElement
{
    SegmentLeg                      value;

public:
    SegmentLeg                      getValue() const;
    void                            setValue(SegmentLeg &value);

    void                            accept(BaseEntityVisitor &v)
    {
        v.visit(*this);
    }

    void                            accept(BaseEntityVisitor &v, QString &fieldName)
    {
        v.visit(*this, fieldName);
    }
};

class GetField : public BaseEntityVisitor {

public:
    Component                       *value;

    void                            visit(BaseEntityElement &ref) override {}
    void                            visit(ProcedureElement &ref) override {}
    void                            visit(AirspaceElement &ref) override {}
    void                            visit(VerticalStructureElement &ref) override {}

    void                            visit(ProcedureTransitionElement &ref) override {}
    void                            visit(AirspaceVolumeElement &ref) override {}
    void                            visit(VerticalStructurePartElement &ref) override {}
    void                            visit(SegmentLegElement &ref) override {}

    void                            visit(ProcedureElement &ref, QString &fieldName) override;
    void                            visit(AirspaceElement &ref, QString &fieldName) override;
    void                            visit(VerticalStructureElement &ref, QString &fieldName) override;

    void                            visit(ProcedureTransitionElement &ref, QString &fieldName) override;
    void                            visit(AirspaceVolumeElement &ref, QString &fieldName) override;
    void                            visit(VerticalStructurePartElement &ref, QString &fieldName) override;
    void                            visit(SegmentLegElement &ref, QString &fieldName) override;
    Component                       *getValue() const;
    void                            setValue(Component *value);
};

class GetAttributesName : public BaseEntityVisitor {

public:
    QStringList                     value;

    void                            visit(BaseEntityElement &ref) override {}
    void                            visit(ProcedureElement &ref) override;
    void                            visit(AirspaceElement &ref) override;
    void                            visit(VerticalStructureElement &ref) override;

    void                            visit(ProcedureTransitionElement &ref) override;
    void                            visit(AirspaceVolumeElement &ref) override;
    void                            visit(VerticalStructurePartElement &ref) override;
    void                            visit(SegmentLegElement &ref) override;

    void                            visit(ProcedureElement &ref, QString &fieldName) override {}
    void                            visit(AirspaceElement &ref, QString &fieldName) override {}
    void                            visit(VerticalStructureElement &ref, QString &fieldName) override {}

    void                            visit(ProcedureTransitionElement &ref, QString &fieldName) override {}
    void                            visit(AirspaceVolumeElement &ref, QString &fieldName) override {}
    void                            visit(VerticalStructurePartElement &ref, QString &fieldName) override {}
    void                            visit(SegmentLegElement &ref, QString &fieldName) override {}
};

class GetHelp : public BaseEntityVisitor
{
public:
    Composite *value;

    void                            visit(BaseEntityElement &ref) override;
    void                            visit(ProcedureElement &ref) override { visit( (BaseEntityElement&) ref);}
    void                            visit(AirspaceElement &ref) override { visit( (BaseEntityElement&) ref);}
    void                            visit(VerticalStructureElement &ref) override { visit( (BaseEntityElement&) ref);}

    void                            visit(ProcedureTransitionElement &ref) override { visit( (BaseEntityElement&) ref);}
    void                            visit(AirspaceVolumeElement &ref) override { visit( (BaseEntityElement&) ref);}
    void                            visit(VerticalStructurePartElement &ref) override { visit( (BaseEntityElement&) ref);}
    void                            visit(SegmentLegElement &ref) override { visit( (BaseEntityElement&) ref);}

    void                            visit(ProcedureElement &ref, QString &fieldName) override {}
    void                            visit(AirspaceElement &ref, QString &fieldName) override {}
    void                            visit(VerticalStructureElement &ref, QString &fieldName) override {}

    void                            visit(ProcedureTransitionElement &ref, QString &fieldName) override {}
    void                            visit(AirspaceVolumeElement &ref, QString &fieldName) override {}
    void                            visit(VerticalStructurePartElement &ref, QString &fieldName) override {}
    void                            visit(SegmentLegElement &ref, QString &fieldName) override {}

    Composite                       *getValue() const;
    void                            setValue(Composite *value);
};


#endif // BASEENTITYVISITOR_H
