#ifndef SEGMENTLEGFACTORY_H
#define SEGMENTLEGFACTORY_H
#include <DataTypes/SegmentLegType.h>

class SegmentLeg;

// Фабрика сегментов

class SegmentLegFactory
{
public:
    SegmentLegFactory();
    static SegmentLeg* makeLeg(SegmentLegType type);
};

#endif // SEGMENTLEGFACTORY_H
