#ifndef DEBUG_H
#define DEBUG_H

#include <QtGlobal>
#include <QtCore>

#include <QLoggingCategory>
#include <QDateTime>
#include <QDebug>
#include <QErrorMessage>
#include <QMessageBox>

/*---EXAMPLE---*/
//
Q_DECLARE_LOGGING_CATEGORY(logLoader)
Q_DECLARE_LOGGING_CATEGORY(logDebug)
Q_DECLARE_LOGGING_CATEGORY(logAixmReader)
Q_DECLARE_LOGGING_CATEGORY(airSpaceBuilder)

/*---EXAMPLE---*/

/********************************

Class:		Debug

Purpose:	Class for create/writing logFile.

********************************/

void                            startLogInFile();  // Redirect all system messages to Log file.
void                            createLogFile(QString &dir, QString name);
void                            messageHandler(QtMsgType type,
                                const QMessageLogContext &context,
                                const QString &msg);
void                            debugRenderObjectVertex(QString name);

#endif // DEBUG_H
