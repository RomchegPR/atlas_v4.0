#ifndef MEASURES_H
#define MEASURES_H

class CAltitude
{
private:
	float m_Altitude;

public:
	CAltitude() {}

	CAltitude(const CAltitude& Altitude);

	CAltitude& operator=(const CAltitude& Altitude);
	CAltitude& operator+=(const CAltitude& Altitude);
    CAltitude& operator+=(const float& value);
	CAltitude& operator-=(const CAltitude& Altitude);
	CAltitude& operator*=(const CAltitude& Altitude);
	CAltitude& operator/=(const CAltitude& Altitude);
	const CAltitude operator+(const CAltitude& Altitude) const;
	const CAltitude operator-(const CAltitude& Altitude) const;
	const CAltitude operator*(const CAltitude& Altitude) const;
	const CAltitude operator/(const CAltitude& Altitude) const;
    const CAltitude operator/(const float& value) const;

	float Meters() const;
	void SetMeters(const float fAltitude_m);

	float Feet() const;
	void SetFeet(const float fAltitude_ft);

	float Kilometers() const;
	void SetKilometers(const float fAltitude_km);

	int FlightLevel() const;
	void SetFlightLevel(const int iAltitude_FL);
};



class CDistance
{
private:
	float m_Distance;

public:
	CDistance() {}

	CDistance(const CDistance& Distance);

	CDistance& operator=(const CDistance& Distance);
	CDistance& operator+=(const CDistance& Distance);
	CDistance& operator-=(const CDistance& Distance);
	CDistance& operator*=(const CDistance& Distance);
	CDistance& operator/=(const CDistance& Distance);
	const CDistance operator+(const CDistance& Distance) const;
	const CDistance operator-(const CDistance& Distance) const;
	const CDistance operator*(const CDistance& Distance) const;
	const CDistance operator/(const CDistance& Distance) const;

	float Meters() const;
	void SetMeters(const float fDistance_m);

	float Kilometers() const;
	void SetKilometers(const float fDistance_km);

	float NauticalMiles() const;
	void SetNauticalMiles(const float fDistance_nm);

	float StatutMiles() const;
	void SetStatutMiles(const float fDistance_miles);
};



class CSpeed
{
private:
	float m_Speed;

public:
	CSpeed() {}

	CSpeed(const CSpeed& Speed);

	CSpeed& operator=(const CSpeed& Speed);
	CSpeed& operator+=(const CSpeed& Speed);
	CSpeed& operator-=(const CSpeed& Speed);
	CSpeed& operator*=(const CSpeed& Speed);
	CSpeed& operator/=(const CSpeed& Speed);
	const CSpeed operator+(const CSpeed& Speed) const;
	const CSpeed operator-(const CSpeed& Speed) const;
	const CSpeed operator*(const CSpeed& Speed) const;
	const CSpeed operator/(const CSpeed& Speed) const;

	float MeterSec() const;
	void SetMeterSec(const float fSpeed_ms);

	float KmH() const;
	void SetKmh(const float fSpeed_KmH);

	float Kts() const;
	void SetKts(const float fSpeed_Kts);

	float FtMin() const;
	void SetFtMin(const float fSpeed_FtMin);
};



class CTime
{
	private:
		float m_Time;
	public:
		CTime() {}

		CTime(const CTime& Time);

		CTime& operator=(const CTime& Time);
		CTime& operator+=(const CTime& Time);
		CTime& operator-=(const CTime& Time);
		CTime& operator*=(const CTime& Time);
		CTime& operator/=(const CTime& Time);
		const CTime operator+(const CTime& Time) const;
		const CTime operator-(const CTime& Time) const;
		const CTime operator*(const CTime& Time) const;
		const CTime operator/(const CTime& Time) const;

		float Seconds() const;
		void SetSeconds(const float fTime_sec);

		float Minutes() const;
		void SetMinutes(const float fTime_min);

		float Hours() const;
		void SetHours(const float fTime_hours);

		float Days() const;
		void SetDays(const float fTime_days);
};

#endif // MEASURES_H
