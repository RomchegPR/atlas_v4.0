#ifndef CODEDESIGNSTANDARD_H
#define CODEDESIGNSTANDARD_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeDesignStandard) \
  public:                                      \
    CodeDesignStandard() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeDesignStandard,
            char,
            PANS_OPS,
            TERPS,
            CANADA_TERPS,
            NATO,
            OTHER
           );

/*enum class CodeDesignStandard
{
    PANS_OPS,
    TERPS,
    CANADA_TERPS,
    NATO,
    OTHER,
    NONE
};*/

#endif // CODEDESIGNSTANDARD_H
