#ifndef CODESIDE_H
#define CODESIDE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeSide) \
  public:                                      \
    CodeSide() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeSide,
            char,
            LEFT,
            RIGHT,
            BOTH,
            OTHER
           );

/*enum class CodeSide
{
    LEFT,
    RIGHT,
    BOTH,
    OTHER,
    NONE
};*/

#endif // CODESIDE_H
