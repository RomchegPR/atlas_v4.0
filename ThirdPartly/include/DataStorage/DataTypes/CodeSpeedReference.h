#ifndef CODESPEEDREFERENCE_H
#define CODESPEEDREFERENCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeSpeedReference) \
  public:                                      \
    CodeSpeedReference() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeSpeedReference,
            char,
            IAS,
            TAS,
            GS,
            OTHER
           );

/*enum class CodeSpeedReference
{
    IAS,
    TAS,
    GS,
    OTHER,
    NONE
};*/

#endif // CODESPEEDREFERENCE_H
