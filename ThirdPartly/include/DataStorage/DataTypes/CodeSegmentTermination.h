#ifndef CODESEGMENTTERMINATION_H
#define CODESEGMENTTERMINATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeSegmentTermination) \
  public:                                      \
    CodeSegmentTermination() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeSegmentTermination,
            char,
            ALTITUDE,
            DISTANCE,
            DURATION,
            INTERCEPT,
            OTHER
           );

/*enum class CodeSegmentTermination
{
    ALTITUDE,
    DISTANCE,
    DURATION,
    INTERCEPT,
    OTHER,
    NONE
};*/

#endif // CODESEGMENTTERMINATION_H
