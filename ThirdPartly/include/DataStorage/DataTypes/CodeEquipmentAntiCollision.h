#ifndef CODEEQUIPMENTANTICOLLISION_H
#define CODEEQUIPMENTANTICOLLISION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeEquipmentAntiCollision) \
  public:                                      \
    CodeEquipmentAntiCollision() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeEquipmentAntiCollision,
            char,
            ACAS_I,
            ACAS_II,
            GPWS,
            OTHER
           );

/*enum class CodeEquipmentAntiCollision
{
    ACAS_I,
    ACAS_II,
    GPWS,
    OTHER,
    NONE
};*/

#endif // CODEEQUIPMENTANTICOLLISION_H
