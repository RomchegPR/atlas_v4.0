#ifndef VALDISTANCEBASETYPE_H
#define VALDISTANCEBASETYPE_H
#include <QString>

class ValDistanceBaseType
{
public:
    ValDistanceBaseType();

    QString getMinInclusive() const;
    void setMinInclusive(const QString &value);


private:
    QString minInclusive;
    init(){
        minInclusive = "";
    }
};

#endif // VALDISTANCEBASETYPE_H
