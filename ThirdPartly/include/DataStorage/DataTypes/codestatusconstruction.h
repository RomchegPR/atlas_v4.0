#ifndef CODESTATUSCONSTRUCTION_H
#define CODESTATUSCONSTRUCTION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeStatusConstruction) \
  public:                                      \
    CodeStatusConstruction() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeStatusConstruction,
            char,
            COMPLETED,
            DEMOLITION_PLANNED,
            IN_CONSTRUCTION,
            IN_DEMOLITION,
            OTHER
           );


#endif // CODESTATUSCONSTRUCTION_H
