#ifndef CODEWAKETURBULENCE_H
#define CODEWAKETURBULENCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeWakeTurbulence) \
  public:                                      \
    CodeWakeTurbulence() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeWakeTurbulence,
            char,
            LOW,
            MEDIUM,
            HIGH,
            OTHER
           );

/*enum class CodeWakeTurbulence
{
    LOW,
    MEDIUM,
    HIGH,
    OTHER,
    NONE
};*/


#endif // CODEWAKETURBULENCE_H
