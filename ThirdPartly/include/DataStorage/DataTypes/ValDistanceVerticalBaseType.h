#ifndef VALDISTANCEVERTICALBASETYPE_H
#define VALDISTANCEVERTICALBASETYPE_H
#include <QString>

class ValDistanceVerticalBaseType
{
public:
    ValDistanceVerticalBaseType();

    QString getPattern() const;
    void setPattern(const QString &value);
private:
    QString pattern;
    init(){
        pattern = "";
    }
};

#endif // VALDISTANCEVERTICALBASETYPE_H
