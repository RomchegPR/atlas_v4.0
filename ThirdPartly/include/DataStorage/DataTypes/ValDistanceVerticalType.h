#ifndef VALDISTANCEVERTICALTYPE_H
#define VALDISTANCEVERTICALTYPE_H
#include "ValDistanceVerticalBaseType.h"
#include "NilReasonEnumeration.h"
#include "UomDistanceVerticalType.h"

class ValDistanceVerticalType: ValDistanceVerticalBaseType
{

public:

    ValDistanceVerticalType();
    ValDistanceVerticalType(double value, const UomDistanceVerticalType &uom);

    NilReasonEnumeration getNilReason() const;
    void setNilReason(const NilReasonEnumeration &value);

    UomDistanceVerticalType getUom() const;
    void setUom(const UomDistanceVerticalType &value);

    double getValue() const;
    void setValue(double value);

private:

    NilReasonEnumeration nilReason;
    UomDistanceVerticalType uom;
    double value;
    init(){
        nilReason = NilReasonEnumeration::OTHER;
        uom = UomDistanceVerticalType::OTHER;
    }
};

#endif // VALDISTANCEVERTICALTYPE_H
