#ifndef PROCEDURETYPE_H
#define PROCEDURETYPE_H


#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(ProcedureType) \
  public:                                      \
    ProcedureType() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            ProcedureType,
            char,
            InstrumentApproachProcedure,
            StandardInstrumentArrival,
            StandardInstrumentDeparture
           );


#endif // PROCEDURETYPE_H
