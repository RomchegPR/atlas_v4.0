#ifndef VALDISTANCETYPE_H
#define VALDISTANCETYPE_H
#include "ValDistanceBaseType.h"
#include "NilReasonEnumeration.h"
#include "UomDistanceType.h"

class ValDistanceType : ValDistanceBaseType
{
public:
    ValDistanceType();

    NilReasonEnumeration getNilReason() const;
    void setNilReason(const NilReasonEnumeration &value);

    UomDistanceType getUom() const;
    void setUom(const UomDistanceType &value);

private:
    NilReasonEnumeration nilReason;
    UomDistanceType uom;
    void init(){
        nilReason = NilReasonEnumeration::OTHER;
        uom = UomDistanceType::OTHER;
    }
};

#endif // VALDISTANCETYPE_H
