#ifndef CODECOURSE_H
#define CODECOURSE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeCourse) \
  public:                                      \
    CodeCourse() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeCourse,
            char,
            TRUE_TRACK,
            MAG_TRACK,
            TRUE_BRG,
            MAG_BRG,
            HDG,
            RDL,
            OTHER
           );

/*enum class CodeCourse
{
    TRUE_TRACK,
    MAG_TRACK,
    TRUE_BRG,
    MAG_BRG,
    HDG,
    RDL,
    OTHER,
    NONE
};*/

#endif // CODECOURSE_H
