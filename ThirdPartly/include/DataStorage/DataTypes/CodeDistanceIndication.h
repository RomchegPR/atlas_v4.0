#ifndef CODEDISTANCEINDICATION_H
#define CODEDISTANCEINDICATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeDistanceIndication) \
  public:                                      \
    CodeDistanceIndication() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeDistanceIndication,
            char,
            DME,
            GEODETIC,
            OTHER
           );


#endif // CODEDISTANCEINDICATION_H
