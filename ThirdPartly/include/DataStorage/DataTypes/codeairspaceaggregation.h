#ifndef CODEAIRSPACEAGGREGATION_H
#define CODEAIRSPACEAGGREGATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAirspaceAggregation) \
  public:                                      \
    CodeAirspaceAggregation() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeAirspaceAggregation,
            char,
            BASE,
            INTERS,
            OTHER,
            SUBTR,
            UNION
           );

#endif // CODEAIRSPACEAGGREGATION_H
