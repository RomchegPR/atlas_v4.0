#ifndef CODEAIRSPACE_H
#define CODEAIRSPACE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAirspace) \
  public:                                      \
    CodeAirspace() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeAirspace,
            char,
            A,
            ADIZ,
            ADV,
            AMA,
            ASR,
            ATZ,
            ATZ_P,
            AWY,
            CBA,
            CLASS,
            CTA,
            CTA_P,
            CTR,
            CTR_P,
            D,
            D_OTHER,
            FIR,
            FIR_P,
            HTZ,
            MTR,
            NAS,
            NAS_P,
            NO_FIR,
            OCA,
            OCA_P,
            OTA,
            OTHER,
            P,
            PART,
            POLITICAL,
            PROTECT,
            R,
            RAS,
            RCA,
            SECTOR,
            SECTOR_C,
            TMA,
            TMA_P,
            TRA,
            TSA,
            UADV,
            UIR,
            UIR_P,
            UTA,
            UTA_P,
            W
           );

#endif // CODEAIRSPACE_H
