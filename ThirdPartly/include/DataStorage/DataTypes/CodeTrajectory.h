#ifndef CODETRAJECTORY_H
#define CODETRAJECTORY_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeTrajectory) \
  public:                                      \
    CodeTrajectory() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeTrajectory,
            char,
            STRAIGHT,
            ARC,
            PT,
            BASETURN,
            HOLDING,
            OTHER
           );

/*enum class CodeTrajectory
{
    STRAIGHT,
    ARC,
    PT,
    BASETURN,
    HOLDING,
    OTHER,
    NONE
};*/

#endif // CODETRAJECTORY_H
