#ifndef CODEDESIGNATEDPOINT_H
#define CODEDESIGNATEDPOINT_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeDesignatedPoint) \
  public:                                      \
    CodeDesignatedPoint() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeDesignatedPoint,
            char,
            ICAO,
            COORD,
            CNF,
            DESIGNED,
            MTR,
            TERMINAL,
            BRG_DIST,
            OTHER
           );

/*enum class CodeDesignatedPoint
{
    ICAO,
    COORD,
    CNF,
    DESIGNED,
    MTR,
    TERMINAL,
    BRG_DIST,
    OTHER,
    NONE
};*/

#endif // CODEDESIGNATEDPOINT_H
