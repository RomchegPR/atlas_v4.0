#ifndef CODEFINALGUIDANCE_H
#define CODEFINALGUIDANCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeFinalGuidance) \
  public:                                      \
    CodeFinalGuidance() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeFinalGuidance,
            char,
            LPV,
            LNAV_VNAV,
            LNAV,
            GLS,
            ASR,
            ARA,
            ARSR,
            PAR,
            ILS,
            ILS_DME,
            ISL_PRM,
            LDA,
            LDA_DME,
            LOC,
            LOC_BC,
            LOC_DME,
            LOC_DME_BC,
            MLS,
            MLS_DME,
            NDB,
            NDB_DME,
            SDF,
            TLS,
            VOR,
            VOR_DME,
            TACAN,
            VORTAC,
            DME,
            LP,
            OTHER
           );


/*enum class CodeFinalGuidance
{
    LPV,
    LNAV_VNAV,
    LNAV,
    GLS,
    ASR,
    ARA,
    ARSR,
    PAR,
    ILS,
    ILS_DME,
    ISL_PRM,
    LDA,
    LDA_DME,
    LOC,
    LOC_BC,
    LOC_DME,
    LOC_DME_BC,
    MLS,
    MLS_DME,
    NDB,
    NDB_DME,
    SDF,
    TLS,
    VOR,
    VOR_DME,
    TACAN,
    VORTAC,
    DME,
    LP,
    OTHER,
    NONE
};*/

#endif // CODEFINALGUIDANCE_H
