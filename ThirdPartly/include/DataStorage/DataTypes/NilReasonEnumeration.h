#ifndef NILREASONENUMERATION_H
#define NILREASONENUMERATION_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(NilReasonEnumeration) \
public:                                      \
    NilReasonEnumeration() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            NilReasonEnumeration,
            char,
            INAPPLICABLE,
            MISSING,
            OTHER,
            TEMPLATE,
            UNKNOWN,
            WITHHELD
            );

#endif // NILREASONENUMERATION_H
