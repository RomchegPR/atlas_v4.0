#ifndef CODETRANSPONDER_H
#define CODETRANSPONDER_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeTransponder) \
  public:                                      \
    CodeTransponder() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeTransponder,
            char,
            MODE_1,
            MODE_2,
            MODE_3A,
            MODE_4,
            MODE_5,
            MODE_C,
            MODE_S,
            OTHER
           );

/*enum class CodeTransponder
{
    MODE_1,
    MODE_2,
    MODE_3A,
    MODE_4,
    MODE_5,
    MODE_C,
    MODE_S,
    OTHER,
    NONE
};*/

#endif // CODETRANSPONDER_H
