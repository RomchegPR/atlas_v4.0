#ifndef CODECOLOUR_H
#define CODECOLOUR_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeColour) \
  public:                                      \
    CodeColour() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeColour,
            char,
            AMBER,
            BLACK,
            BLUE,
            BROWN,
            GREEN,
            GREY,
            LIGHT_GREY,
            MAGENTA,
            ORANGE,
            OTHER,
            PINK,
            PURPLE,
            RED,
            VIOLET,
            WHITE,
            YELLOW
           );

#endif // CODECOLOUR_H
