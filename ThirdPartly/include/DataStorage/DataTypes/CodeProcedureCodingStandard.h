#ifndef CODEPROCEDURECODINGSTANDARD_H
#define CODEPROCEDURECODINGSTANDARD_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeProcedureCodingStandard) \
  public:                                      \
    CodeProcedureCodingStandard() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeProcedureCodingStandard,
            char,
            PANS_OPS,
            ARINC_424_15,
            ARINC_424_18,
            ARINC_424_19,
            OTHER
           );

/*enum class CodeProcedureCodingStandard
{
    PANS_OPS,
    ARINC_424_15,
    ARINC_424_18,
    ARINC_424_19,
    OTHER,
    NONE
};*/

#endif // CODEPROCEDURECODINGSTANDARD_H
