#ifndef CODEAPPROACH_H
#define CODEAPPROACH_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeApproach) \
  public:                                      \
    CodeApproach() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeApproach,
            char,
            ASR,
            ARA,
            ARSR,
            PAR,
            ILS,
            ILS_DME,
            ILS_PRM,
            LDA,
            LDA_DME,
            LOC,
            LOC_DME,
            LOC_DME_BC,
            MLS,
            MLS_DME,
            NDB,
            NDB_DME,
            SDF,
            TLS,
            VOR,
            VOR_DME,
            TACAN,
            VORTAC,
            DME,
            DME_DME,
            RNP,
            GPS,
            GLONASS,
            GALILEO,
            RNAV,
            IGS,
            OTHER
           )

/*enum class CodeApproach
{
    ASR,
    ARA,
    ARSR,
    PAR,
    ILS,
    ILS_DME,
    ILS_PRM,
    LDA,
    LDA_DME,
    LOC,
    LOC_DME,
    LOC_DME_BC,
    MLS,
    MLS_DME,
    NDB,
    NDB_DME,
    SDF,
    TLS,
    VOR,
    VOR_DME,
    TACAN,
    VORTAC,
    DME,
    DME_DME,
    RNP,
    GPS,
    GLONASS,
    GALILEO,
    RNAV,
    IGS,
    OTHER,
    NONE
};*/

#endif // CODEAPPROACH_H
