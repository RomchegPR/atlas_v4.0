#ifndef VALDISTANCESIGNEDTYPE_H
#define VALDISTANCESIGNEDTYPE_H
#include "NilReasonEnumeration.h"
#include "UomDistanceType.h"

class ValDistanceSignedType
{
public:
    ValDistanceSignedType();

    NilReasonEnumeration getNilReason() const;
    void setNilReason(const NilReasonEnumeration &value);

    UomDistanceType getUom() const;
    void setUom(const UomDistanceType &value);

private:
    NilReasonEnumeration nilReason;
    UomDistanceType uom;
    init(){
        nilReason = NilReasonEnumeration::OTHER;
        uom = UomDistanceType::OTHER;
    }
};

#endif // VALDISTANCESIGNEDTYPE_H
