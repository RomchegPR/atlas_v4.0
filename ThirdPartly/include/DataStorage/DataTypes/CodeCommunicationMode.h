#ifndef CODECOMMUNICATIONMODE_H
#define CODECOMMUNICATIONMODE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeCommunicationMode) \
  public:                                      \
    CodeCommunicationMode() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeCommunicationMode,
            char,
            HF,
            VHF,
            VDL1,
            VDL2,
            VDL4,
            AMSS,
            ADS_B,
            ADS_B_VDL,
            HFDL,
            VHF_833,
            UHF,
            OTHER
           );

/*enum class CodeCommunicationMode
{
    HF,
    VHF,
    VDL1,
    VDL2,
    VDL4,
    AMSS,
    ADS_B,
    ADS_B_VDL,
    HFDL,
    VHF_833,
    UHF,
    OTHER,
    NONE
};*/

#endif // CODECOMMUNICATIONMODE_H
