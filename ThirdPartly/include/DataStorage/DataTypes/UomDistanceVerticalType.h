#ifndef UOMDISTANCEVERTICALTYPE_H
#define UOMDISTANCEVERTICALTYPE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(UomDistanceVerticalType) \
public:                                      \
    UomDistanceVerticalType() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            UomDistanceVerticalType,
            char,
            FL,
            FT,
            M,
            OTHER,
            SM
            );

#endif // UOMDISTANCEVERTICALTYPE_H
