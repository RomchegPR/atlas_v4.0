#ifndef CODEBEARING_H
#define CODEBEARING_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeBearing) \
  public:                                      \
    CodeBearing() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeBearing,
            char,
            TRUE_BEARING,
            MAG,
            RDL,
            TRK,
            HDG,
            OTHER
           );

/*enum class CodeBearing
{
    TRUE_BEARING,
    MAG,
    RDL,
    TRK,
    HDG,
    OTHER,
    NONE
};*/

#endif // CODEBEARING_H
