#ifndef UOMDISTANCETYPE_H
#define UOMDISTANCETYPE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(UomDistanceType) \
public:                                      \
    UomDistanceType() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            UomDistanceType,
            char,
            CM,
            FT,
            KM,
            M,
            MI,
            NM,
            OTHER
            );

#endif // UOMDISTANCETYPE_H
