#ifndef CODEPROCEDUREFIXROLE_H
#define CODEPROCEDUREFIXROLE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeProcedureFixRole) \
  public:                                      \
    CodeProcedureFixRole() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeProcedureFixRole,
            char,
            IAF,
            IF,
            IF_IAF,
            FAF,
            VDP,
            SDF,
            FPAP,
            FTP,
            FROP,
            TP,
            MAPT,
            MAHF,
            RECNAV,
            OTHER
           );

/*enum class CodeProcedureFixRole
{
    IAF,
    IF,
    IF_IAF,
    FAF,
    VDP,
    SDF,
    FPAP,
    FTP,
    FROP,
    TP,
    MAPT,
    MAHF,
    OTHER,
    NONE
};*/

#endif // CODEPROCEDUREFIXROLE_H
