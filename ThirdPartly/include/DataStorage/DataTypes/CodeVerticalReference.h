#ifndef CODEVERTICALREFERENCE_H
#define CODEVERTICALREFERENCE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeVerticalReference) \
  public:                                      \
    CodeVerticalReference() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeVerticalReference,
            char,
            SFC,
            MSL,
            W84,
            STD,
            OTHER
           );

/*enum class CodeVerticalReference
{
    SFC,
    MSL,
    W84,
    STD,
    OTHER,
    NONE
};*/

#endif // CODEVERTICALREFERENCE_H
