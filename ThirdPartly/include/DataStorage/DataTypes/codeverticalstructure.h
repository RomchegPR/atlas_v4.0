#ifndef CODEVERTICALSTRUCTURE_H
#define CODEVERTICALSTRUCTURE_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeVerticalStructure) \
  public:                                      \
    CodeVerticalStructure() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeVerticalStructure,
            char,
            AG_EQUIP,
            ANTENNA,
            ARCH,
            BRIDGE,
            BUILDING,
            CABLE_CAR,
            CATENARY,
            COMPRESSED_AIR_SYSTEM,
            CONTROL_MONITORING_SYSTEM,
            CONTROL_TOWER,
            COOLING_TOWER,
            CRANE,
            DAM,
            DOME,
            ELECTRICAL_EXIT_LIGHT,
            ELECTRICAL_SYSTEM,
            ELEVATOR,
            FENCE,
            FUEL_SYSTEM,
            GATE,
            GENERAL_UTILITY,
            GRAIN_ELEVATOR,
            HEAT_COOL_SYSTEM,
            INDUSTRIAL_SYSTEM,
            LIGHTHOUSE,
            MONUMENT,
            NATURAL_GAS_SYSTEM,
            NATURAL_HIGHPOINT,
            NAVAID,
            NUCLEAR_REACTOR,
            OTHER,
            POLE,
            POWER_PLANT,
            REFINERY,
            RIG,
            SALTWATER_SYSTEM,
            SIGN,
            SPIRE,
            STACK,
            STADIUM,
            STORM_SYSTEM,
            TANK,
            TETHERED_BALLOON,
            TOWER,
            TRAMWAY,
            TRANSMISSION_LINE,
            TREE,
            URBAN,
            VEGETATION,
            WALL,
            WASTEWATER_SYSTEM,
            WATER_SYSTEM,
            WATER_TOWER,
            WINDMILL,
            WINDMILL_FARMS
           );

#endif // CODEVERTICALSTRUCTURE_H
