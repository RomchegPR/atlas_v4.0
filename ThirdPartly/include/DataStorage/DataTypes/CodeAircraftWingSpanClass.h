#ifndef CODEAIRCRAFTWINGSPANCLASS_H
#define CODEAIRCRAFTWINGSPANCLASS_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeAircraftWingspanClass) \
  public:                                      \
    CodeAircraftWingspanClass() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeAircraftWingspanClass,
            char,
            I,
            II,
            III,
            IV,
            V,
            VI,
            OTHER
           )

/*enum class CodeAircraftWingspanClass
{
    I,
    II,
    III,
    IV,
    V,
    VI,
    OTHER,
    NONE
};*/

#endif // CODEAIRCRAFTWINGSPANCLASS_H
