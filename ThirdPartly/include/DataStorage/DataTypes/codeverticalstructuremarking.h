#ifndef CODEVERTICALSTRUCTUREMARKING_H
#define CODEVERTICALSTRUCTUREMARKING_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeVerticalStructureMarking) \
  public:                                      \
    CodeVerticalStructureMarking() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeVerticalStructureMarking,
            char,
            CHEQUERED,
            FLAG,
            HBANDS,
            MARKERS,
            MONOCOLOUR,
            OTHER,
            VBANDS
           );


#endif // CODEVERTICALSTRUCTUREMARKING_H
