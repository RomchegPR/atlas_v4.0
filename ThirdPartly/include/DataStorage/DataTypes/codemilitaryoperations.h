#ifndef CODEMILITARYOPERATIONS_H
#define CODEMILITARYOPERATIONS_H

#define BETTER_ENUMS_STRICT_CONVERSION
#define BETTER_ENUMS_DEFAULT_CONSTRUCTOR(CodeMilitaryOperations) \
  public:                                      \
    CodeMilitaryOperations() = default;
#include "Utils/enum.h"

BETTER_ENUM(
            CodeMilitaryOperations,
            char,
            CIVIL,
            JOINT,
            OTHER,
            MIL
           );

#endif // CODEMILITARYOPERATIONS_H
