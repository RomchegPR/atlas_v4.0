#ifndef GEOMETRYHELPER_H
#define GEOMETRYHELPER_H

#include <glm/glm.hpp>
#include <QVector>
#include "GeoMath/GeoProjection.h"

class CGeometryHelper
{
public:
    CGeometryHelper();
    //Function to create Lines
    QVector < glm::vec3 > GetVerticalLine(const CGeoPoint& p1, const int AltMax, const int AltMin = 0) const;
    QVector < glm::vec3 > GetHorizontalLine(const CGeoPoint& p1, const CGeoPoint& p2, const int AltMax, const int AltMin = 0) const;
    QVector < glm::vec3 > GetLinesForSurface(const QVector < CGeoPoint > vectorPoint, const int height) const;
    QVector < glm::vec3 > GetLinesForSurface(const QVector < CGeoPoint > vectorPoint, const int AltMax, const int AltMin) const;

    //Function to create some objects or Sides (triangles)
    QVector < glm::vec3 > GetTriaglesForCube(const CGeoPoint geoPoint, const double& altMax, const int& sizeOfCube) const; // Creates Cube
    QVector < glm::vec3 > GetTrianglesVec3ForSurface(const QVector < CGeoPoint >& vectorGeopoint, const double& altMax, const double& altMin); //gets all Triangles to create AirSpace

    QVector < glm::vec3 > GetTrianglesForVerticalRectangle(CDecartPoint& p1, CDecartPoint& p2, const int& AltMax, const int& AltMin) const;
    QVector < glm::vec3 > GetTrianglesForVerticalRectangle(CDecartPoint& p1, CDecartPoint& p2, const int& AltMax1, const int& AltMin1, const int& AltMax2, const int& AltMin2) const;
    QVector < glm::vec3 > GetTrianglesForHorizontalSide(QVector < CDecartPoint >& vectorDecartPoint, const int& AltMax, const int& AltMin) const;
    QVector < glm::vec3 > GetTrianglesForVerticalSide(QVector < CDecartPoint >& vectorDecartPoint, const int& AltMax, const int& AltMin);

    //Function to convert
    QVector < CDecartPoint >        VectorGeopoint2VectorMercatorPoint(const QVector < CGeoPoint > &vectorGeoPoint) const;
    glm::vec3                       Geocentric2Vec3(const CGeocentricPoint& point) const;
    CGeocentricPoint                Mercator2Geocentric(const CDecartPoint &mercatorPoint, const int& altitude) const;
    QVector < glm::vec3 >           VectorGeocentric2VectorVec3(const QVector < CGeocentricPoint >& inVector) const;
    QVector < CGeocentricPoint >    TriangleMercator2TriangleGeocentric(const CDecartPoint &mercatorPointA,
                                                                                           const CDecartPoint &mercatorPointB,
                                                                                           const CDecartPoint &mercatorPointC,
                                                                                           const int height) const;
    QVector < glm::vec3 > GetTriangleHorizontalSide(const CDecartPoint &mercatorPointA,
                                                                 const CDecartPoint &mercatorPointB,
                                                                 const CDecartPoint &mercatorPointC,
                                                                 const int &AltMax, const int &AltMin) const;
    QVector < glm::vec3 > GetTriangleHorizontalSide(const CDecartPoint &mercatorPointA,
                                                                 const CDecartPoint &mercatorPointB,
                                                                 const CDecartPoint &mercatorPointC,
                                                                 const int& height) const;

    QVector < glm::vec3 > GetTrianglesForSide(QVector < CDecartPoint > &vectorDecartPoint, const int& AltMax, const int& AltMin, const double& distanceBetweenPoints);
    void CheckDistanceBetweenPoints(QVector < CDecartPoint > &vectorMercatorPoints, const double& distance);
    void AddIntermediatePoint(QVector < CDecartPoint > &vectorDecartPoint, const int index);
    CDecartPoint CalcMiddlePoint(const CDecartPoint& p1, const CDecartPoint& p2) const;
};

#endif // GEOMETRYHELPER_H
