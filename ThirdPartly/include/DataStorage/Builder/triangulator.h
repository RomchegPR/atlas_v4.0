#ifndef TRIANGULATOR_H
#define TRIANGULATOR_H

#include <Builder/triangle.h>
#include <QVector>
#include <glm/glm.hpp>
#include "Utils/GeoHelper.h"
typedef QList<int> ILine;
typedef QList<ILine> SILine;

class Trianglator
{
private:
    QVector < QVector < CDecartPoint > >    trianglesVertexes;      // Треугольники в вершинах
    QVector < CDecartPoint >                vertexes;               // все вершины участвующие в триангуляции
    QVector < QVector <int> >               trianglesIndexes;       // Индексы вершин треугольников
    QVector < QVector <int> >               edges;

public:
    // Функция трианлуляции полигона
    bool DoTriangulate(const QVector < CDecartPoint > &vctVertsSource, QVector < QVector <int> >  *segments = NULL);
    QVector < QVector < CDecartPoint > > getIndexes();
    QVector <CDecartPoint>               getVerts() const;
    QVector < QVector < CDecartPoint > > getTrianglesVertexes() const;
    void clearAll();
    QVector < QVector <int> > createSegments(int amountVertexes);
};

#endif // TRIANGULATOR_H
