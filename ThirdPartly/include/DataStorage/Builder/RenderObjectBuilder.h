#ifndef RENDEROBJECTBUILDER_H
#define RENDEROBJECTBUILDER_H

#include "Entities/RenderObject/RenderObject.h"
#include "Entities/BaseEntity.h"
#include "Entities/geometry.h"
#include "GeometryHelper.h"

class RenderObjectBuilder
{
protected:
    RenderObject*           m_pRenderObject;
    BaseEntity*             m_pObject;
    double                  m_DistanceBetweenPoints;
    CGeometryHelper         m_GeometryHelper;

public:
    RenderObjectBuilder();

    virtual void            CalcSurface(){}
    virtual void            CalcLines(){}
    virtual void            CalcPoints(){}
    virtual bool            IsCorrectType(){}

    void                    convertPointsToGL();
    void                    calcNormals();

    virtual RenderObject* getRenderObject(){ return m_pRenderObject; }

    BaseEntity *getObject() const;
    void setObject(BaseEntity *object);
};

#endif // RENDEROBJECTBUILDER_H
