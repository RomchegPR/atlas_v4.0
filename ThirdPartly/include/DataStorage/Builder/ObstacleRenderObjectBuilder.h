#ifndef OBSTACLERENDEROBJECTBUILDER_H
#define OBSTACLERENDEROBJECTBUILDER_H

#include "RenderObjectBuilder.h"
#include "GeoMath/ReferenceEllipsoid.h"
#include "Entities/VerticalStructure.h"

class CObstacleRenderObjectBuilder : public RenderObjectBuilder
{
private:
    VerticalStructure*          m_pObstacle;
    int                         m_CubeSize;
    int                         m_HeightRate;
    CGeodeticPoint              m_GeodeticPoint;
    QVector < QPointF >         m_Points;
    QVector < glm::vec3 >       m_CoordsCube;

private:


public:
    CObstacleRenderObjectBuilder();
    void CalcLines();
    void CalcSurface();
    bool IsCorrectType();
};

#endif // OBSTACLERENDEROBJECTBUILDER_H
