#ifndef DIRECTOR_H
#define DIRECTOR_H
#include "RenderObjectBuilder.h"
#include "Entities/BaseEntity.h"
#include "datastoragedll_global.h"

class DATASTORAGEDLLSHARED_EXPORT Director
{
public:

    RenderObject* calcRenderData(RenderObjectBuilder & builder, BaseEntity* object);
    RenderObject* getRenderObject();

public:
    Director();
};

#endif // DIRECTOR_H
