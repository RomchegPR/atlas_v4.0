#ifndef AIRPORT_H
#define AIRPORT_H

#include <QMap>
#include <QString>
#include <QStringList>
#include "Builder/ProcedureBuilder/AirPort/WayPoint.h"
#include "glm/glm.hpp"
#include "Builder/ProcedureBuilder/AirPort/Route.h"
//#include "include/Ray.h"

/********************************

Class:		AirPort

Purpose:	Airport is main class which contains
            Routes and colors for them.


            This class is required for reading
            the input data for the Routes
            and common management of them
            (drawing, color change, choosing etc.)

            to correct working:
            1) fillData
            2) init
            3) draw

********************************/

#define InputAirportData QMap < QString, QVector < QVector < Waypoint > > >

class AirPort
{
public:

    AirPort();

    void                fillData                ( InputAirportData &airPoints );

    void                init                    ( );

    void                draw                    ( );
    void                draw                    ( QString name );
    void                draw                    ( QString name,
                                                  int id );

//    QString             checkClickRoute     ( Ray &ray,
//                                                  QSet < QString > &NameObj );

//    bool                checkTriangle           ( Hall Hall,
//                                                  int i1,
//                                                  int i2,
//                                                  int i3,
//                                                  Ray &ray );

    bool                addCheckedRoute     ( QString name );

    void                clear                   ();

    void                setName                 ( const QString &value );
    void                setDrawMask             ( const HallBitmask &value );
    void                setHeightRate           ( const float value );
    void                setFillTrancparency     ( float val );

    QString             getName                 ( ) const;
    HallBitmask         getDrawMask             ( ) const;
    QStringList         getRouteNames       ( ) const;
    float               getHeightRate           ( ) const;
    Hall*               getCursorOnHall         ( ) ;
    float               getFillTrancparency     ( );

private:
    QString                                 mName;
    QMap <RouteType, glm::vec3 >            mRouteColors;         // Example - STAR = yellow
    HallBitmask                             mDrawMask;                // needs to saving elementsToDraw (Headers.h)
    glm::vec3                               mColorForChecked;         // color for Route, that clicked
    Hall                                    mCursorOnHall;

    static float                            TRANCPARENCY_HALL;


public:
    QVector <Route >                        mRoutes;
    float                                   mHeightRate;


};

#endif // AIRPORT_H
