#ifndef WAYPOINT_H
#define WAYPOINT_H

#include "Utils/Headers.h"
#include <glm/glm.hpp>
#include <QMap>
#include <QVector>

struct Waypoint{
    double x,y,z, minAlt, maxAlt, mCourse, speed;
    double lat, lon;
    GLboolean mFlyOver;
    QString mRouteName;
    QString mName;
    RouteMode mode;
    RouteType type;

    Waypoint& operator=(glm::vec3& vec3){
        this->x = vec3.x;
        this->y = vec3.y;
        this->z = vec3.z;
        return *this;
    }
};

#define StrStrMap QVector < QMap < QString, QString> >

struct RouteData{
    QString name;
    StrStrMap points;
};

#endif // WAYPOINT_H
