/*******************************************Описание CNavCalc**************************************************************
 * Класс предназначен для выполнения основных навигационных расчетов, позволяющих построить траекторию полета ВС, таких
 * как перевод приборной скорости в истинную, расчет радиуса разворота, линейного упреждения разворота, перевода основных
 * величин из одной системы мер в другую и т.д.
 * Решение прямой и обратной геодезических задач основано на алгоритме Тадеуша Винсенти.
 * ***********************************************************************************************************************/

#ifndef NAVCALC_H
#define NAVCALC_H

#include <GeoMath/GeoProjection.h>
#include <GeoMath/GeoPoint.h>
#include <QVector>
#include "Measures/measures.h"

// Класс NavCalc для выполнения навигационных расчетов (реализован как синглтон Майерса)
class CNavCalc
{
private:
        CReferenceEllipsoid m_Ellipsoid;

    CNavCalc() {}
    ~CNavCalc() {}
    CNavCalc(const CNavCalc&);

    CNavCalc& operator = (const CNavCalc);

public:
        CGeoProjection m_GeoProj;
        static CNavCalc& GetInstance();
//    {
//        static CNavCalc instance;
//        return instance;
//    }


///////////////////////////////////////////
    //Демонстрация расчетов
    void NavCalcDemo ();
//////////////////////////////////////////

///////////////////////////////////////////
    //Геодезические расчеты
///////////////////////////////////////////

    // Расчет координат точки по заданным азимуту и дальности (CPolarPoint) из заданной географической точки (прямая геодезическая задача) по алгоритму Винсенти
    CGeoPoint SolveDirectTask(const CGeoPoint& GeoPointStart, const CPolarPoint& PolarPoint);

    // Расчет азимута (в радианах) и дальности (в метрах) между двумя точками по их географическим координатам (обратная геодезическая задача) по алгоритму Винсенти
    CPolarPoint SolveInverseTask(const CGeoPoint& GeoPointStart, const CGeoPoint& GeoPointEnd);

    // Расчет значения схождения меридианов (разница путевых углов для двух точек ортодромии, проходящей через эти точки)
	float CalcMeridiansConvergence_rad(const CGeoPoint& GeoPointStart, const CGeoPoint& GeoPointEnd);

    //Расчет точек касания и центральной точки дуги для заданного радиуса в метрах и трех исходных точек
	QVector<CGeoPoint> CalcBaseTurnPoints(const CGeoPoint& FirstPoint, const CGeoPoint& SecondPoint, const CGeoPoint& ThirdPoint, const CDistance& Radius);

    //Расчет точек разворота
	QVector<CGeoPoint> CalcTurnPoints(const QVector<CGeoPoint>& vecWayPoints, const CSpeed& IAS, const CAltitude& Height, const int dBank_deg = 25.0, const bool bFlyOver = false, const bool bDFterminator = false);

    //Расчет промежуточных точек для построения дуги разворота
	QVector<CGeoPoint> CalcArcPoints(const CGeoPoint& CenterPoint, const CGeoPoint& StartPoint, const CGeoPoint& EndPoint, const bool bRightTurn, const float dStepAngle_deg = 3.0);

    //Расчет длины дуги в метрах
	CDistance CalcArcLength(const CGeoPoint& CenterPoint, const CGeoPoint& StartPoint, const CGeoPoint& EndPoint);
	CDistance CalcArcLength(const float dStartAzimuth_rad, const float dEndAzimuth_rad, const CDistance& Radius);

    //Расчет угловой величины дуги в радианах
	float CalcArcAngle_rad(const CGeoPoint& CenterPoint, const CGeoPoint& StartPoint, const CGeoPoint& EndPoint);
	float CalcArcAngle_rad(const float dStartAzimuth_rad, const float dEndAzimuth_rad);

    //Расчет номинальной траектории ожидания
	QVector<CGeoPoint> CalcNominalHoldingTrajectory(const CGeoPoint& HoldingPoint, const float dHoldingCourse_rad, const CAltitude& MaxAltitude, const bool bRightTurn = true);

    //Расчет точки пересечения двух участков заданных точками

    //Расчет точки пересечения двух участков заданных точками и азимутами

    //Расчет промежуточных точек ортодромии

    //Расчет точек вертекса ортодромии

    //Расчет точек пересечения двух дальностей

    //Расчет точки траверза


///////////////////////////////////////////
	//Расчеты международной стандартной атмосферы (МСА)
///////////////////////////////////////////

	//Расчет ускорения свободного падения для заданной в метрах высоты
	float CalcAcceleration (const CAltitude& Altitude);

	//Расчет давления воздуха на заданной в метрах высоте, с учетом отклонения от МСА (для высот от 0 до 20000 м)
	float CalcPressure(const CAltitude& Altitude, const float dTemperatureVaritaion = 0.0);

	//Расчет температуры воздуха в Кельвинах на заданной в метрах высоте
	float CalcTemp(const CAltitude& Altitude, const float dTemperatureVaritaion = 0.0);

	//Перевод геопотенциальной высоты в геометрическую
	CAltitude Convert2GeometricalAlt(const CAltitude& GeopotentialAltitude);

	//Перевод геометрической высоты в геопотенциальную
	CAltitude Convert2GeopotentialAlt(const CAltitude& GeometricalAltitude);


///////////////////////////////////////////
    //Расчеты параметров разворотов
///////////////////////////////////////////
	//Перевод приборной воздушной скорости (IAS) в истинную (TAS) в км/ч, исходя из высоты над уровнем моря в метрах и отклонением температуры от стандартной атмосферы (в градусах) без учета сжимаемости
	CSpeed CalcTrueAirSpeed(const CSpeed& IndicatedAirSpeed, const CAltitude& Altitude, const int iTemperatureVariation_deg = 0);

	//Перевод приборной воздушной скорости (IAS) в истинную (TAS) в км/ч, исходя из высоты над уровнем моря в метрах и отклонением температуры от стандартной атмосферы (в градусах) с учетом сжимаемости
	CSpeed CalcTrueAirSpeedCompression(const CSpeed& IndicatedAirSpeed, const CAltitude& Altitude, const int iTemperatureVariation_deg = 0);

    //Расчет угловой скорости разворота для заданных значений истинной скорости и угла крена (в градусах в секунду)
	float CalcAngularTurnSpeed(const CSpeed& TrueAirSpeed, const float dBankAngle_deg = 25.0);

    //Расчет радиуса разворота (в метрах)
	CDistance CalcTurnRadius(const CSpeed& TrueAirSpeed, const float dBankAngle_deg = 25.0);

    //Расчет линейного упреждения разворота (ЛУР) в метрах
	CDistance CalcLTA(const CDistance& TurnRadius, const float fTurnAngle_deg);
	CDistance CalcLTA(const CGeoPoint& FirstPoint, const CGeoPoint& SecondPoint, const CGeoPoint& ThirdPoint, const CDistance& TurnRadius);

    //Расчет угла разворота (изменения путевого угла в точке разворота) в радианах - с учетом схождения меридианов
	float CalcTurnAngle(const CGeoPoint& FirstPoint, const CGeoPoint& SecondPoint, const CGeoPoint& ThirdPoint);
    //Расчет угла разворота (изменения путевого угла в точке разворота) в радианах - без учета схождения меридианов
	float CalcTurnAngle(const float dTrueDirectionStart_rad, const float dTrueDirectionEnd_rad);

    //Расчет градиента набора или снижения высоты в процентах
	float CalcGradient(const CDistance& DistanceChange, const CAltitude& HeightChange);

    //Расчет стандартного ветра ИКАО в км/ч для заданной высоты в метрах
	CSpeed CalcWindICAO(const CAltitude& Altitude);


///////////////////////////////////////////
    //Пересчеты величин из разных систем мер
///////////////////////////////////////////

    //Перевод футов в метры
	float ConvertFeet2Meters(const float dFeet);

    //Перевод метров в футы (для высот)
	float ConvertMeters2Feet(const float dMeters);

    //Перевод эшелонов полета (FL) в футы (для высот)
	float ConvertFlightLevel2Feet(const int iFlightLevel);

    //Перевод футов в эшелоны полета (FL) с округлением до ближайшего эшелона ПВП или ППП (для высот)
	int ConvertFeet2FlightLevel(const float dFeet);

    //Перевод эшелонов полета (FL) в метры (для высот)
        float ConvertFlightLevel2Meters(const int iFlightLevel);

    //Перевод метров в эшелоны полета (FL) (для высот)
	int ConvertMeters2FlightLevel(const float dMeters);

    //Перевод узлов (kt) в км/ч или морских миль (nm) в километры (для расстояний и скоростей)
	float ConvertKnots2Kmh(const float dKnots);

    //Перевод км/ч  в узлы (kt) или километров в морские мили (nm) (для расстояний и скоростей)
	float ConvertKmh2Knots(const float dKmh);

    //Перевод морских миль (nm) в метры (для расстояний)
	float ConvertNauticalMiles2Meters(const float dNauticalMiles);

    //Перевод метров в морские мили (nm) (для расстояний)
	float ConvertMeters2NauticalMiles(const float dMeters);

    //Перевод км/ч в м/с (для вертикальных и горизонтальных скоростей)
	float ConvertKmh2MetersPerSecond(const float dKmh);

    //Перевод м/с в км/ч (для вертикальных и горизонтальных скоростей)
	float ConvertMetersPerSecond2Kmh(const float dMetersPerSecond);

    //Перевод м/с в ft/min (для вертикальных скоростей)
	float ConvertMetersPerSecond2FeetPerMinute(const float dMetersPerSecond);

    //Перевод ft/min в м/с (для вертикальных скоростей)
	float ConvertFeetPerMinute2MetersPerSecond(const float dFeetPerMinute);

};

#endif // NAVCALC_H
