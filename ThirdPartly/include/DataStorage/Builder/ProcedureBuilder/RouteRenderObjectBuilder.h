#ifndef ROUTERENDEROBJECTBUILDER_H
#define ROUTERENDEROBJECTBUILDER_H

#include "Builder/RenderObjectBuilder.h"
#include "Builder/ProcedureBuilder/AirPort/WayPoint.h"
#include "Builder/ProcedureBuilder/AirPort/Hall.h"
#include "Builder/ProcedureBuilder/AirPort/Turn.h"
#include "Entities/Procedure.h"
#include "Measures/measures.h"

class RouteRenderObjectBuilder : public RenderObjectBuilder
{
public:
    RouteRenderObjectBuilder();

    void CalcLines();
    void CalcSurface();
    void CalcPoints();
    bool IsCorrectType();

private:

    Procedure*               m_pProcedure; // from dataStorage

    // структура для описания точки осевой линии маршрута
    struct CProcedurePoint
    {
        CProcedurePoint() {}

        CProcedurePoint(CGeoPoint geoPoint, CAltitude maxAlt, CAltitude minAlt){
            m_DesignatedPoint.setPoint(geoPoint);
            m_fAltitudeMax_m = maxAlt;
            m_fAltitudeMin_m = minAlt;
        }

        uint                m_iWaypointIndex;

        CAltitude           m_fAltitudeMax_m;
        CAltitude           m_fAltitudeMin_m;

        bool                b_FlyOver;

        DesignatedPoint     m_DesignatedPoint;
    };

    QVector<CProcedurePoint> m_VecProcPoints; // main Points of Procedure
    QVector<CProcedurePoint> m_FullRoutePointsCentral;      // Points with turns
    QVector<CProcedurePoint> m_FullRoutePointsLeft;
    QVector<CProcedurePoint> m_FullRoutePointsRight;

    void SetHeights(SegmentLeg* curSegment, CProcedurePoint& ProcPoint);
    void UpdateProcedure(const int& iIAS, const int& iBank, const int& iHeight);
    QVector<CProcedurePoint> VecGeoPoint2VecProcedurePoint(QVector<CGeoPoint> &vecGeoPoint,
                                                           CAltitude &startHeightMax,
                                                           CAltitude &endHeightMax,
                                                           CAltitude &startHeightMin,
                                                           CAltitude &endHeightMin
                                                           );


    // вершины участка процедуры
    struct HallVertexes {                       //1 Ld  left of the box            2-------6     / \Y
         //vertexes of Hall;                    // Rd                            / |     /|      |
        glm::vec3 a, a0, a1, a2, a3;            // RU                            /  |    / |      |   /\Z
        glm::vec3 b, b0, b1, b2, b3;            // LU                           3-------7  |      |  /
                                                            //                  | a|    | b|      | /
    };                                          //2 LD down of the box          |  1----|--5   ---0 ---->X
                                                // RD                           | /     | /      /|
                                                // RU                           |/      |/      / |
                                                // LD                           0-------4

    void CheckPoints();
    void AddHall(CProcedurePoint &startPoint, CProcedurePoint &centerPoint, CProcedurePoint &endPoint, glm::vec3 &FirstHallNormal, bool isLastPart);
    void FillHallIndices();
    void addTriangleIndices(int a, int b, int c);
    HallVertexes SetHallPoints(CProcedurePoint &startPoint, CProcedurePoint &centerPoint, CProcedurePoint &endPoint, glm::vec3 &firstHallNormal, bool isLastPart);
    HallVertexes SetHallPoints(CProcedurePoint &startPoint, CProcedurePoint &endPoint, glm::vec3 &firstHallNormal);

    bool isNullVec(glm::vec3 vec);

    //Рассчет линий участка
    void CalcHallLines(HallVertexes &hall);
    void CalcHallTrajectoryLine(HallVertexes &hall);

    // количество индексов для всех участков
    int m_iIndexID;
    int m_iIndexIDHallLinesObjectType;
    int m_iIndexIDHallLinesTrajectoryObjectType;



};

#endif // ROUTERENDEROBJECTBUILDER_H
