#ifndef AIRSPACERENDEROBJECTBUILDER_H
#define AIRSPACERENDEROBJECTBUILDER_H

#include "RenderObjectBuilder.h"
#include "Entities/Airspace.h"

class AirSpaceRenderObjectBuilder : public RenderObjectBuilder
{
private:
    Airspace*   m_pAirspace;
    bool        IsAirspaceType();

public:
    AirSpaceRenderObjectBuilder();
    void        CalcLines();
    void        CalcSurface();
    bool        IsCorrectType();
};

#endif // AIRSPACERENDEROBJECTBUILDER_H
