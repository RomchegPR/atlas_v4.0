#ifndef PROCEDURETRANSITIONLEG_H
#define PROCEDURETRANSITIONLEG_H

#include <Entities/BaseEntity.h>

// Неиспользуемый класс

class ProcedureTransitionLeg : public BaseEntity
{
private:
    int seqNumberARINC;
public:
    ProcedureTransitionLeg();
    ProcedureTransitionLeg(QString uuid);
    virtual ~ProcedureTransitionLeg();
    
    int getSeqNumberARINC() const;
    void setSeqNumberARINC(int value);
    
private:
    void init(int seqNumberARINC = HUGE_VALUE);
};

#endif // PROCEDURETRANSITIONLEG_H
