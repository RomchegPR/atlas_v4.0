#ifndef GM_SURFACE_H
#define GM_SURFACE_H

#include <QVector>
#include <GeoMath/GeoPoint.h>

class GM_Surface {

public:

    GM_Surface();
    GM_Surface(const QVector<CGeoPoint> &basePointList);

    QVector<CGeoPoint>          getBasePointList() const;
    void                        setBasePointList(const QVector<CGeoPoint> &BasePointList);

private:

    QVector<CGeoPoint>          m_BasePointList;
};

#endif // GM_SURFACE_H
