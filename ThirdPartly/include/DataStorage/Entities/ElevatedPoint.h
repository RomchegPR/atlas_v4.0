#ifndef ELEVATEDPOINT_H
#define ELEVATEDPOINT_H

#include "DataTypes/ValDistanceVerticalType.h"
#include "DataTypes/ValDistanceSignedType.h"
#include "Point.h"

class ElevatedPoint : public Point
{
public:
    ElevatedPoint();
    ElevatedPoint(QString uuid);
    virtual ~ElevatedPoint(){}

    ValDistanceVerticalType *getElevation() const;
    void setElevation(ValDistanceVerticalType *value);

    ValDistanceSignedType *getGeoidUndulation() const;
    void setGeoidUndulation(ValDistanceSignedType *value);

    ValDistanceType *getVerticalAccuracy() const;
    void setVerticalAccuracy(ValDistanceType *value);

private:
    ValDistanceVerticalType *elevation;
    ValDistanceSignedType *geoidUndulation;
    ValDistanceType *verticalAccuracy;
    //CodeVerticalDatumType verticalDatum; //Не нуждаемся в использовании
    init(){
        elevation = new ValDistanceVerticalType();
        geoidUndulation = new ValDistanceSignedType();
        verticalAccuracy = new ValDistanceType();
    }
};

#endif // ELEVATEDPOINT_H
