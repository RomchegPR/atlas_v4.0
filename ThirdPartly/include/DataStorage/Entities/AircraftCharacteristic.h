#ifndef AIRCRAFTCHARACTERISTIC_H
#define AIRCRAFTCHARACTERISTIC_H

#include <Entities/BaseEntity.h>
#include <DataTypes/CodeAircraft.h>
#include <DataTypes/CodeAircraftEngine.h>
#include <DataTypes/CodeAircraftEngineNumber.h>
#include <DataTypes/CodeAircraftCategory.h>
#include <DataTypes/CodeValueInterpretation.h>
#include <DataTypes/CodeAircraftWingspanClass.h>
#include <DataTypes/CodeWakeTurbulence.h>
#include <DataTypes/CodeNavigationEquipment.h>
#include <DataTypes/CodeNavigationSpecification.h>
#include <DataTypes/CodeEquipmentAntiCollision.h>
#include <DataTypes/CodeCommunicationMode.h>
#include <DataTypes/CodeTransponder.h>

// неиспользуемый класс

class AircraftCharacteristic : public BaseEntity
{
private:
    CodeAircraft type;
    CodeAircraftEngine engine;
    CodeAircraftEngineNumber numberEngine;
    int typeAircraftICAO;
    CodeAircraftCategory aircraftLandingCategory;
    double wingSpan;
    CodeValueInterpretation wingSpanInterpretation;
    CodeAircraftWingspanClass classWingSpan;
    double weight;
    CodeValueInterpretation weightInterpretation;
    int passengers;
    CodeValueInterpretation passengersInterpretation;
    double speed;
    CodeValueInterpretation speedInterpretation;
    CodeWakeTurbulence wakeTurbulence;
    CodeNavigationEquipment navigationEquipment;
    CodeNavigationSpecification navigationSpecification;
    bool verticalSeparationCapability;
    CodeEquipmentAntiCollision antiCollisionAndSeparationEquipment;
    CodeCommunicationMode communicationEquipment;
    CodeTransponder surveillanceEquipment;

public:
    AircraftCharacteristic();
    AircraftCharacteristic(QString uuid);
    virtual ~AircraftCharacteristic();

    CodeAircraftWingspanClass getClassWingSpan() const;
    void setClassWingSpan(const CodeAircraftWingspanClass &value);

    CodeAircraft getType() const;
    void setType(const CodeAircraft &value);

    CodeAircraftEngine getEngine() const;
    void setEngine(const CodeAircraftEngine &value);

    CodeAircraftEngineNumber getNumberEngine() const;
    void setNumberEngine(const CodeAircraftEngineNumber &value);

    int getTypeAircraftICAO() const;
    void setTypeAircraftICAO(int value);

    CodeAircraftCategory getAircraftLandingCategory() const;
    void setAircraftLandingCategory(const CodeAircraftCategory &value);

    double getWingSpan() const;
    void setWingSpan(double value);

    CodeValueInterpretation getWingSpanInterpretation() const;
    void setWingSpanInterpretation(const CodeValueInterpretation &value);

    double getWeight() const;
    void setWeight(double value);

    CodeValueInterpretation getWeightInterpretation() const;
    void setWeightInterpretation(const CodeValueInterpretation &value);

    int getPassengers() const;
    void setPassengers(int value);

    CodeValueInterpretation getPassengersInterpretation() const;
    void setPassengersInterpretation(const CodeValueInterpretation &value);

    double getSpeed() const;
    void setSpeed(double value);

    CodeValueInterpretation getSpeedInterpretation() const;
    void setSpeedInterpretation(const CodeValueInterpretation &value);

    CodeWakeTurbulence getWakeTurbulence() const;
    void setWakeTurbulence(const CodeWakeTurbulence &value);

    CodeNavigationEquipment getNavigationEquipment() const;
    void setNavigationEquipment(const CodeNavigationEquipment &value);

    CodeNavigationSpecification getNavigationSpecification() const;
    void setNavigationSpecification(const CodeNavigationSpecification &value);

    bool getVerticalSeparationCapability() const;
    void setVerticalSeparationCapability(bool value);

    CodeEquipmentAntiCollision getAntiCollisionAndSeparationEquipment() const;
    void setAntiCollisionAndSeparationEquipment(const CodeEquipmentAntiCollision &value);

    CodeCommunicationMode getCommunicationEquipment() const;
    void setCommunicationEquipment(const CodeCommunicationMode &value);

    CodeTransponder getSurveillanceEquipment() const;
    void setSurveillanceEquipment(const CodeTransponder &value);

private:
    void init(
                CodeAircraft type = CodeAircraft::OTHER,
                CodeAircraftEngine engine = CodeAircraftEngine::OTHER,
                CodeAircraftEngineNumber numberEngine = CodeAircraftEngineNumber::OTHER,
                int typeAircraftICAO = HUGE_VALUE,
                CodeAircraftCategory aircraftLandingCategory = CodeAircraftCategory::OTHER,
                double wingSpan = INF,
                CodeValueInterpretation wingSpanInterpretation = CodeValueInterpretation::OTHER,
                CodeAircraftWingspanClass classWingSpan = CodeAircraftWingspanClass::OTHER,
                double weight = INF,
                CodeValueInterpretation weightInterpretation = CodeValueInterpretation::OTHER,
                int passengers = HUGE_VALUE,
                CodeValueInterpretation passengersInterpretation = CodeValueInterpretation::OTHER,
                double speed = INF,
                CodeValueInterpretation speedInterpretation = CodeValueInterpretation::OTHER,
                CodeWakeTurbulence wakeTurbulence = CodeWakeTurbulence::OTHER,
                CodeNavigationEquipment navigationEquipment = CodeNavigationEquipment::OTHER,
                CodeNavigationSpecification navigationSpecification = CodeNavigationSpecification::OTHER,
                bool verticalSeparationCapability = false,
                CodeEquipmentAntiCollision antiCollisionAndSeparationEquipment = CodeEquipmentAntiCollision::OTHER,
                CodeCommunicationMode communicationEquipment = CodeCommunicationMode::OTHER,
                CodeTransponder surveillanceEquipment = CodeTransponder::OTHER
            );
};

#endif // AIRCRAFTCHARACTERISTIC_H
