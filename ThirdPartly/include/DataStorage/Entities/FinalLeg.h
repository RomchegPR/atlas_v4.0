#ifndef FINALLEG_H
#define FINALLEG_H
#include <Entities/ApproachLeg.h>
#include <DataTypes/CodeFinalGuidance.h>
#include <DataTypes/CodeApproachGuidance.h>
#include <DataTypes/CodeSide.h>
#include <DataTypes/CodeRelativePosition.h>

// Сегмент апроача (Finall)

class FinalLeg : public ApproachLeg
{
private:
    CodeFinalGuidance guidanceSystem;
    CodeApproachGuidance landingSystemCategory;
    double minimumBaroVnavTemperature;
    bool rnpDMEAuthorized;
    double courseOffsetAngle;
    CodeSide courseOffsetSide;
    double courseCentrelineDistance;
    double courseOffsetDistance;
    CodeRelativePosition courseCentrelineIntersect;

public:
    FinalLeg();
    FinalLeg(QString uuid);
    virtual ~FinalLeg();

    CodeFinalGuidance getGuidanceSystem() const;
    void setGuidanceSystem(const CodeFinalGuidance &value);

    CodeApproachGuidance getLandingSystemCategory() const;
    void setLandingSystemCategory(const CodeApproachGuidance &value);

    double getMinimumBaroVnavTemperature() const;
    void setMinimumBaroVnavTemperature(double value);

    bool getRnpDMEAuthorized() const;
    void setRnpDMEAuthorized(bool value);

    double getCourseOffsetAngle() const;
    void setCourseOffsetAngle(double value);

    CodeSide getCourseOffsetSide() const;
    void setCourseOffsetSide(const CodeSide &value);

    double getCourseCentrelineDistance() const;
    void setCourseCentrelineDistance(double value);

    double getCourseOffsetDistance() const;
    void setCourseOffsetDistance(double value);

    CodeRelativePosition getCourseCentrelineIntersect() const;
    void setCourseCentrelineIntersect(const CodeRelativePosition &value);

private:
    void init(
            CodeFinalGuidance guidanceSystem = CodeFinalGuidance::OTHER,
            CodeApproachGuidance landingSystemCategory = CodeApproachGuidance::OTHER,
            double minimumBaroVnavTemperature = INF,
            bool rnpDMEAuthorized = false,
            double courseOffsetAngle = INF,
            CodeSide courseOffsetSide = CodeSide::OTHER,
            double courseCentrelineDistance = INF,
            double courseOffsetDistance = INF,
            CodeRelativePosition courseCentrelineIntersect = CodeRelativePosition::OTHER
            );
};

#endif // FINALLEG_H
