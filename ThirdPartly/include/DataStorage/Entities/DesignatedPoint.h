#ifndef DESIGNATEDPOINT_H
#define DESIGNATEDPOINT_H

#include "Entities/BaseEntity.h"
#include "DataTypes/CodeDesignatedPoint.h"
#include <GeoMath/GeoPoint.h>

// Класс, описывающий геоточку

class DesignatedPoint : public BaseEntity
{
private:
    CodeDesignatedPoint type;
    QString             name;
    QString             designator;
    CGeoPoint           point;
public:
    DesignatedPoint();
    DesignatedPoint(QString uuid);
    virtual ~DesignatedPoint();

    float latitude();
    void setLatitude(float latitude);
    float longitude();
    void setLongitude(float longitude);
    QString getName();
    void setName(const QString& name);
    CodeDesignatedPoint getType();
    void setType(CodeDesignatedPoint type);
    CGeoPoint getPoint() const;
    void setPoint(const CGeoPoint &value);

    QString getDesignator() const;
    void setDesignator(const QString &value);

private:
    void init(
            CodeDesignatedPoint type = CodeDesignatedPoint::OTHER,
            QString name = "",
            CGeoPoint point = CGeoPoint()
            );
};

#endif // DESIGNATEDPOINT_H
