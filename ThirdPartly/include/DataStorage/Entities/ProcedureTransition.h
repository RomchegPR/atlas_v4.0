#ifndef PROCEDURETRANSITION_H
#define PROCEDURETRANSITION_H
#include <QString>
#include <QVector>

#include <Entities/SegmentLeg.h>
#include <Entities/BaseEntity.h>

#include <DataTypes/CodeProcedurePhase.h>
#include "Entities/Curve.h"
// Класс, описывающий путь/геометрию процедуры

class ProcedureTransition : public BaseEntity
{
private:
    QString                     transitionId;
    CodeProcedurePhase          type;
    QString                     instruction;
    double                      vectorHeading; // min 0, max 360
    QVector<SegmentLeg* >       segments;
    Curve                       *trajectory;
public:
    ProcedureTransition();
    ProcedureTransition(QString uuid);
    virtual ~ProcedureTransition();

    QString                     getTransitionId() const;
    void                        setTransitionId(const QString &value);

    CodeProcedurePhase          getType() const;
    void                        setType(const CodeProcedurePhase &value);

    QString                     getInstruction() const;
    void                        setInstruction(const QString &value);

    double                      getVectorHeading() const;
    void                        setVectorHeading(double value);

    QVector<SegmentLeg *>       getSegments() const;
    void                        setSegments(const QVector<SegmentLeg *> &value);

    QString                     getField(QString fieldName);
    QStringList                 getAttributesName();

    Curve                       *getTrajectory() const;
    void                        setTrajectory(Curve *value);

private:
    void init(QString transitionId = "",
            CodeProcedurePhase type = CodeProcedurePhase::OTHER,
            QString instruction = "",
            double vectorHeading = INF,
            Curve *trajectory = nullptr
            );
};

#endif // PROCEDURETRANSITION_H
