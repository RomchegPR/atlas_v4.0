#ifndef VERTICALSTRUCTUREPART_H
#define VERTICALSTRUCTUREPART_H

#include <QString>
#include <QVector>
#include <QPointF>
#include <GeoMath/GeoPoint.h>

#include "DataTypes/CodeStatusConstruction.h"
#include "DataTypes/CodeColour.h"
#include "DataTypes/CodeVerticalStructureMarking.h"
#include "DataTypes/CodeVerticalStructure.h"
#include "DataTypes/CodeVerticalStructureMaterial.h"
#include "DataTypes/GeometryType.h"
#include "VerticalStructurePartGeometry.h"
#include "../IMetaInfo.h"

class VerticalStructurePart
{
private:

    CodeStatusConstruction          constructionStatus;
    QString                         designator;
    bool                            frangible;
    CodeColour                      markingFirstColour;
    CodeVerticalStructureMarking    markingPattern;
    CodeColour                      markingSecondColour;
    bool                            mobile;
    CodeVerticalStructure           type;
    double                          verticalExtent;
    double                          verticalExtentAccuracy;
    CodeVerticalStructureMaterial   visibleMaterial;
    GeometryType                    geometryType;
    VerticalStructurePartGeometry   *horizontalProjection;

public:

    VerticalStructurePart();
    virtual ~VerticalStructurePart() = default;

    CodeStatusConstruction          getConstructionStatus() const;
    void                            setConstructionStatus(const CodeStatusConstruction &value);

    QString                         getDesignator() const;
    void                            setDesignator(const QString &value);

    bool                            getFrangible() const;
    void                            setFrangible(bool value);

    CodeColour                      getMarkingFirstColour() const;
    void                            setMarkingFirstColour(const CodeColour &value);

    CodeVerticalStructureMarking    getMarkingPattern() const;
    void                            setMarkingPattern(const CodeVerticalStructureMarking &value);

    CodeColour                      getMarkingSecondColour() const;
    void                            setMarkingSecondColour(const CodeColour &value);

    bool                            getMobile() const;
    void                            setMobile(bool value);

    CodeVerticalStructure           getType() const;
    void                            setType(const CodeVerticalStructure &value);

    double                          getVerticalExtent() const;
    void                            setVerticalExtent(double value);

    double                          getVerticalExtentAccuracy() const;
    void                            setVerticalExtentAccuracy(double value);

    CodeVerticalStructureMaterial   getVisibleMaterial() const;
    void                            setVisibleMaterial(const CodeVerticalStructureMaterial &value);

    GeometryType                    getGeometryType() const;
    void                            setGeometryType(const GeometryType &value);


    VerticalStructurePartGeometry   *getHorizontalProjection() const;
    void                            setHorizontalProjection(VerticalStructurePartGeometry *value);

};

#endif // VERTICALSTRUCTUREPART_H
