#ifndef AIRSPACEGEOMETRYCOMPONENT_H
#define AIRSPACEGEOMETRYCOMPONENT_H

#include "DataTypes/CodeAirspaceAggregation.h"

// Неиспользуемый класс

class AirspaceGeometryComponent
{
public:
    AirspaceGeometryComponent();

    CodeAirspaceAggregation getOperation() const;
    void setOperation(const CodeAirspaceAggregation &value);

    int getOperationSeq() const;
    void setOperationSeq(int value);

private:
    CodeAirspaceAggregation operation;
    int operationSeq;
};

#endif // AIRSPACEGEOMETRYCOMPONENT_H
