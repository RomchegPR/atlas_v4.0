#ifndef VERTICALSTRUCTUREPARTGEOMETRY_H
#define VERTICALSTRUCTUREPARTGEOMETRY_H

#include "ElevatedPoint.h"
#include "ElevatedCurve.h"
#include "ElevatedSurface.h"

class VerticalStructurePartGeometry
{

public:

    VerticalStructurePartGeometry();

    ElevatedPoint *getElevatedPoint() const;
    void setElevatedPoint(ElevatedPoint *value);

    ElevatedCurve *getElevatedCurve() const;
    void setElevatedCurve(ElevatedCurve *value);

    ElevatedSurface *getElevatedSurface() const;
    void setElevatedSurface(ElevatedSurface *value);

private:

    ElevatedPoint *elevatedPoint;
    ElevatedCurve *elevatedCurve;
    ElevatedSurface *elevatedSurface;
    init(){
        elevatedPoint = nullptr;
        elevatedCurve = nullptr;
        elevatedSurface = nullptr;
    }
};

#endif // VERTICALSTRUCTUREPARTGEOMETRY_H
