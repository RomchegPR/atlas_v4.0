#ifndef POINT_H
#define POINT_H
#include "BaseEntity.h"
#include "GM_Point.h"
#include "DataTypes/ValDistanceType.h"

class Point : public BaseEntity, public GM_Point
{
public:

    Point();
    Point(QString uuid);
    virtual ~Point(){}

    QString toString() const;

protected:
    ValDistanceType *getHorizontalAccuracy() const;
    void setHorizontalAccuracy(ValDistanceType *value);

private:
    ValDistanceType             *horizontalAccuracy;
    void init(){
        horizontalAccuracy = new ValDistanceType();
    }
};

#endif // POINT_H
