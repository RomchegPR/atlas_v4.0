#ifndef AIRSPACEVOLUME_H
#define AIRSPACEVOLUME_H


#include <QVector>
#include <QPointF>
#include <GeoMath/GeoPoint.h>

#include "DataTypes/CodeVerticalReference.h"
#include "Curve.h"
#include "Surface.h"
#include "../IMetaInfo.h"


class Airspace;

// Класс, описывающий объем воздушного пространства

class AirspaceVolume
{

private:

    double                      lowerLimit{INF};
    CodeVerticalReference       lowerLimitReference{CodeVerticalReference::OTHER};
    double                      maximumLimit{INF};
    CodeVerticalReference       maximumLimitReference{CodeVerticalReference::OTHER};
    double                      minimumLimit{INF};
    CodeVerticalReference       minimumLimitReference{CodeVerticalReference::OTHER};
    double                      upperLimit{INF};
    CodeVerticalReference       upperLimitReference{CodeVerticalReference::OTHER};
    double                      width{INF};

    // Воздушное пространство могут описываться центральной линией, если они коридорного типа.
    Curve                       *centreline;

    // Вектор, хранящий координаты плоскости воздушного пространства
    Surface                     *horizontalProjection;

    Airspace* airspace;

public:

    AirspaceVolume();

    double              getLowerLimit() const;
    void                setLowerLimit(double value);

    CodeVerticalReference getLowerLimitReference() const;
    void                setLowerLimitReference(const CodeVerticalReference &value);

    double              getMaximumLimit() const;
    void                setMaximumLimit(double value);

    CodeVerticalReference getMaximumLimitReference() const;
    void                setMaximumLimitReference(const CodeVerticalReference &value);

    double              getMinimumLimit() const;
    void                setMinimumLimit(double value);

    CodeVerticalReference getMinimumLimitReference() const;
    void                setMinimumLimitReference(const CodeVerticalReference &value);

    double              getUpperLimit() const;
    void                setUpperLimit(double value);

    CodeVerticalReference getUpperLimitReference() const;
    void                setUpperLimitReference(const CodeVerticalReference &value);

    double              getWidth() const;
    void                setWidth(double value);

    Curve               *getCentreline() const;
    void                setCentreline(Curve *value);

    Surface             *getHorizontalProjection() const;
    void                setHorizontalProjection(Surface *value);

    //WTF REALLY?
    Airspace *getAirspace() const;
    void setAirspace(Airspace *value);

};

#endif // AIRSPACEVOLUME_H
