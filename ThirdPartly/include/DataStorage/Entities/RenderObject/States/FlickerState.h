#ifndef FLICKERSTATE_H
#define FLICKERSTATE_H

#include <QObject>
#include "PaintedState.h"

class FlickerState : public PaintedState
{
public:
    FlickerState();
    void exec(RenderObject *renderObject);
};

#endif // FLICKERSTATE_H
