#ifndef RENDEROBJECTSTATE_H
#define RENDEROBJECTSTATE_H

#include <QObject>

class RenderObject;

class RenderObjectState
{
public:
    enum State
    {
        STATE_CREATING,
        STATE_INITING,
        STATE_PAINT_READY,
        STATE_PAINTING,
        STATE_FLICKERING,
        STATE_SHINING
    };

public:
    RenderObjectState();
    virtual void exec(RenderObject *) = 0;
    virtual RenderObjectState* handleInput(State) = 0;
};

#endif // RENDEROBJECTSTATE_H
