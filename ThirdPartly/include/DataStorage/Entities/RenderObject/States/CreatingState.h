#ifndef NOTINITSTATE_H
#define NOTINITSTATE_H

#include <QObject>
#include "RenderObjectState.h"

class CreatingState : public RenderObjectState
{
public:
    CreatingState();
    void exec(RenderObject* renderObject);
    RenderObjectState* handleInput(State);

};

#endif // NOTINITSTATE_H
