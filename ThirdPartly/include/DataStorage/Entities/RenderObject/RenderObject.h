#ifndef RENDEROBJECT_H
#define RENDEROBJECT_H

#include <QMap>

#include "Entities/RenderObject/RenderData.h"

#include "States/RenderObjectState.h"
#include "States/FlickerState.h"
#include "States/CreatingState.h"
#include "States/InitializationState.h"
#include "States/PaintedState.h"
#include "States/ShinedState.h"
#include "States/ReadyState.h"

/********************************

Class:		RenderObject

Purpose:	wrapper class for RenderData,
            use for rendered objects

********************************/

class RenderObject
{

public:

    RenderObject();

    enum RenderObjectType {

        BasicType,
        LinesType,

        HallObjectType,
        HallLinesObjectType,
        HallLinesTrajectoryObjectType,
        HallLinesRamObjectType,
        HallGroundLinesProjectionObjectType,
        HallGroundLinesObjectType

    };

    // Initialize Buffers
    virtual void        init        ( ) {}

    //GL functions switching on/off
    virtual void        paint       ( ) {}

    virtual void        clear       ( ) {
        for(QMap<RenderObjectType, RenderData >::Iterator it = mRenderData.end() - 1; it != mRenderData.begin() - 1; it--){
                it.value().clear();
        }
    }

public:

    QMap < RenderObjectType,  RenderData >         mRenderData;

    void setState(RenderObjectState::State state);
    QString m_Name;

private:
    RenderObjectState* m_State;


};

#endif // RENDEROBJECT_H
