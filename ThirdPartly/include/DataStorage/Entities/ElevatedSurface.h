#ifndef ELEVATEDSURFACE_H
#define ELEVATEDSURFACE_H

#include "DataTypes/ValDistanceVerticalType.h"
#include "DataTypes/ValDistanceSignedType.h"
#include "Surface.h"

class ElevatedSurface : public Surface
{
public:
    ElevatedSurface();
    ElevatedSurface(QString uuid);
    virtual ~ElevatedSurface(){}

    ValDistanceVerticalType *getElevation() const;
    void setElevation(ValDistanceVerticalType *value);

    ValDistanceSignedType *getGeoidUndulation() const;
    void setGeoidUndulation(ValDistanceSignedType *value);

    ValDistanceType *getVerticalAccuracy() const;
    void setVerticalAccuracy(ValDistanceType *value);

private:
    ValDistanceVerticalType *elevation;
    ValDistanceSignedType *geoidUndulation;
    ValDistanceType *verticalAccuracy;
    //CodeVerticalDatumType verticalDatum; //Не нуждаемся в использовании
    init(){
        elevation = new ValDistanceVerticalType();
        geoidUndulation = new ValDistanceSignedType();
        verticalAccuracy = new ValDistanceType();
    }
};

#endif // ELEVATEDSURFACE_H
