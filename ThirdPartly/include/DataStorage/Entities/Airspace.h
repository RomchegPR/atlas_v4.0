#ifndef AIXMAIRSPACE_H
#define AIXMAIRSPACE_H

#include "DataTypes/CodeMilitaryOperations.h"
#include "Entities/AirspaceVolume.h"
#include "DataTypes/CodeAirspace.h"
#include <QString>
#include <QVector>
#include <Entities/BaseEntity.h>
#include <Entities/geometry.h>
#include "../IMetaInfo.h"
// Класс-модель, описывающий воздушное пространство

class Airspace : public BaseEntity
{

private:
    CodeMilitaryOperations              controlType;
    QString                             designator;
    bool                                designatorICAO;
    QString                             localType;
    QString                             name;
    CodeAirspace                        type;
    QString                             upperLowerSeparation;

    // Воздушное пространство может состоять измножества объемов. CTR как пример
    QVector<AirspaceVolume*>            volumes;

    init(){
        name = "None";
        type = CodeAirspace::OTHER;
        localType = "None";
        controlType = CodeMilitaryOperations::OTHER;
        designator = "None";
        upperLowerSeparation = "None";
        designatorICAO = false;
    }

public:
    Airspace();

    bool                                operator ==(const Airspace &rhs) const;

    QString                             getLocalType() const;
    void                                setLocalType(const QString &value);

    CodeMilitaryOperations              getControlType() const;
    void                                setControlType(const CodeMilitaryOperations &value);

    QString                             getDesignator() const;
    void                                setDesignator(const QString &value);

    bool                                getDesignatorICAO() const;
    void                                setDesignatorICAO(bool value);

    QString                             getName() const;
    void                                setName(const QString &value);

    CodeAirspace                        getType() const;
    void                                setType(const CodeAirspace &value);

    QString                             getUpperLowerSeparation() const;
    void                                setUpperLowerSeparation(const QString &value);

    QVector<AirspaceVolume *>           getVolumes() const;
    void                                setVolumes(const QVector<AirspaceVolume *> &value);

    QVector<Geometry>                   getGeometry() const;

};

#endif // AIXMAIRSPACE_H
