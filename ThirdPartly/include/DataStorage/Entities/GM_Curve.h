#ifndef GM_CURVE_H
#define GM_CURVE_H

#include <QVector>
#include <GeoMath/GeoPoint.h>
#include <GeoMath/ReferenceEllipsoid.h>

class GM_Curve {

public:

    GM_Curve();
    GM_Curve(const QVector<CGeoPoint> &basePointList);

    QVector<CGeoPoint>          basePointList() const;
    void                        setBasePointList(const QVector<CGeoPoint> &basePointList);

private:
    QVector<CGeoPoint>          m_basePointList;
};

#endif // GM_CURVE_H
