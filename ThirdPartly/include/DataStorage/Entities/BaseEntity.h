#ifndef BASEENTITY_H
#define BASEENTITY_H

#include <QString>
#include <limits>
#include <QUuid>

#include "Entities/RenderObject/RenderObject.h"

const int HUGE_VALUE = std::numeric_limits<int>::max();
const double INF =  std::numeric_limits<double>::infinity();

// Базовая сущность для всех фич AIXM'a

class BaseEntity
{
protected:
    QUuid                               uuid;
    RenderObject*                       mRenderObject;

public:
    BaseEntity();
    BaseEntity(QString uuid);
    virtual ~BaseEntity();

    bool                                operator ==(const BaseEntity &rhs) const;

    QString                             getUuid() const;
    void                                setUuid(const QString uuid);

    RenderObject*                       getRenderObject() const;
    void                                setRenderObject(RenderObject *renderObject);
//    QVector <> getGeometry();
    virtual QString                     getName(){}
};

#endif // BASEENTITY_H
