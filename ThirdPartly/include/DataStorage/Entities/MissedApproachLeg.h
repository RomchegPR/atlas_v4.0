#ifndef MISSEDAPPROACHLEG_H
#define MISSEDAPPROACHLEG_H
#include <Entities/ApproachLeg.h>
#include <DataTypes/CodeMissedApproach.h>

// Missed Approach сегмент

class MissedApproachLeg: public ApproachLeg
{
private:
    CodeMissedApproach type;
    bool thresholdAfterMAPT;

public:
    MissedApproachLeg();
    MissedApproachLeg(QString uuid);
    virtual ~MissedApproachLeg();

    CodeMissedApproach getType() const;
    void setType(const CodeMissedApproach &value);

    bool getThresholdAfterMAPT() const;
    void setThresholdAfterMAPT(bool value);

private:
    void init(
            CodeMissedApproach type = CodeMissedApproach::OTHER,
            bool thresholdAfterMAPT = false
            );
};
#endif // MISSEDAPPROACHLEG_H
