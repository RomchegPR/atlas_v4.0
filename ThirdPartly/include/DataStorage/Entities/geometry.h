#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <QVector>
#include <QPointF>
#include <GeoMath/GeoPoint.h>
#include "DataTypes/GeometryType.h"
#include "Point.h"
#include "Curve.h"
#include "Surface.h"

class Geometry
{
public:
    Geometry();

    Point                   *getPoint() const;
    void                    setPoint(Point *pPoint);
    bool                    hasPoint() const;

    Curve                   *getCurve() const;
    void                    setCurve(Curve *pCurve);
    bool                    hasCurve() const;

    Surface                 *getSurface() const;
    void                    setSurface(Surface *pSurface);
    bool                    hasSurface() const;

    double                  getMinAlt() const;
    void                    setMinAlt(double value);

    double                  getMaxAlt() const;
    void                    setMaxAlt(double value);

    QString                 toString() const;
private:

    Point                   *m_pPoint;
    Curve                   *m_pCurve;
    Surface                 *m_pSurface;
    double                  minAlt;
    double                  maxAlt;
    init()
    {
        m_pPoint = nullptr;
        m_pCurve = nullptr;
        m_pSurface = nullptr;
    }
};

#endif // GEOMETRY_H
