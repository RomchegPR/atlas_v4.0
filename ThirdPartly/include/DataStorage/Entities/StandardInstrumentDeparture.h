#ifndef STANDARDINSTRUMENTDEPARTURE_H
#define STANDARDINSTRUMENTDEPARTURE_H
#include <Entities/Procedure.h>
#include <QString>

// SID

class StandardInstrumentDeparture : public Procedure
{
private:
    QString designator;
    bool contingencyRoute;
public:
    StandardInstrumentDeparture();
    StandardInstrumentDeparture(QString uuid);
    virtual ~StandardInstrumentDeparture();

    QString getDesignator() const;
    void setDesignator(const QString &value);

    bool getContingencyRoute() const;
    void setContingencyRoute(bool value);
private:
    void init(QString designator = "", bool contingencyRoute = false);
};

#endif // STANDARDINSTRUMENTDEPARTURE_H
