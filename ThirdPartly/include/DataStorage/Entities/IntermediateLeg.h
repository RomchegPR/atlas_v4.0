#ifndef INTERMEDIATELEG_H
#define INTERMEDIATELEG_H
#include <Entities/ApproachLeg.h>

// Intremediate сегмент апроача

class IntermediateLeg : public ApproachLeg
{
public:
    IntermediateLeg();
    IntermediateLeg(QString uuid);
    virtual ~IntermediateLeg();
};

#endif // INTERMEDIATELEG_H
