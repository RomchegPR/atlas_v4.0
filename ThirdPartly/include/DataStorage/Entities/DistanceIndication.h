#ifndef DISTANCEINDICATION_H
#define DISTANCEINDICATION_H

#include <Entities/BaseEntity.h>

// Неиспользуемый класс

class DistanceIndication : public BaseEntity
{
private:
    double distance;
    double minimumReceptionAltitude;
public:
    DistanceIndication();
    DistanceIndication(QString uuid);
    virtual ~DistanceIndication();
    double getDistance() const;
    void setDistance(double value);
    double getMinimumReceptionAltitude() const;
    void setMinimumReceptionAltitude(double value);
private:
    void init(
            double distance = INF,
            double minimumReceptionAltitude = INF
            );
};

#endif // DISTANCEINDICATION_H
