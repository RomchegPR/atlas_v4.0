#ifndef TERMINALSEGMENTPOINT_H
#define TERMINALSEGMENTPOINT_H

#include <Entities/BaseEntity.h>
#include <Entities/DesignatedPoint.h>
#include <DataTypes/CodeProcedureFixRole.h>
#include <DataTypes/CodeATCReporting.h>

// Класс, описывающий точку сегмента процедуры

class TerminalSegmentPoint : public BaseEntity
{
private:
    bool                        indicatorFACF;
    double                      leadRadial;
    double                      leadDME;
    CodeProcedureFixRole        role;
    bool                        flyOver;
    bool                        radarGuidance;
    CodeATCReporting            reportingATC;
    bool                        waypoint;
    DesignatedPoint*            fixDesignatedPoint; // need to fix it
public:
    TerminalSegmentPoint();
    TerminalSegmentPoint(QString uuid);
    virtual ~TerminalSegmentPoint();

    CodeProcedureFixRole getRole() const;
    void setRole(const CodeProcedureFixRole &value);

    double getLeadRadial() const;
    void setLeadRadial(double value);

    double getLeadDME() const;
    void setLeadDME(double value);

    bool getIndicatorFACF() const;
    void setIndicatorFACF(bool value);

    CodeATCReporting getReportingATC() const;
    void setReportingATC(const CodeATCReporting &value);

    bool getFlyOver() const;
    void setFlyOver(bool value);

    bool getWaypoint() const;
    void setWaypoint(bool value);

    bool getRadarGuidance() const;
    void setRadarGuidance(bool value);

    DesignatedPoint *getFixDesignatedPoint() const;
    void setFixDesignatedPoint(DesignatedPoint *value);

private:
    void init(
            CodeProcedureFixRole role = CodeProcedureFixRole::OTHER,
            double leadRadial = INF,
            double leadDME = INF,
            bool indicatorFACF = false,
            CodeATCReporting reportingATC = CodeATCReporting::OTHER,
            bool flyOver = false,
            bool waypoint = false,
            bool radarGuidance = false,
            DesignatedPoint* fixDesignatedPoint = nullptr
            );
};

#endif // TERMINALSEGMENTPOINT_H
