#ifndef GM_POINT_H
#define GM_POINT_H

#include <GeoMath/GeoPoint.h>

class GM_Point {

public:

    GM_Point();
    GM_Point(CGeoPoint point);
    virtual ~GM_Point(){}

    CGeoPoint                   getGpPosition() const;
    void                        setGpPosition(const CGeoPoint &gpPosition);

private:

    CGeoPoint                   m_gpPosition;
};

#endif // GM_POINT_H
