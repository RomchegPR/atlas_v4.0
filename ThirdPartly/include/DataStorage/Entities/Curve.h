#ifndef CURVE_H
#define CURVE_H
#include "BaseEntity.h"
#include "GM_Curve.h"
#include "DataTypes/ValDistanceType.h"

class Curve : public BaseEntity, public GM_Curve
{
public:

    Curve();
    Curve(QString uuid);

    virtual ~Curve(){}

    ValDistanceType             *getHorizontalAccuracy() const;
    void                        setHorizontalAccuracy(ValDistanceType *value);

private:

    ValDistanceType             *horizontalAccuracy;
    void init(){
        horizontalAccuracy = new ValDistanceType();
    }
};

#endif // CURVE_H
