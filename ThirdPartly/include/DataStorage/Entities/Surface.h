#ifndef SURFACE_H
#define SURFACE_H
#include "BaseEntity.h"
#include "GM_Surface.h"
#include "DataTypes/ValDistanceType.h"

class Surface : public BaseEntity, public GM_Surface
{

public:

    Surface();
    Surface(QString uuid);

    virtual ~Surface(){}

    ValDistanceType         *getHorizontalAccuracy() const;
    void                    setHorizontalAccuracy(ValDistanceType *value);

private:

    ValDistanceType         *horizontalAccuracy;
    void init(){
        horizontalAccuracy = new ValDistanceType();
    }
};

#endif // SURFACE_H
