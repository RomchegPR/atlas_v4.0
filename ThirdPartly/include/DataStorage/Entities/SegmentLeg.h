#ifndef SEGMENTLEG_H
#define SEGMENTLEG_H

#include <QString>
#include <QVector>
#include <QPoint>
#include "Entities/BaseEntity.h"
#include "Entities/TerminalSegmentPoint.h"
#include "Entities/AircraftCharacteristic.h"
#include "Entities/AngleIndication.h"
#include "Entities/DistanceIndication.h"
#include "Entities/HoldingPattern.h"
#include "DataTypes/CodeSegmentTermination.h"
#include "DataTypes/CodeTrajectory.h"
#include "DataTypes/CodeSegmentPath.h"
#include "DataTypes/CodeCourse.h"
#include "DataTypes/CodeDirectionTurn.h"
#include "DataTypes/CodeSpeedReference.h"
#include "DataTypes/CodeAltitudeUse.h"
#include "DataTypes/SegmentLegType.h"
#include "DataTypes/CodeVerticalReference.h"
#include "Curve.h"

// Класс, описывающий сегмент

class SegmentLeg : public BaseEntity
{
protected:
    SegmentLegType                  segmentLegType;
    CodeSegmentTermination          endConditionDesignator;
    CodeTrajectory                  legPath;
    CodeSegmentPath                 legTypeARINC;
    CodeCourse                      courseType;
    CodeDirectionReference          courseDirection;
    CodeDirectionTurn               turnDirection;

    CodeSpeedReference              speedReference;
    CodeAltitudeUse                 speedIntrpretation;


    CodeVerticalReference           upperLimitReference;
  
    CodeVerticalReference           lowerLimitReference;
    CodeAltitudeUse                 altitudeInterpretation;

    CodeVerticalReference           altitudeOverrideReference;

    TerminalSegmentPoint*           startPoint;
    TerminalSegmentPoint*           endPoint;
    TerminalSegmentPoint*           arcCentre;
    AngleIndication*                angle;
    DistanceIndication*             distance;
    QVector<AircraftCharacteristic* > aircraftCategory;
    HoldingPattern*                 holding;
    QVector<QPointF>                coords;
    Curve*                          trajectory;
    
    double                          bankAngle;
    double                          length;
    double                          duration;
    bool                            procedureTurnRequired;
    double                          speedLimit;
    double                          course;
    double                          verticalAngle;
    
    QString                         lowerLimitAltitude;
    QString                         upperLimitAltitude;
    QString                         altitudeOverrideATC;
    
public:
    
    SegmentLeg();
    SegmentLeg(QString uuid);
    virtual ~SegmentLeg();

    CodeSegmentTermination getEndConditionDesignator() const;
    void setEndConditionDesignator(const CodeSegmentTermination &value);
    
    CodeTrajectory getLegPath() const;
    void setLegPath(const CodeTrajectory &value);
    
    CodeSegmentPath getLegTypeARINC() const;
    void setLegTypeARINC(const CodeSegmentPath &value);
    
    double getCourse() const;
    void setCourse(double value);
    
    CodeCourse getCourseType() const;
    void setCourseType(const CodeCourse &value);
    
    CodeDirectionReference getCourseDirection() const;
    void setCourseDirection(const CodeDirectionReference &value);
    
    CodeDirectionTurn getTurnDirection() const;
    void setTurnDirection(const CodeDirectionTurn &value);
    
    double getSpeedLimit() const;
    void setSpeedLimit(double value);
    
    CodeSpeedReference getSpeedReference() const;
    void setSpeedReference(const CodeSpeedReference &value);
    
    CodeAltitudeUse getSpeedIntrpretation() const;
    void setSpeedIntrpretation(const CodeAltitudeUse &value);
    
    double getBankAngle() const;
    void setBankAngle(double value);
    
    double getLength() const;
    void setLength(double value);
    
    double getDuration() const;
    void setDuration(double value);
    
    bool getProcedureTurnRequired() const;
    void setProcedureTurnRequired(bool value);
    
    QString getUpperLimitAltitude() const;
    void setUpperLimitAltitude(const QString &value);
    
    CodeVerticalReference getUpperLimitReference() const;
    void setUpperLimitReference(const CodeVerticalReference &value);
    
    QString getLowerLimitAltitude() const;
    void setLowerLimitAltitude(const QString &value);
    
    CodeVerticalReference getLowerLimitReference() const;
    void setLowerLimitReference(const CodeVerticalReference &value);
    
    CodeAltitudeUse getAltitudeInterpretation() const;
    void setAltitudeInterpretation(const CodeAltitudeUse &value);
    
    QString getAltitudeOverrideATC() const;
    void setAltitudeOverrideATC(const QString &value);
    
    CodeVerticalReference getAltitudeOverrideReference() const;
    void setAltitudeOverrideReference(const CodeVerticalReference &value);
    
    double getVerticalAngle() const;
    void setVerticalAngle(double value);
    
    TerminalSegmentPoint *getStartPoint() const;
    void setStartPoint(TerminalSegmentPoint *value);
    
    TerminalSegmentPoint *getEndPoint() const;
    void setEndPoint(TerminalSegmentPoint *value);
    
    TerminalSegmentPoint *getArcCentre() const;
    void setArcCentre(TerminalSegmentPoint *value);
    
    AngleIndication *getAngle() const;
    void setAngle(AngleIndication *value);
    
    DistanceIndication *getDistance() const;
    void setDistance(DistanceIndication *value);
    
    QVector<AircraftCharacteristic *> getAircraftCategory() const;
    void setAircraftCategory(const QVector<AircraftCharacteristic *> &value);
    
    HoldingPattern *getHolding() const;
    void setHolding(HoldingPattern *value);
    
    QVector<QPointF> getCoords() const;
    void setCoords(const QVector<QPointF> &value);
    
    SegmentLegType getSegmentLegType() const;
    void setSegmentLegType(const SegmentLegType &value);

    Curve *getTrajectory() const;
    void setTrajectory(Curve *value);

private:
    void init(
            CodeSegmentTermination endConditionDesignator = CodeSegmentTermination::OTHER,
            CodeTrajectory legPath = CodeTrajectory::OTHER,
            CodeSegmentPath legTypeARINC = CodeSegmentPath::OTHER,
            double course = INF,
            CodeCourse courseType = CodeCourse::OTHER,
            CodeDirectionReference courseDirection = CodeDirectionReference::OTHER,
            CodeDirectionTurn turnDirection = CodeDirectionTurn::OTHER,
            double speedLimit = INF,
            CodeSpeedReference speedReference = CodeSpeedReference::OTHER,
            CodeAltitudeUse speedIntrpretation = CodeAltitudeUse::OTHER,
            double bankAngle = INF,
            double length = INF,
            double duration = INF,
            bool procedureTurnRequired = false,
            QString upperLimitAltitude = "None",
            CodeVerticalReference upperLimitReference = CodeVerticalReference::OTHER,
            QString lowerLimitAltitude = "None",
            CodeVerticalReference lowerLimitReference = CodeVerticalReference::OTHER,
            CodeAltitudeUse altitudeInterpretation = CodeAltitudeUse::OTHER,
            QString altitudeOverrideATC = "None",
            CodeVerticalReference altitudeOverrideReference = CodeVerticalReference::OTHER,
            double verticalAngle = INF,
            TerminalSegmentPoint *startPoint = nullptr,
            TerminalSegmentPoint *endPoint = nullptr,
            TerminalSegmentPoint *arcCentre = nullptr,
            AngleIndication *angle = nullptr,
            DistanceIndication *distance = nullptr,
            HoldingPattern *holding = nullptr,
            Curve *trajectory = nullptr
            );
};

#endif // SEGMENTLEG_H
