#ifndef ELEVATEDCURVE_H
#define ELEVATEDCURVE_H

#include "DataTypes/ValDistanceVerticalType.h"
#include "DataTypes/ValDistanceSignedType.h"
#include "Curve.h"


class ElevatedCurve : public Curve
{
public:
    ElevatedCurve();
    ElevatedCurve(QString uuid);
    virtual ~ElevatedCurve(){}

    ValDistanceVerticalType *getElevation() const;
    void setElevation(ValDistanceVerticalType *value);

    ValDistanceSignedType *getGeoidUndulation() const;
    void setGeoidUndulation(ValDistanceSignedType *value);

    ValDistanceType *getVerticalAccuracy() const;
    void setVerticalAccuracy(ValDistanceType *value);

private:
    ValDistanceVerticalType *elevation;
    ValDistanceSignedType *geoidUndulation;
    ValDistanceType *verticalAccuracy;
    //CodeVerticalDatumType verticalDatum; //Не нуждаемся в использовании
    init(){
        elevation = new ValDistanceVerticalType();
        geoidUndulation = new ValDistanceSignedType();
        verticalAccuracy = new ValDistanceType();
    }
};

#endif // ELEVATEDCURVE_H
