#ifndef VERTICALSTRUCTURE_H
#define VERTICALSTRUCTURE_H

#include <QString>
#include "Entities/BaseEntity.h"
#include "DataTypes/CodeVerticalStructure.h"
#include "Entities/VerticalStructurePart.h"
#include "Entities/Geometry.h"
#include <QVector>
#include "../IMetaInfo.h"

class VerticalStructure : public BaseEntity
{
private:

    bool                                group;
    double                              length;
    bool                                lighted;
    bool                                lightingICAOStandard;
    bool                                markingICAOStandard;
    QString                             name;
    double                              radius;
    bool                                synchronisedLighting;
    double                              width;
    CodeVerticalStructure               type;
    QVector<VerticalStructurePart*>     parts;

public:
    VerticalStructure();
    virtual ~VerticalStructure() = default;

    bool                                operator ==(const VerticalStructure &rhs) const;

    bool                                getGroup() const;
    void                                setGroup(bool value);

    double                              getLength() const;
    void                                setLength(double value);

    bool                                getLighted() const;
    void                                setLighted(bool value);

    bool                                getLightingICAOStandard() const;
    void                                setLightingICAOStandard(bool value);

    bool                                getMarkingICAOStandard() const;
    void                                setMarkingICAOStandard(bool value);

    QString                             getName() const;
    void                                setName(const QString &value);

    double                              getRadius() const;
    void                                setRadius(double value);

    bool                                getSynchronisedLighting() const;
    void                                setSynchronisedLighting(bool value);

    double                              getWidth() const;
    void                                setWidth(double value);

    CodeVerticalStructure               getType() const;
    void                                setType(const CodeVerticalStructure &value);

    QVector<VerticalStructurePart *>    getParts() const;
    void                                setParts(const QVector<VerticalStructurePart *> &value);

    QVector<Geometry>                   getGeometry() const;

};

#endif // VERTICALSTRUCTURE_H
