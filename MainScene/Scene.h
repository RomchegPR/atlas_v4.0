#ifndef SCENE_H
#define SCENE_H

#define GLEW_STATIC
#include <GL/glew.h>

#include <QGraphicsScene>
#include <QTimer>
#include <QTime>

class DataStorage_test;
class RenderObject;

class Scene : public QGraphicsScene
{
    Q_OBJECT
public:
    Scene();
    void drawBackground(QPainter *painter, const QRectF &rect);

    void initObjects();

private:

   void initGL();
   GLuint VBO, VAO;
   GLuint shaderProgram;

   QTimer *m_timer;
   QTime m_time;
   
   DataStorage_test* m_pDataStorage;
};

#endif // SCENE_H
