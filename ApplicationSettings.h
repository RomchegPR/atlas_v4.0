#ifndef APPLICATIONSETTINGS_H
#define APPLICATIONSETTINGS_H

#define COMPANY_NAME "NITA"
#define PROGRAMM_NAME "ATLAS"

#include <QSettings>
#include <QString>

/********************************

Class:		ApplicationSettings

Purpose: Stores in itself all the variables which responsible for the operation of the program.

********************************/

struct ApplicationSettings
{
    QSettings           mSettings;

    QString             mMainWorkingDirectory;
    QString             mMainDataDirectory;
    QString             mMainLogsDirectory;
    QString             mMainTexturesDirectory;

    bool                m_WriteLogInFile;
    bool                m_WriteOnlyDebugFlag;

    double              m_DistanceBetweenPoint_m;
    float               m_minimumAngleBetweenPointArc_m;

public:
    ApplicationSettings();
    readSettings();
    loadDefault();
    writeSettings();
};

extern ApplicationSettings applicationSettings;

#endif // APPLICATIONSETTINGS_H
