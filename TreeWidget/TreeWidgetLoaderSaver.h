#ifndef TREEWIDGETLOADERSAVER_H
#define TREEWIDGETLOADERSAVER_H

#include <QObject>
#include <QString>
#include <QTreeWidget>
#include <QXmlStreamWriter>

class TreeWidgetLoaderSaver
{
public:
    TreeWidgetLoaderSaver();

    void loadFromFile(QString FileName, QTreeWidget* Widget);
    bool writeToFile(QString FileName, QTreeWidget* Widget);

private:
    void writeIndexEntry(QXmlStreamWriter *xmlWriter, QTreeWidgetItem *item);
};

#endif // TREEWIDGETLOADERSAVER_H
