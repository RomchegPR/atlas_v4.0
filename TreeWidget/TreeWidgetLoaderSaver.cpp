#include "TreeWidgetLoaderSaver.h"
#include "TreeWidgetXmlStreamReader.h"
#include <QFile>
#include <iostream>

TreeWidgetLoaderSaver::TreeWidgetLoaderSaver()
{

}

void TreeWidgetLoaderSaver::loadFromFile(QString FileName, QTreeWidget* Widget)
{
    TreeWidgetXmlStreamReader xmlStreamReader(Widget);
    xmlStreamReader.readFile(FileName);
}

bool TreeWidgetLoaderSaver::writeToFile(QString FileName, QTreeWidget* Widget)
{
    QFile File(FileName);

    if (!File.open(QFile::WriteOnly | QFile::Text)) {
        std::cerr << "Error: Cannot write file "
                  << qPrintable(FileName) << ": "
                  << qPrintable(File.errorString()) << std::endl;
        return false;
    }

    QXmlStreamWriter xmlWriter(&File);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("bookindex");
    for (int i = 0; i < Widget->topLevelItemCount(); ++i)
        writeIndexEntry(&xmlWriter, Widget->topLevelItem(i));
    xmlWriter.writeEndDocument();
    File.close();

    if (File.error()) {
        std::cerr << "Error: Cannot write file "
                  << qPrintable(FileName) << ": "
                  << qPrintable(File.errorString()) << std::endl;
        return false;
    }
    return true;
}

void TreeWidgetLoaderSaver::writeIndexEntry(QXmlStreamWriter *xmlWriter, QTreeWidgetItem *item)
{
    xmlWriter->writeStartElement("entry");
    xmlWriter->writeAttribute("term", item->text(0));
    QString pageString = item->text(1);
    if (!pageString.isEmpty()) {
        QStringList pages = pageString.split(", ");
        foreach (QString page, pages)
            xmlWriter->writeTextElement("page", page);
    }
    for (int i = 0; i < item->childCount(); ++i)
        writeIndexEntry(xmlWriter, item->child(i));
    xmlWriter->writeEndElement();
}
