#ifndef TREEWIDGETVIEW_H
#define TREEWIDGETVIEW_H

#include <QObject>
#include <QTreeWidget>

class TreeWidgetView : public QTreeWidget
{
    Q_OBJECT

public:
    TreeWidgetView(QWidget *parent = 0);

    void dropEvent(QDropEvent * event);
    void dragEnterEvent(QDragEnterEvent *event);
    void deleteItem();
    void addNewItem();
    void clear();

    void useTreeWidget(QTreeWidget * widget);
    static void useFlagsForAllTreeItems ( QTreeWidgetItem *item, Qt::ItemFlags treeItemFlags );

    void setTreeItemFlags(const Qt::ItemFlags &TreeItemFlags);
    Qt::ItemFlags TreeItemFlags() const;

    void keyPressEvent(QKeyEvent*);

private:
    Qt::ItemFlags m_TreeItemFlags;

};

#endif // TREEWIDGETVIEW_H
