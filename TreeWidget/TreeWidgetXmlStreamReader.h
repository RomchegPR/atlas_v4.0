#ifndef TREEWIDGETXMLSTREAMREADER_H
#define TREEWIDGETXMLSTREAMREADER_H

#include <QXmlStreamReader>
#include <QTreeWidget>

class TreeWidgetXmlStreamReader
{
public:
    TreeWidgetXmlStreamReader(QTreeWidget *tree);

    void setTreeWidget(QTreeWidget *value);
    bool readFile(const QString &fileName);


private:
    void readBookindexElement();
    void readEntryElement(QTreeWidgetItem *parent);
    void readPageElement(QTreeWidgetItem *parent);
    void skipUnknownElement();

    QTreeWidget *treeWidget;
    QXmlStreamReader reader;
};

#endif // TREEWIDGETXMLSTREAMREADER_H
