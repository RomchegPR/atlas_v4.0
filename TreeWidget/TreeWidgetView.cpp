#include "TreeWidgetView.h"
#include <QDropEvent>

TreeWidgetView::TreeWidgetView(QWidget *parent)
{
    m_TreeItemFlags = Qt::ItemIsEnabled | Qt::ItemIsSelectable
            | Qt::ItemIsDropEnabled | Qt::ItemIsEditable | Qt::ItemIsDragEnabled;

    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setDragEnabled(true);

    viewport()->setAcceptDrops(true);
    setDropIndicatorShown(true);
    setDragDropMode(QAbstractItemView::InternalMove);

}

void TreeWidgetView::dropEvent(QDropEvent *event)
{
    QTreeWidget::dropEvent(event);

    QModelIndex index = indexAt(event->pos());

    QTreeWidgetItem* pItem;

    if (!index.isValid()) {  // just in case
//        event->setDropAction(Qt::IgnoreAction);
        pItem = NULL; //new QTreeWidgetItem(this);
    } else {
        pItem = this->itemAt(event->pos());
    }

    QTreeWidget *tree = dynamic_cast<QTreeWidget *>(event->source());


    if (tree->selectedItems().size() > 0){

        for (QTreeWidgetItem * child : tree->selectedItems())
            if (pItem == NULL){
                addTopLevelItem(child->clone());
            } else {
                pItem->addChild(child->clone());
            }
    }

}

void TreeWidgetView::dragEnterEvent(QDragEnterEvent *event)
{
    event->acceptProposedAction();
}

void TreeWidgetView::deleteItem()
{
    if (currentItem() != NULL){
        QList<QTreeWidgetItem*> selectedItems( this->selectedItems() );
        foreach ( QTreeWidgetItem * item, selectedItems )
            if (item != NULL){
                delete item;
            }
    }
}

void TreeWidgetView::addNewItem()
{
    QTreeWidgetItem* pItem;

    if (currentItem() != NULL){
        pItem = new QTreeWidgetItem(currentItem());
    } else {
        pItem = new QTreeWidgetItem(this);
    }

    pItem->setText(0, "NEW ITEM");
    pItem->setFlags(m_TreeItemFlags);
    pItem->addChild(pItem);
}

void TreeWidgetView::clear()
{
    for (int i = 0; i < topLevelItemCount(); ++i)
    {
        qDeleteAll(topLevelItem(i)->takeChildren());
        delete topLevelItem(i);
    }

    if (topLevelItemCount() > 0) clear();
}

void TreeWidgetView::useTreeWidget(QTreeWidget *widget)
{
    clear();

    for (int i = 0; i < widget->topLevelItemCount(); ++i){
        insertTopLevelItem(0, widget->topLevelItem(i)->clone());
    }

    useFlagsForAllTreeItems(invisibleRootItem(), m_TreeItemFlags);

}

void TreeWidgetView::useFlagsForAllTreeItems(QTreeWidgetItem *item, Qt::ItemFlags TreeItemFlags)
{
    item->setCheckState(0, Qt::Unchecked);
    item->setFlags(TreeItemFlags);

    for( int i = 0; i < item->childCount(); ++i )
        useFlagsForAllTreeItems( item->child(i), TreeItemFlags );
}

void TreeWidgetView::setTreeItemFlags(const Qt::ItemFlags &TreeItemFlags)
{
    m_TreeItemFlags = TreeItemFlags;
}

Qt::ItemFlags TreeWidgetView::TreeItemFlags() const
{
    return m_TreeItemFlags;
}

void TreeWidgetView::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Delete){
        deleteItem();
    }
}
