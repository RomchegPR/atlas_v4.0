#include "PaintedState.h"
#include "RenderObject.h"
#include <QDebug>

PaintedState::PaintedState()
{

}

void PaintedState::exec(RenderObject *renderObject)
{
    renderObject->paint();
}

RenderObjectState* PaintedState::handleInput(RenderObjectState::State state)
{
    switch (state) {
    case STATE_PAINTING:
        return new PaintedState();
        break;
    case STATE_PAINT_READY:
        return new ReadyState();
        break;
    case STATE_FLICKERING:
        return new FlickerState();
        break;
    case STATE_SHINING:
        return new ShinedState();
        break;
    case STATE_INITING:
        return new PaintedState();
        break;
    default:
        qDebug() << "can't change State from NotPaintedState";
        return nullptr;
        break;
    }
}

