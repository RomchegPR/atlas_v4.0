#ifndef PAINTEDSTATE_H
#define PAINTEDSTATE_H

#include <QObject>
#include "RenderObjectState.h"

class PaintedState : public RenderObjectState
{
public:
    PaintedState();
    void exec(RenderObject *renderObject);
    RenderObjectState* handleInput(State);

};

#endif // PAINTEDSTATE_H
