#include "InitializationState.h"
#include <QDebug>
#include "RenderObject.h"

InitializationState::InitializationState()
{

}

void InitializationState::exec(RenderObject *renderObject)
{
    renderObject->init();
}

RenderObjectState* InitializationState::handleInput(RenderObjectState::State state)
{
    switch (state) {
    case STATE_PAINT_READY:
        return new ReadyState();
        break;
    default:
        qDebug() << "can't change State from NotPaintedState";
        return nullptr;
        break;
    }
}
