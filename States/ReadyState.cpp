#include "ReadyState.h"
#include <QDebug>
#include "RenderObject.h"

ReadyState::ReadyState()
{

}

void ReadyState::exec(RenderObject *renderObject)
{

}

RenderObjectState* ReadyState::handleInput(RenderObjectState::State state)
{
    switch (state) {
    case STATE_PAINT_READY:
        return new ReadyState();
        break;
    case STATE_PAINTING:
        return new PaintedState();
        break;
    case STATE_FLICKERING:
        return new FlickerState();
        break;
    case STATE_SHINING:
        return new ShinedState();
        break;
    default:
        qDebug() << "can't change State from ReadyState";
        return nullptr;
        break;
    }
}
