#ifndef SHINEDSTATE_H
#define SHINEDSTATE_H

#include <QObject>
#include "PaintedState.h"

class ShinedState : public PaintedState
{
public:
    ShinedState();
    void exec(RenderObject *renderObject);
};

#endif // SHINEDSTATE_H
