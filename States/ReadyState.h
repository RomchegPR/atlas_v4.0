#ifndef READYSTATE_H
#define READYSTATE_H

#include "RenderObjectState.h"

class ReadyState : public RenderObjectState
{
public:
    ReadyState();
    void exec(RenderObject *renderObject);
    RenderObjectState* handleInput(RenderObjectState::State state);

};

#endif // READYSTATE_H
