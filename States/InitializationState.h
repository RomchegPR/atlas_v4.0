#ifndef NOTPAINTEDSTATE_H
#define NOTPAINTEDSTATE_H

#include <QObject>
#include "RenderObjectState.h"

class InitializationState : public RenderObjectState
{
public:
    InitializationState();
    void exec(RenderObject *renderObject);
    RenderObjectState* handleInput(State);

};

#endif // NOTPAINTEDSTATE_H
