#include "CreatingState.h"
#include "InitializationState.h"
#include <QDebug>

CreatingState::CreatingState()
{

}

void CreatingState::exec(RenderObject *renderObject)
{

}

RenderObjectState* CreatingState::handleInput(RenderObjectState::State state)
{
    if (state == STATE_CREATING){
        return new CreatingState();
    }
    if (state == STATE_INITING){
        return new InitializationState();
    } else {
        qDebug() << "can't change State from NotInitState";
        return nullptr;
    }
}

