/*****************************Описание класса CDistance***************************
 *
**********************************************************************************/

#include "measures.h"

//Конструктор копии
CDistance::CDistance(const CDistance& Distance)
{
	m_Distance = Distance.Meters();
}

//Перегрузка операторов
CDistance& CDistance::operator=(const CDistance& Distance)
{
	if(this == &Distance)
		return *this;

	m_Distance = Distance.Meters();
	return *this;
}

CDistance& CDistance::operator+=(const CDistance& Distance)
{
	m_Distance += Distance.Meters();
	return *this;
}

CDistance& CDistance::operator-=(const CDistance& Distance)
{
	m_Distance -= Distance.Meters();
	return *this;
}

CDistance& CDistance::operator*=(const CDistance& Distance)
{
	m_Distance *= Distance.Meters();
	return *this;
}

CDistance& CDistance::operator/=(const CDistance& Distance)
{
	m_Distance /= Distance.Meters();
	return *this;
}

const CDistance CDistance::operator+(const CDistance& Distance) const
{
	CDistance Sum;
	Sum = *this;
	Sum.SetMeters(Sum.Meters() + Distance.Meters());
	return Sum;
}

const CDistance CDistance::operator-(const CDistance& Distance) const
{
	CDistance Dif;
	Dif = *this;
	Dif.SetMeters(Dif.Meters() - Distance.Meters());
	return Dif;
}

const CDistance CDistance::operator*(const CDistance& Distance) const
{
	CDistance Mult;
	Mult = *this;
	Mult.SetMeters(Mult.Meters() * Distance.Meters());
	return Mult;
}

const CDistance CDistance::operator/(const CDistance& Distance) const
{
	CDistance Div;
	Div = *this;
	Div.SetMeters(Div.Meters() / Distance.Meters());
	return Div;
}

//Методы получения и присваивания
float CDistance::Meters() const
{
    return m_Distance;
}

void CDistance::SetMeters(const float fDistance_m)
{
    m_Distance = fDistance_m;
}

float CDistance::Kilometers() const
{
    return m_Distance / 1000.0;
}

void CDistance::SetKilometers(const float fDistance_km)
{
    m_Distance = fDistance_km * 1000.0;
}

float CDistance::NauticalMiles() const
{
    return m_Distance / 1852.0;
}

void CDistance::SetNauticalMiles(const float fDistance_nm)
{
	m_Distance = fDistance_nm * 1852.0;
}

float CDistance::StatutMiles() const
{
    return m_Distance / 1609.344;
}

void CDistance::SetStatutMiles(const float fDistance_miles)
{
	m_Distance = fDistance_miles * 1609.344;
}

