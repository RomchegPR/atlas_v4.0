#include "measures.h"

//Конструктор копии
CTime::CTime(const CTime& Time)
{
	m_Time = Time.Seconds();
}

//Перегрузка операторов
CTime& CTime::operator=(const CTime& Time)
{
	if(this == &Time)
		return *this;

	m_Time = Time.Seconds();
	return *this;
}

CTime& CTime::operator+=(const CTime& Time)
{
	m_Time += Time.Seconds();
	return *this;
}

CTime& CTime::operator-=(const CTime& Time)
{
	m_Time -= Time.Seconds();
	return *this;
}

CTime& CTime::operator*=(const CTime& Time)
{
	m_Time *= Time.Seconds();
	return *this;
}

CTime& CTime::operator/=(const CTime& Time)
{
	m_Time /= Time.Seconds();
	return *this;
}

const CTime CTime::operator+(const CTime& Time) const
{
	CTime Sum;
	Sum = *this;
	Sum.SetSeconds(Sum.Seconds() + Time.Seconds());
	return Sum;
}

const CTime CTime::operator-(const CTime& Time) const
{
	CTime Dif;
	Dif = *this;
	Dif.SetSeconds(Dif.Seconds() - Time.Seconds());
	return Dif;
}

const CTime CTime::operator*(const CTime& Time) const
{
	CTime Mult;
	Mult = *this;
	Mult.SetSeconds(Mult.Seconds() * Time.Seconds());
	return Mult;
}

const CTime CTime::operator/(const CTime& Time) const
{
	CTime Div;
	Div = *this;
	Div.SetSeconds(Div.Seconds() / Time.Seconds());
	return Div;
}

//Методы получения и присваивания
float CTime::Seconds() const
{
	return m_Time;
}

void CTime::SetSeconds(const float fTime_sec)
{
	m_Time = fTime_sec;
}

float CTime::Minutes() const
{
	return m_Time / 60.0;
}

void CTime::SetMinutes(const float fTime_min)
{
	m_Time = fTime_min * 60.0;
}

float CTime::Hours() const
{
	return m_Time / 3600.0;
}

void CTime::SetHours(const float fTime_hours)
{
	m_Time = fTime_hours * 3600.0;
}

float CTime::Days() const
{
	return m_Time / 86400.0;
}

void CTime::SetDays(const float fTime_days)
{
	m_Time = fTime_days * 86400.0;
}
