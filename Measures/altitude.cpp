#include "measures.h"
#include <math.h>

//Конструктор копии
CAltitude::CAltitude(const CAltitude& Altitude)
{
	m_Altitude = Altitude.Meters();
}

//Перегрузка операторов
CAltitude& CAltitude::operator=(const CAltitude& Altitude)
{
	if(this == &Altitude)
		return *this;

	m_Altitude = Altitude.Meters();
	return *this;
}

CAltitude& CAltitude::operator+=(const CAltitude& Altitude)
{
	m_Altitude += Altitude.Meters();
	return *this;
}

CAltitude& CAltitude::operator+=(const float& value)
{
    m_Altitude += value;
    return *this;
}

CAltitude& CAltitude::operator-=(const CAltitude& Altitude)
{
	m_Altitude -= Altitude.Meters();
	return *this;
}

CAltitude& CAltitude::operator*=(const CAltitude& Altitude)
{
	m_Altitude *= Altitude.Meters();
	return *this;
}

CAltitude& CAltitude::operator/=(const CAltitude& Altitude)
{
	m_Altitude /= Altitude.Meters();
	return *this;
}

const CAltitude CAltitude::operator+(const CAltitude& Altitude) const
{
	CAltitude Sum;
	Sum = *this;
	Sum.SetMeters(Sum.Meters() + Altitude.Meters());
	return Sum;
}

const CAltitude CAltitude::operator-(const CAltitude& Altitude) const
{
	CAltitude Dif;
	Dif = *this;
	Dif.SetMeters(Dif.Meters() - Altitude.Meters());
	return Dif;
}

const CAltitude CAltitude::operator*(const CAltitude& Altitude) const
{
	CAltitude Mult;
	Mult = *this;
	Mult.SetMeters(Mult.Meters() * Altitude.Meters());
	return Mult;
}

const CAltitude CAltitude::operator/(const CAltitude& Altitude) const
{
	CAltitude Div;
	Div = *this;
	Div.SetMeters(Div.Meters() / Altitude.Meters());
	return Div;
}

const CAltitude CAltitude::operator/(const float& value) const
{
    CAltitude Div;
    Div = *this;
    Div.SetMeters(Div.Meters() / value);
    return Div;
}

//Методы получения и присваивания
float CAltitude::Meters() const
{
	return m_Altitude;
}

void CAltitude::SetMeters(const float fAltitude_m)
{
    m_Altitude = fAltitude_m;
}

float CAltitude::Feet() const
{
    return m_Altitude / 0.3048;
}

void CAltitude::SetFeet(const float fAltitude_ft)
{
    m_Altitude = fAltitude_ft * 0.3048;
}

float CAltitude::Kilometers() const
{
    return m_Altitude / 1000.0;
}

void CAltitude::SetKilometers(const float fAltitude_km)
{
    m_Altitude = fAltitude_km * 1000.0;
}

int CAltitude::FlightLevel() const
{
    return (int)((round(m_Altitude / 0.3048 / 500) * 5) );
}

void CAltitude::SetFlightLevel(const int iAltitude_FL)
{
	m_Altitude = (float)iAltitude_FL * 30.48;
}
