#include "measures.h"

//Конструктор копии
CSpeed::CSpeed(const CSpeed& Speed)
{
	m_Speed = Speed.MeterSec();
}

//Перегрузка операторов
CSpeed& CSpeed::operator=(const CSpeed& Speed)
{
	if(this == &Speed)
		return *this;

	m_Speed = Speed.MeterSec();
	return *this;
}

CSpeed& CSpeed::operator+=(const CSpeed& Speed)
{
	m_Speed += Speed.MeterSec();
	return *this;
}

CSpeed& CSpeed::operator-=(const CSpeed& Speed)
{
	m_Speed -= Speed.MeterSec();
	return *this;
}

CSpeed& CSpeed::operator*=(const CSpeed& Speed)
{
	m_Speed *= Speed.MeterSec();
	return *this;
}

CSpeed& CSpeed::operator/=(const CSpeed& Speed)
{
	m_Speed /= Speed.MeterSec();
	return *this;
}

const CSpeed CSpeed::operator+(const CSpeed& Speed) const
{
	CSpeed Sum;
	Sum = *this;
	Sum.SetMeterSec(Sum.MeterSec() + Speed.MeterSec());
	return Sum;
}

const CSpeed CSpeed::operator-(const CSpeed& Speed) const
{
	CSpeed Dif;
	Dif = *this;
	Dif.SetMeterSec(Dif.MeterSec() - Speed.MeterSec());
	return Dif;
}

const CSpeed CSpeed::operator*(const CSpeed& Speed) const
{
	CSpeed Mult;
	Mult = *this;
	Mult.SetMeterSec(Mult.MeterSec() * Speed.MeterSec());
	return Mult;
}

const CSpeed CSpeed::operator/(const CSpeed& Speed) const
{
	CSpeed Div;
	Div = *this;
	Div.SetMeterSec(Div.MeterSec() / Speed.MeterSec());
	return Div;
}

//Методы получения и присваивания
float CSpeed::MeterSec() const
{
    return m_Speed;
}

void CSpeed::SetMeterSec(const float fSpeed_ms)
{
    m_Speed = fSpeed_ms;
}

float CSpeed::KmH() const
{
	return m_Speed * 3.6;
}

void CSpeed::SetKmh(const float fSpeed_KmH)
{
	m_Speed = fSpeed_KmH / 3.6;
}

float CSpeed::Kts() const
{
    return m_Speed * 3600.0 / 1852.0;
}

void CSpeed::SetKts(const float fSpeed_Kts)
{
    m_Speed = fSpeed_Kts * 1852.0 / 3600.0;
}

float CSpeed::FtMin() const
{
    return m_Speed / 0.3048 * 60.0;
}

void CSpeed::SetFtMin(const float fSpeed_FtMin)
{
	m_Speed = fSpeed_FtMin * 0.3048 / 60.0;
}
