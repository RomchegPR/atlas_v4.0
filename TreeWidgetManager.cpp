#include "TreeWidgetManager.h"
#include <QDebug>
#include "TreeWidget/TreeWidgetLoaderSaver.h"
#include "TreeWidget/TreeWidgetView.h"
#include <QCoreApplication>


TreeWidgetManager::TreeWidgetManager()
{
    stopped = false;
    m_isLoadOperation = LOAD;

    m_pTreeWidgetCurrentView = nullptr;
    m_pTreeWidgetAllObjects = nullptr;
    m_pTreeWidgetMainView = nullptr;


    m_FileName = "C:/Nita/Atlas/res/TreeViewMenu/Default.txt";
}

void TreeWidgetManager::run()
{
    TreeWidgetLoaderSaver LoaderSaver;

    if (m_isLoadOperation == LOAD)
        LoaderSaver.loadFromFile(m_FileName, m_pTreeWidgetAllObjects);
    else
        LoaderSaver.writeToFile(m_FileName, m_pTreeWidgetCurrentView);

    stopped = false;
}

void TreeWidgetManager::stop()
{
    stopped = true;
}

void TreeWidgetManager::setFileName(QString fileName)
{
    m_FileName = fileName;
}

void TreeWidgetManager::setMode(Mode mode)
{
    m_isLoadOperation = mode;
}

void TreeWidgetManager::useViewSettings()
{
    m_pTreeWidgetMainView->clear();
    Qt::ItemFlags treeItemFlags = Qt::ItemIsEnabled |  Qt::ItemIsUserCheckable;

    for (int i = 0; i < m_pTreeWidgetCurrentView->topLevelItemCount(); ++i){
        m_pTreeWidgetMainView->insertTopLevelItem(0, m_pTreeWidgetCurrentView->topLevelItem(i)->clone());
    }

    TreeWidgetView::useFlagsForAllTreeItems(m_pTreeWidgetMainView->invisibleRootItem(), treeItemFlags);
//    m_pTreeWidgetMainView;
}

void TreeWidgetManager::setTreeWidgetCurrentView(QTreeWidget *pTreeWidgetCurrentView)
{
    m_pTreeWidgetCurrentView = pTreeWidgetCurrentView;
}

void TreeWidgetManager::setTreeWidgetAllObjects(QTreeWidget *pTreeWidgetAllObjects)
{
    m_pTreeWidgetAllObjects = pTreeWidgetAllObjects;
}

void TreeWidgetManager::setTreeWidgetMainView(QTreeWidget *pTreeWidgetMainView)
{
    m_pTreeWidgetMainView = pTreeWidgetMainView;
}


