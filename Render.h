#ifndef RENDER_H
#define RENDER_H

#include <QObject>
#include <QThread>

class DataStorage_test;

class Render  : public QThread
{
public:
    Render();
    void run();
    void stop();

    void setDataStorage(DataStorage_test *pDataStorage);

private:

    volatile bool stopped;
    DataStorage_test *m_pDataStorage;
};

#endif // RENDER_H
