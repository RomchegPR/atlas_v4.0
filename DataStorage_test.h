#ifndef DATASTORAGE_H
#define DATASTORAGE_H

#include "RenderObject.h"
#include <QVector>
#include <QString>
#include <QThread>
class Scene;

class DataStorage_test :  public QThread
{
private:
    static DataStorage_test* instance;

    DataStorage_test( ) {}
    DataStorage_test( DataStorage_test const&) = delete;
    DataStorage_test& operator=( DataStorage_test const&) = delete;

public:

    virtual ~DataStorage_test( );
    static DataStorage_test* getInstance( );

    void run();
    void stop();

    RenderObject*            getObject(QString name);
    QVector <RenderObject*>  getAllObjects();
    void                     addObject(RenderObject * object);

    void setMainScene(Scene *pMainScene);


private:
    volatile bool stopped;

    QVector <RenderObject*>  m_Objects;
    Scene                   *m_pMainScene;
};

#endif // DATASTORAGE_H
