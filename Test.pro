#-------------------------------------------------
#
# Project created by QtCreator 2017-08-08T16:53:42
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets xml

TARGET = Test
TEMPLATE = app
CONFIG += thread

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


INCLUDEPATH += $(NITAINCLUDE)
INCLUDEPATH += ThirdPartly/include
INCLUDEPATH += ThirdPartly/include/DataStorage

DEPENDPATH += lib

LIBS += -L$(NITALIB)
LIBS += -lgeomath_net_d
LIBS += -lglew32 -lopengl32

LIBS += "C:/Users/pra/atlas_v4.0RELEASE/release/DataStorageLib1.dll"
#win32: LIBS += "C:/Users/pra/atlas_v4.0/lib/DataStorageLib1.dll"
#win32: LIBS += "C:/Users/pra/atlas_v4.0/lib/testDll.dll"

SOURCES += main.cpp\
        mainwindow.cpp \
    States/PaintedState.cpp \
    States/FlickerState.cpp \
    States/RenderObjectState.cpp \
    RenderObject.cpp \
    States/ShinedState.cpp \
    Render.cpp \
    TreeWidget/TreeWidgetLoaderSaver.cpp \
    TreeWidget/TreeWidgetView.cpp \
    TreeWidget/TreeWidgetXmlStreamReader.cpp \
    TreeWidgetManager.cpp \
    DataStorageLoader.cpp \
    States/ReadyState.cpp \
    States/CreatingState.cpp \
    States/InitializationState.cpp \
    AtlasSystem.cpp \
    DataStorage_test.cpp \
    MainScene/Scene.cpp

HEADERS  += mainwindow.h \
    States/PaintedState.h \
    States/FlickerState.h \
    States/RenderObjectState.h \
    RenderObject.h \
    States/ShinedState.h \
    Render.h \
    TreeWidget/TreeWidgetLoaderSaver.h \
    TreeWidget/TreeWidgetView.h \
    TreeWidget/TreeWidgetXmlStreamReader.h \
    TreeWidgetManager.h \
    DataStorageLoader.h \
    States/ReadyState.h \
    States/CreatingState.h \
    States/InitializationState.h \
    AtlasSystem.h \
    DataStorage_test.h \
    ThirdPartly/include/DataStorage/DataStorage.h \
    ThirdPartly/include/DataStorage/datastoragedll_global.h \
    MainScene/Scene.h

FORMS    += mainwindow.ui

DISTFILES += \
    DataStorage/Utils.rar \
    Readers/AIXMReader/Res/14_03_2016_aixm.xml \
    Readers/AIXMReader/Res/AIXM_Airspace.xml \
    Readers/AIXMReader/Res/AIXM_Airspace_ULLI.xml \
    Readers/AIXMReader/Res/aixm_obstacles.xml \
    Readers/AIXMReader/Res/AIXM_Obstacles_Moscow.xml \
    Readers/AIXMReader/Res/aixmdataLED.xml \
    Readers/AIXMReader/Res/allinone.xml \
    Readers/AIXMReader/Res/DM_01.xml \
    Readers/AIXMReader/Res/Donlon.xml \
    Readers/AIXMReader/Res/export_sample_LSZB_1.aixm5 \
    Readers/AIXMReader/Res/MA-19.xml \
    Readers/AIXMReader/Res/test_aixm.xml

RESOURCES += \
    scene.qrc
