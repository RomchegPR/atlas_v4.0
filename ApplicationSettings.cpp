#include "ApplicationSettings.h"
#include <QCoreApplication>

ApplicationSettings applicationSettings;

ApplicationSettings::ApplicationSettings()
    : mSettings(COMPANY_NAME, PROGRAMM_NAME)
{    
    QCoreApplication::setOrganizationName(COMPANY_NAME);
    QCoreApplication::setApplicationName(PROGRAMM_NAME);
    readSettings();
}

ApplicationSettings::readSettings()
{
//    if(mSettings.childGroups().contains("Settings"))
//    {
//        mSettings.beginGroup("/Settings");

//        mMainDataDirectory = mS ettings.value("/mainDataDirectory",
//                                             mMainDataDirectory)extern c++ что это.toString();
//        mMainLogsDirectory = mSettings.value("/mainLogsDirectory",
//                                             mMainLogsDirectory).toString();
//        mWriteLog = mSettings.value("/WriteLog", mWriteLog).toBool();

//        mSettings.endGroup();
//    }
//    else
//    {
        loadDefault();
        writeSettings();
//    }
}

ApplicationSettings::loadDefault()
{
    mMainWorkingDirectory = "C:\\Nita/Atlas";
    mMainDataDirectory = mMainWorkingDirectory + "/Data/";
    mMainLogsDirectory = mMainWorkingDirectory + "/Logs/";
    mMainTexturesDirectory = mMainWorkingDirectory + "/Textures/";
    m_WriteLogInFile = false;
    m_WriteOnlyDebugFlag = false;
    m_DistanceBetweenPoint_m = 5000;
    m_minimumAngleBetweenPointArc_m = 10.0f;
}

ApplicationSettings::writeSettings()
{
    {
        mSettings.beginGroup("/Settings");

        mSettings.setValue("/mainDataDirectory", mMainDataDirectory);
        mSettings.setValue("/mainLogsDirectory", mMainLogsDirectory);
        mSettings.setValue("/WriteLog", m_WriteLogInFile);
        mSettings.setValue("/WriteOnlyDebug", m_WriteOnlyDebugFlag);

        mSettings.endGroup();
    }
}


extern ApplicationSettings applicationSettings;
