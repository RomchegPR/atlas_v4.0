#ifndef DATASTORAGELOADER_H
#define DATASTORAGELOADER_H

#include <QObject>
#include <QThread>

class DataStorage_test;

class DataStorageLoader :  public QThread
{
public:
    DataStorageLoader();
    void run();
    void stop();

    void setDataStorage(DataStorage_test *pDataStorage);

private:
    volatile bool stopped;
    DataStorage_test *m_pDataStorage;

};

#endif // DATASTORAGELOADER_H
