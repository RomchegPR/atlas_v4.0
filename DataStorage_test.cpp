#include "DataStorage_test.h"
#include <QDebug>
#include "MainScene/Scene.h"

DataStorage_test* DataStorage_test::instance = NULL;

DataStorage_test::~DataStorage_test()
{
    delete instance;
}

DataStorage_test* DataStorage_test::getInstance(){
    if( !instance ){
        instance = new DataStorage_test();
    }
    return instance;
}


void DataStorage_test::run()
{
    for (RenderObject* object : m_Objects){
//        object->setState(RenderObjectState::STATE_INITING);
//        object->setState(RenderObjectState::STATE_PAINT_READY);
    }

    stopped = false;
}

void DataStorage_test::stop()
{
    stopped = true;
}

QVector<RenderObject*> DataStorage_test::getAllObjects()
{
    return m_Objects;
}

void DataStorage_test::addObject(RenderObject * object)
{
//    object->setState(RenderObjectState::STATE_CREATING);
    m_Objects.push_back(object);
}

void DataStorage_test::setMainScene(Scene *pMainScene)
{
    m_pMainScene = pMainScene;
}

RenderObject* DataStorage_test::getObject(QString name)
{
    Q_UNUSED(name)
    return new RenderObject();
}
