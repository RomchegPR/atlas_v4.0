#ifndef CSVAPI_H
#define CSVAPI_H
#include <Readers/CSVReader/CSVReader.h>

class CSVApi
{

private:

    CSVApi(){}
    ~CSVApi(){}
    CSVApi( const CSVApi& ) = delete;
    CSVApi& operator=( CSVApi& ) = delete;

    QString                     getType(QString fileName);

public:

    static CSVApi&              getInstance();
    void                        readFile(QString fileName);
    void                        readFiles(QFileInfoList listOfFiles);

};

#endif // CSVAPI_H

