#include "Converter.h"

Converter::Converter()
{

}
int Converter::flToMetres(QString h)
{
    double x;
    //WillAddProcessingFields
    h = h.toUpper();
    if (!h.contains("FL") and !h.contains("M")){
        throw QString("Format is wrong");
    }
    if (h.contains("FL")){
        h.remove("FL");
        x = h.toDouble();
        if (int(x) == 0){
            throw QString("Format is wrong");
        }
        x = x * 100 / 3.2808;
        int y = int(x) % 50;
        if (y >= 25){
            x += 50 - y;
        } else {
            x -= y;
        }
    }
    if (h.contains("M")){
        h.remove("M");
        x = h.toDouble();
        if (int(x) == 0){
            throw QString("Format is wrong");
        }
    }

    return int(x);
}

double Converter::toDegrees(QString coord)
{
    QRegExp rx("(\\d+\\.?\\d+)");
    QString tmp;
    QStringList lines;
    int pos = 0;
    while ((pos = rx.indexIn(coord,pos)) != -1){
        lines << rx.cap(1);
        pos += rx.matchedLength();
    }
    tmp = lines.join("");
    double h = tmp.toDouble();
    int degrees = h / 10000;
    int minutes = (h - degrees * 10000) / 100;
    double seconds = h - degrees * 10000 - minutes * 100;
    double answer = DMS::Decode( (double) degrees,(double) minutes, (double) seconds);
    answer *= 1000000;
    answer = (answer - floor(answer) < 0.5) ? floor(answer) : ceil(answer);
    answer /= 1000000;
    if (coord.contains("ю") || coord.contains("з")){
        answer = - answer;
    }
    return answer;
}
