#include "CSVReader.h"
#include "ApplicationSettings.h"
CSVReader::CSVReader()
{
}

CSVReader::~CSVReader()
{

}

CSVData CSVReader::readFile(QString fileName)
{
    QFile file(fileName);

    CSVData csvData;

    bool readed = file.open(QIODevice::ReadOnly);
    if (!readed){
        throw QString("File doesn't exist.");
    }

    if (file.size() == 0){
        throw QString("File is empty.");
    }


    CSVTitle csvTitle(QString(file.readLine()).replace("\r\n", "").split(';'));
    csvData.setTitle(csvTitle);

    QList<CSVElement> listElems;
    CSVElement* csvElement;
    CSVTable data;
    QString lastName = "Name";
    while (!file.atEnd()) {
        QString line = file.readLine();
        CSVLine lineCSV = line.replace("\r\n", "").split(';');

        if (csvData.getType() != ""){
            QString name = lineCSV.at(csvTitle.value("Name"));
            if (!name.isEmpty() && name != lastName){
                lastName = name;
                if (data.size() != 0) {
                    csvElement = new CSVElement();
                    csvElement->setElement(data);
                    listElems.append(*csvElement);
                    data.clear();
                }
            }
        }

        data.append(lineCSV);
    }

    if (data.size() != 0) {
        csvElement = new CSVElement();
        csvElement->setElement(data);
        listElems.append(*csvElement);
        data.clear();
    }

    csvData.setElements(listElems);

    file.close();

    return csvData;
}

CSVData CSVReader::getTitle(QString fileName)
{
    QFile file(fileName);

    CSVData csvData;

    bool readed = file.open(QIODevice::ReadOnly);
    if (!readed){
        throw QString("File doesn't exist.");
    }

    if (file.size() == 0){
        throw QString("File is empty.");
    }

    CSVTitle csvTitle(QString(file.readLine()).replace("\r\n", "").split(';'));
    csvData.setTitle(csvTitle);

    file.close();

    return csvData;
}


void CSVReader::setDesignatedPointField(DesignatedPoint* designatedPoint, const QString &fieldName, QString value)
{
    if (fieldName.compare("Name") == 0){
        designatedPoint->setName(value);
    }
    if (fieldName.compare("Lat") == 0){
        value.replace(",", ".");
        CSVConverter converter;
        float y = converter.toDegrees(value);
        designatedPoint->setLatitude(y);
    }
    if (fieldName.compare("Lon") == 0){
        value.replace(",", ".");
        CSVConverter converter;
        float x = converter.toDegrees(value);
        designatedPoint->setLongitude(x);
    }
    if (fieldName.compare("Description") == 0){
        designatedPoint->setDesignator(value);
    }
}

void CSVReader::setProcedureField(Procedure *procedure, const QString &fieldName, QString &value)
{

}

void CSVReader::parseDesignatedPoints(const CSVData &data){

    DataStorage* dataStorage = DataStorage::getInstance();
    CSVTitle csvTitle = data.getTitle();

    for(CSVElement csvElem : data.getElements()){

        DesignatedPoint* designatedPoint = new DesignatedPoint();
        dataStorage->addDesignatedPoint(designatedPoint);

        for (QString key : csvTitle.getMap().keys()){

            try {
                QString value = csvElem.at(0).at(csvTitle.value(key));
                setDesignatedPointField(designatedPoint, key, value);

            } catch (QString e){
                qDebug() << e;
            }
        }
    }
}

SegmentLeg* CSVReader::makeSegmentLeg(const CSVLine &line, const CSVTitle &csvTitle){

    SegmentLegType segmentLegType = SegmentLegType::SegmentLeg;
    SegmentLeg* segmentLeg = SegmentLegFactory::makeLeg(segmentLegType);

    QString strCourse = line.at(csvTitle.value("TrueDir"));
    QString strLowerLimitAltitude = line.at(csvTitle.value("MinAltLvl"));
    strLowerLimitAltitude += line.at(csvTitle.value("MinAlt"));
    QString strUpperLimitAltitude = line.at(csvTitle.value("MaxAltLvl"));
    strUpperLimitAltitude += line.at(csvTitle.value("MaxAlt"));
    QString strSpeedLimit = line.at(csvTitle.value("TA"));
    QString codeSegmentPath = line.at(csvTitle.value("PT"));

    segmentLeg->setSegmentLegType(segmentLegType);
    segmentLeg->setCourse(strCourse.toDouble());
    segmentLeg->setLowerLimitAltitude(strLowerLimitAltitude);
    segmentLeg->setUpperLimitAltitude(strUpperLimitAltitude);
    segmentLeg->setSpeedLimit(strSpeedLimit.toDouble());

    if (!codeSegmentPath.isEmpty()){
        segmentLeg->setLegTypeARINC(CodeSegmentPath::_from_string(codeSegmentPath.toStdString().c_str()));
    }
    return segmentLeg;
}

void CSVReader::parseProcedures(const CSVData &data){

    DataStorage* dataStorage = DataStorage::getInstance();
    CSVTitle csvTitle = data.getTitle();

    QVector<SegmentLeg*> *segmentLegs;


    int lineCount = 0;
    for(CSVElement csvElem : data.getElements()){
        csvElem.removeLast();

        QString strLegType = csvElem.at(0).at(csvTitle.value("LegType"));

        Procedure* procedure = nullptr;

        if (strLegType.compare("APCH") == 0) {

//            InstrumentApproachProcedure* iap = new InstrumentApproachProcedure();
//            iap->setApproachType(CodeApproach ::OTHER); //check it
//            procedure = iap;
            procedure = new InstrumentApproachProcedure();
        } else if (strLegType.compare("SID") == 0) {

            procedure = new StandardInstrumentDeparture();

        } else if (strLegType.compare("STAR") == 0) {

            procedure = new StandardInstrumentArrival();

        } else {

            throw QString("Unknown type of procedure");
        }

        QString name = csvElem.at(0).at(csvTitle.value("Name"));


        procedure->setName(name);
        segmentLegs = new QVector<SegmentLeg*>;

        lineCount = 0;
        for (QStringList line : csvElem.getElement()){

            lineCount += 1;
            TerminalSegmentPoint* point = new TerminalSegmentPoint();
            QString nameDPoint = line.at(csvTitle.value("ID"));
            QString strFlyOver = line.at(csvTitle.value("FO"));

            bool flyOver = strFlyOver.compare("Y") == 0 ? true : false;
            point->setFlyOver(flyOver);

            for (DesignatedPoint* dPoint : dataStorage->getDesignatedPoints())
            {
                if (dPoint->getName() == nameDPoint){
                    point->setFixDesignatedPoint(dPoint);
                    break;
                }
            }

            SegmentLeg* segmentLeg = this->makeSegmentLeg(line, csvTitle);
            segmentLegs->append(segmentLeg);

            if (lineCount == 1){
                segmentLeg->setStartPoint(point);
            } else {
                segmentLeg->setEndPoint(point);
                segmentLeg = this->makeSegmentLeg(line, csvTitle);
                segmentLeg->setStartPoint(point);
                segmentLegs->append(segmentLeg);
            }
        }
        QVector<ProcedureTransition*> procedureTransitions;
        for (int i = 0; i < segmentLegs->length()/2; i++){
            ProcedureTransition* procedureTransition = new ProcedureTransition();
            QVector<SegmentLeg*> tmpSegmentLegs;
            tmpSegmentLegs.append(segmentLegs->at(2 * i));
            tmpSegmentLegs.append(segmentLegs->at(2 * i + 1));
            procedureTransition->setSegments(tmpSegmentLegs);
            procedureTransitions.append(procedureTransition);
        }
        procedure->setFlightTransition(procedureTransitions);
        dataStorage->addProcedure(procedure);
    }
}

void CSVReader::parseAirspaces(const CSVData &data){

    DataStorage* dataStorage = DataStorage::getInstance();
    CSVTitle csvTitle = data.getTitle();

    Airspace* airSpace;
    QVector<AirspaceVolume*> *airSpaceVolumes;

    for(CSVElement csvElem : data.getElements()){

        for (CSVLine line : csvElem.getElement()){

            if (!line.at(0).isEmpty()){
                airSpace = new Airspace();
                dataStorage->addAirspace(airSpace);
                QString name = line.at(csvTitle.value("Name"));
                airSpace->setName(name);
                airSpaceVolumes = new QVector<AirspaceVolume*>;
            }
            if (!line.at(1).isEmpty()){
                AirspaceVolume* airSpaceVolume = new AirspaceVolume();

                QString strLowerLimit = line.at(csvTitle.value("AltMin"));
                QString strUpperLimit = line.at(csvTitle.value("AltMax"));
                try{
                    airSpaceVolume->setLowerLimit(getAltitude(strLowerLimit));
                    airSpaceVolume->setUpperLimit(getAltitude(strUpperLimit));
                } catch(QString e){
                    qDebug() << e;
                }

                QString strPoints = line.at(csvTitle.value("Boundary"));
                QVector<CGeoPoint> coords;

                QStringList listStrPoints = strPoints.split(',');

                for (QString part : listStrPoints){
                    if (part.contains("Окружность")){
                        coords.append(getCircleCoords(part));
                    } else if (part.contains("далее")){
                        coords.append(getArcCoords(part, coords.last()));
                    } else if (part.contains("исключая границы")){
                        qDebug() << "\"Исключая границы\" не обрабатывается";
                    } else {
                        coords.append(parseCoords(part));
                    }
                }

                Surface* horizontalProjection = new Surface();
                horizontalProjection->setBasePointList(coords);

                airSpaceVolume->setHorizontalProjection(horizontalProjection);
                airSpaceVolumes->append(airSpaceVolume);
                airSpace->setVolumes(*airSpaceVolumes);
            }
        }
    }
}

QVector<CGeoPoint> CSVReader::getCircleCoords(const QString &line){

    CSVLine points = parseDigits(line);
    double radius = points.at(0).toDouble();

    CSVConverter converter;
    CGeoPoint center;
    center.SetDegLatLon(converter.toDegrees(points.at(1)),
                       converter.toDegrees(points.at(2)));

    CPolarPoint startPolar(0, radius * 1000);
    CPolarPoint endPolar(GeoMath::Deg2Rad(357), radius * 1000);

    CNavCalc& navCalc = CNavCalc::GetInstance();

    CGeoPoint startPoint(navCalc.SolveDirectTask(center, startPolar));
    CGeoPoint endPoint(navCalc.SolveDirectTask(center, endPolar));
    QVector<CGeoPoint> coords = navCalc.CalcArcPoints(center, startPoint, endPoint, true, applicationSettings.m_minimumAngleBetweenPointArc_m);

    return coords;
}

CSVLine CSVReader::parseDigits(const QString &line){
    //get only sequences of digit
    QRegExp rx("(\\d+\\.?\\d?\\S?)");
    CSVLine points;
    int pos = 0;
    while ((pos = rx.indexIn(line, pos)) != -1)
    {
        points << rx.cap(1);
        pos += rx.matchedLength();
    }
    return points;
}

QVector<CGeoPoint> CSVReader::getArcCoords(const QString &line, const CGeoPoint &startPoint){

    bool isClockwise = false;
    if (line.contains("по часовой")){
        isClockwise = true;
    }

    CSVLine points = parseDigits(line);

    CSVConverter converter;
    CGeoPoint center;
    center.SetDegLatLon(converter.toDegrees(points.at(1)),
                       converter.toDegrees(points.at(2)));

    CGeoPoint endPoint;
    endPoint.SetDegLatLon(converter.toDegrees(points.at(3)),
                       converter.toDegrees(points.at(4)));

    CNavCalc& navCalc = CNavCalc::GetInstance();
    QVector<CGeoPoint> coords = navCalc.CalcArcPoints(center, startPoint, endPoint, isClockwise, applicationSettings.m_minimumAngleBetweenPointArc_m);
    coords.remove(0);

    return coords;
}

QVector<CGeoPoint> CSVReader::parseCoords(const QString &line){

    QVector<CGeoPoint> coords;
    CSVLine points = parseDigits(line);

    for (int i = 0; i < points.length(); i += 2 )
    {
        CSVConverter converter;
        double latitude = converter.toDegrees(points.at(i));
        double longitude = converter.toDegrees(points.at(i + 1));
        CGeoPoint point;
        point.SetDegLatLon(latitude, longitude);
        coords.append(point);
    }

    return coords;
}

void CSVReader::parseVerticalStructures(const CSVData &data){

    DataStorage* dataStorage = DataStorage::getInstance();
    CSVTitle csvTitle = data.getTitle();

    VerticalStructure *obstacle;
    VerticalStructurePart *obstaclePart;
    VerticalStructurePartGeometry *horizontalProjection;

    CSVConverter converter;
    for(CSVElement csvElem : data.getElements()){

        obstacle = new VerticalStructure();
        dataStorage->addObstacle(obstacle);

        QString name = csvElem.at(0).at(csvTitle.value("Name"));
        obstacle->setName(name);

        obstaclePart = new VerticalStructurePart();
        horizontalProjection = new VerticalStructurePartGeometry();
        obstaclePart->setHorizontalProjection(horizontalProjection);

        QVector<VerticalStructurePart*> obstacleParts;
        obstacleParts.append(obstaclePart);
        obstacle->setParts(obstacleParts);

        QVector<CGeoPoint> coords;

        double elevation = 0;

        for (CSVLine line : csvElem.getElement()){

            QString strVerticalExtent = line.at(csvTitle.value("Height"));
            obstaclePart->setVerticalExtent(strVerticalExtent.toDouble());

            try{
                QString strElevation = line.at(csvTitle.value("Altitude"));
                elevation = strElevation.toDouble();
            } catch(QString e){
                qDebug() << e;
            }

            QString strLatitude = line.at(csvTitle.value("Latitude"));
            QString strLongitude = line.at(csvTitle.value("Longitude"));


            double latitude = converter.toDegrees(strLatitude);
            double longitude = converter.toDegrees(strLongitude);
            CGeoPoint point;
            point.SetDegLatitude(latitude);
            point.SetDegLongitude(longitude);
            coords.append(point);
        }

        QString strType = csvElem.at(0).at(csvTitle.value("Type"));

        if (strType.compare("одиночное") == 0){

            obstaclePart->setGeometryType(GeometryType::POINT);

            ElevatedPoint* elevatedPoint = new ElevatedPoint();
            elevatedPoint->setGpPosition(coords.at(0));
            elevatedPoint->setElevation(new ValDistanceVerticalType(elevation, UomDistanceVerticalType::M));

            horizontalProjection->setElevatedPoint(elevatedPoint);

        } else if (strType.compare("линейное") == 0){

            obstaclePart->setGeometryType(GeometryType::CURVE);

            ElevatedCurve* elevatedCurve = new ElevatedCurve();
            elevatedCurve->setBasePointList(coords);
            elevatedCurve->setElevation(new ValDistanceVerticalType(elevation, UomDistanceVerticalType::M));

            horizontalProjection->setElevatedCurve(elevatedCurve);

        } else if (strType.compare("полигон") == 0){

            obstaclePart->setGeometryType(GeometryType::SURFACE);

            ElevatedSurface* elevatedSurface = new ElevatedSurface();
            elevatedSurface->setBasePointList(coords);
            elevatedSurface->setElevation(new ValDistanceVerticalType(elevation, UomDistanceVerticalType::M));

            horizontalProjection->setElevatedSurface(elevatedSurface);

        }
    }
}

double CSVReader::getAltitude(const QString &strAltitude)
{
    //WillAddProcessingFields

    QString line = strAltitude.toUpper();

    CNavCalc& navCalc = CNavCalc::GetInstance();

    if (strAltitude.contains("GND")){
        return 0;
    }

    if (strAltitude.contains("UNL")){
        return 30000;
    }

    QRegExp rx("(\\d+)");
    CSVLine seqDigits;
    int pos = 0;
    while ((pos = rx.indexIn(line, pos)) != -1)
    {
        seqDigits << rx.cap(1);
        pos += rx.matchedLength();
    }
    double x = seqDigits.at(0).toDouble();

    if (strAltitude.contains("FL")){
        return navCalc.ConvertFlightLevel2Meters(x);
    }

    if (strAltitude.contains("FT")){
        return navCalc.ConvertFeet2Meters(x);
    }

    return x;

}
