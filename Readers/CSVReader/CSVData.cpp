#include "CSVData.h"

CSVTitle CSVData::getTitle() const
{
    return title;
}

void CSVData::setTitle(const CSVTitle &value)
{
    title = value;
}

QList<CSVElement> CSVData::getElements() const
{
    return elements;
}

void CSVData::setElements(const QList<CSVElement> &value)
{
    elements = value;
}

QString CSVData::getType()
{
    if (isPointsData()){
        return "Points";
    }

    if (isProceduresData()){
        return "Procedures";
    }

    if (isAirSpacesData()){
        return "AirSpaces";
    }

    if (isVerticalStructuresData()){
        return "VerticalStructures";
    }

    return "";
}

bool CSVData::isPointsData()
{
    return title.contains("AD");
}

bool CSVData::isProceduresData()
{
    return title.contains("LegType");
}

bool CSVData::isAirSpacesData()
{
    return title.contains("Boundary");
}

bool CSVData::isVerticalStructuresData()
{
    return title.contains("Datum");
}

CSVData::CSVData()
{

}
