#include "CSVElement.h"

QList<QStringList> CSVElement::getElement() const
{
    return element;
}

void CSVElement::setElement(const QList<QStringList> &value)
{
    element = value;
}

const QStringList CSVElement::at(int i) const
{
    return this->element.at(i);
}

void CSVElement::removeLast()
{
    this->element.removeLast();
}

CSVElement::CSVElement()
{

}

CSVElement::CSVElement(QList<QStringList> element)
{
    setElement(element);
}
