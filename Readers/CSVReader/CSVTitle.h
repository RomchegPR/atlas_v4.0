#ifndef CSVTITLE_H
#define CSVTITLE_H
#include <QStringList>
#include <QMap>

class CSVTitle
{
private:
    QMap<QString, int>          map;
    QMap<QString, int>          makeMap(QStringList title);
public:
    CSVTitle();
    CSVTitle(QStringList title);
    QMap<QString, int>          getMap() const;
    void                        setMap(const QMap<QString, int> &value);
    const int                   value(const QString &key) const;
    bool                        contains(const QString &key) const;
};

#endif // CSVTITLE_H
