#ifndef CSVCONVERTER_H
#define CSVCONVERTER_H

#include <QString>
#include <QStringList>
#include <QRegExp>
#include <GeographicLib/DMS.hpp>
#include <QtCore>
#include "DataStorage/Builder/ProcedureBuilder/navcalc.h"

using namespace GeographicLib;

class CSVConverter
{
public:
    CSVConverter();

    //int                         flToMetres(QString h);
    double                      toDegrees(QString coord);
};

#endif // CSVCONVERTER_H
