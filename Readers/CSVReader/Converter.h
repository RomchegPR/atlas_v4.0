#ifndef CONVERTER_H
#define CONVERTER_H

#include <QString>
#include <QStringList>
#include <QRegExp>
#include <GeographicLib/DMS.hpp>


using namespace GeographicLib;

class Converter
{
public:
    Converter();

    int                         flToMetres(QString h);
    double                      toDegrees(QString coord);
};

#endif // CONVERTER_H
