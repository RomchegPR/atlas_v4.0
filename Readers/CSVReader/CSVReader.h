#ifndef CSVREADER_H
#define CSVREADER_H

#include <QFile>
#include <QStringList>
#include <QDebug>
#include "DataStorage/Entities/DesignatedPoint.h"
#include "DataStorage/Entities/InstrumentApproachProcedure.h"
#include "DataStorage/Entities/StandardInstrumentArrival.h"
#include "DataStorage/Entities/StandardInstrumentDeparture.h"
#include "Readers/AIXMReader/Utils/SegmentLegFactory.h"
#include "DataStorage/DataStorage.h"
#include "CSVConverter.h"
#include "CSVData.h"
#include "DataStorage/Builder/ProcedureBuilder/navcalc.h"

class CSVReader
{

private:
    double                      getAltitude(const QString &strAltitude);
    CSVLine                     parseDigits(const QString &line);
    QVector<CGeoPoint>          parseCoords(const QString &line);
    QVector<CGeoPoint>          getCircleCoords(const QString &line);
    QVector<CGeoPoint>          getArcCoords(const QString &line, const CGeoPoint &startPoint);

    SegmentLeg*                 makeSegmentLeg(const CSVLine &line, const CSVTitle &csvTitle);
    void                        setDesignatedPointField(DesignatedPoint* designatedPoint, const QString &fieldName, QString value);
    void                        setProcedureField(Procedure* procedure, const QString &fieldName, QString &value);
    void                        setAirspaceField(Airspace* airspace, const QString &fieldName, QString &value);
    void                        setVerticalStructureField(VerticalStructure* verticalStructure, const QString &fieldName, QString &value);

public:

    CSVReader();
    ~CSVReader();

    void                        parseDesignatedPoints(const CSVData &data);
    void                        parseProcedures(const CSVData &data);
    void                        parseAirspaces(const CSVData &data);
    void                        parseVerticalStructures(const CSVData &data);
    CSVData                     readFile(QString fileName);
    CSVData                     getTitle(QString fileName);

};

#endif // CSVREADER_H
