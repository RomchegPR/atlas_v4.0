#ifndef CSVELEMENT_H
#define CSVELEMENT_H
#include <QStringList>

class CSVElement
{
private:
    QList<QStringList>          element;
public:
    CSVElement();
    CSVElement(QList<QStringList> element);
    QList<QStringList>          getElement() const;
    void                        setElement(const QList<QStringList> &value);
    const QStringList           at(int i) const;
    void                        removeLast();
};

#endif // CSVELEMENT_H
