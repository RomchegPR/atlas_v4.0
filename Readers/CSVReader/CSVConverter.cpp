#include "Readers/CSVReader/CSVConverter.h"

CSVConverter::CSVConverter()
{

}
//int CSVConverter::flToMetres(QString h)
//{
//    double x;
//    //WillAddProcessingFields
//    h = h.toUpper();

//    CNavCalc& navCalc = CNavCalc::GetInstance();

//    if (h.contains("GND")){
//        return 0;
//    }

//    if (h.contains("UNL")){
//        return 30000;
//    }

//    if (h.contains("FL")){
//        h.remove("FL");
//        x = h.toDouble();
//        if (int(x) == 0){
//            throw QString("Format is wrong");
//        }
//        x = x * 100 / 3.2808;
//        int y = int(x) % 50;
//        if (y >= 25){
//            x += 50 - y;
//        } else {
//            x -= y;
//        }
//    }

//    if (h.contains("M")){
//        h.remove("M");
//        x = h.toDouble();
//        if (int(x) == 0){
//            throw QString("Format is wrong");
//        }
//    }

//    return int(x);
//}

double CSVConverter::toDegrees(QString coord)
{
    QRegExp rx("(\\d+\\.?\\d+)");
    QString tmp;
    QStringList lines;
    int pos = 0;
    while ((pos = rx.indexIn(coord,pos)) != -1){
        lines << rx.cap(1);
        pos += rx.matchedLength();
    }
    tmp = lines.join("");
    double h = tmp.toDouble();
    int degrees = h / 10000;
    int minutes = (h - degrees * 10000) / 100;
    double seconds = h - degrees * 10000 - minutes * 100;
    double answer = DMS::Decode( (double) degrees,(double) minutes, (double) seconds);

    if ((degrees < 0 || degrees > 59) || (minutes < 0 || minutes > 59) || (seconds < 0 || seconds > 59));

    answer *= 1000000;
    answer = (answer - floor(answer) < 0.5) ? floor(answer) : ceil(answer);
    answer /= 1000000;
    if (coord.contains("ю") || coord.contains("з")){
        answer = - answer;
    }
    return answer;
}
