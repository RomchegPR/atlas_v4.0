#ifndef CSVDATA_H
#define CSVDATA_H
#include <QString>
#include <Qlist>
#include "CSVTitle.h"
#include "CSVElement.h"

#define     CSVLine             QStringList
#define     CSVTable            QList<QStringList>

class CSVData
{
private:
    CSVTitle                    title;
    QList<CSVElement>           elements;
    bool                        isPointsData();
    bool                        isProceduresData();
    bool                        isAirSpacesData();
    bool                        isVerticalStructuresData();
public:
    CSVData();
    CSVTitle                    getTitle() const;
    void                        setTitle(const CSVTitle &value);
    QList<CSVElement>           getElements() const;
    void                        setElements(const QList<CSVElement> &value);
    QString                     getType();
};

#endif // CSVDATA_H
