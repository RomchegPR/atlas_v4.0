#include "CSVTitle.h"

QMap<QString, int> CSVTitle::getMap() const
{
    return map;
}

void CSVTitle::setMap(const QMap<QString, int> &value)
{
    map = value;
}

const int CSVTitle::value(const QString &key) const
{
    if (map.contains(key)){
        return this->map.value(key);
    } else{
        throw key + " not found.";
    }
    return 1;
}

bool CSVTitle::contains(const QString &key) const
{
    return map.contains(key);
}

QMap<QString, int> CSVTitle::makeMap(QStringList title)
{
    QMap<QString, int> map;
    int i = 0;
    for (QString str : title){
        map.insert(str, i);
        i++;
    }
    return map;
}

CSVTitle::CSVTitle()
{

}

CSVTitle::CSVTitle(QStringList title)
{
    this->map = makeMap(title);
}
