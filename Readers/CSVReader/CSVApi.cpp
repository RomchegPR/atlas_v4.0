#include "CSVApi.h"
#include "PriorityQueue.h"

QString CSVApi::getType(QString fileName)
{
    CSVReader* csvReader = new CSVReader();
    CSVData data = csvReader->getTitle(fileName);
    QString type;
    try{
        type = data.getType();
    } catch (int e){
        throw "Wrong type";
    }
    return type;
}

CSVApi& CSVApi::getInstance(){
    static CSVApi ms_instance;
    return ms_instance;
}

void CSVApi::readFile(QString fileName)
{
    CSVReader* csvReader = new CSVReader();

    QString type;
    try{
        type = getType(fileName);
    } catch (int e){
        throw "Wrong type";
    }

    CSVData data = csvReader->readFile(fileName);
    DataStorage* dataStorage = DataStorage::getInstance();

    if (type.compare("Points") == 0){
        csvReader->parseDesignatedPoints(data);
    }
    if (type.compare("Procedures") == 0){
        csvReader->parseProcedures(data);
    }
    if (type.compare("VerticalStructures") == 0){
        csvReader->parseVerticalStructures(data);
        for (VerticalStructure *obstacle : dataStorage->getObstacles()){
            for (VerticalStructurePart *part : obstacle->getParts()){
                if (part->getGeometryType() == GeometryType::SURFACE){
                    QVector<CGeoPoint> coords = part->getHorizontalProjection()->getElevatedSurface()->getBasePointList();
                    if (coords.at(0) != coords.last()){
                        coords.append(coords.at(0));
                    }
                    VerticalStructurePartGeometry* horizontalProjection = new VerticalStructurePartGeometry();
                    ElevatedSurface* elevatedSurface = new ElevatedSurface();
                    elevatedSurface->setBasePointList(coords);
                    horizontalProjection->setElevatedSurface(elevatedSurface);
                    part->setHorizontalProjection(horizontalProjection);
                }
            }
        }

    }
    if (type.compare("AirSpaces") == 0){
        csvReader->parseAirspaces(data);

        for (Airspace *airspace : dataStorage->getAirspaces()){
            for (AirspaceVolume *volume : airspace->getVolumes()){
                QVector<CGeoPoint> coords = volume->getHorizontalProjection()->getBasePointList();
                if (coords.at(0) != coords.last()){
                    coords.append(coords.at(0));
                }
                Surface* horizontalProjection = new Surface();
                horizontalProjection->setBasePointList(coords);
                volume->setHorizontalProjection(horizontalProjection);
            }
        }

    }
}

void CSVApi::readFiles(QFileInfoList listOfFiles){

    PriorityQueue<QString> queue;

    for (QFileInfo fileInfo : listOfFiles){
        QString fileName = fileInfo.filePath();
        if (getType(fileName).compare("Points") == 0){
            queue.enqueue(Priority::High, fileName);
        } else {
            queue.enqueue(Priority::Normal, fileName);
        }
    }

    while (queue.count() > 0){
        readFile(queue.dequeue());
    }
}
