#ifndef AIXMREADER_H
#define AIXMREADER_H

#include <QString>
#include <QDomDocument>

// Читатель AIXM'a
// Необходим для начальной загрузки файла и формирование DOM документа из него, который далее передается в класс AIXMData

class AIXMReader
{
private:
    QDomDocument mDocument;
public:
    AIXMReader();
    AIXMReader(QString filename);
    virtual ~AIXMReader();
    QDomDocument& getDocument();
    void setData(const QString& filename);
};

#endif // AIXMREADER_H
