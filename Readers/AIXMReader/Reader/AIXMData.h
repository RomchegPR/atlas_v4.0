#ifndef AIXMDATA_H
#define AIXMDATA_H

#include <QDomDocument>
#include <QString>
#include <QVector>
#include "DataStorage/Entities/Procedure.h"
#include "Readers/AIXMReader//AIXMNames.h"

/*
    Класс, предназначенный для начальной загрузки нодов из Aixm.

*/

class AIXMData
{   
public:
    // Указатель на документ
    QDomDocument* mDocument;

    // Вектор нод процедур
    QVector<QDomNode> mProcedures;

    // Вектор нод точек
    QVector<QDomNode> mDesignatedPoints;

    // Вектор нод сегментов
    QVector<QDomNode> mSegmentLegs;

    // Вектор нод ВПП
    QVector<QDomNode> mRunways;

    // Вектор нод воздушного пространтсва
    QVector<QDomNode> mAirspaces;

    // Вектор нод препятствий
    QVector<QDomNode> mObstacles;

public:
    AIXMData();
    AIXMData(AIXMData* aixm);
    AIXMData(QDomDocument& document);

    // Получение документа
    QDomDocument* getDocument();
    virtual ~AIXMData();

    // Получение корня дерева в виде элемента
    QDomElement getRoot();

    // Метод загрзуки нод
    void loadNodes(QString nodeName = AIXM_HAS_MEMBER);

    // Неиспользуемые ноды
    QDomNode getValue(QDomNode* node,QString nodeName);
    void setValue(QDomNode* node, QString nodeName, QString value);

private:
    void loadDesignatedPoints();
    void loadProcedures();
};

#endif // AIXMDATA_H
