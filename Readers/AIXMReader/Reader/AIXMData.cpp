#include "AIXMData.h"
#include <QString>
#include "Readers/AIXMReader/AIXMNames.h"
#include <QDebug>

AIXMData::AIXMData()
{
}

AIXMData::AIXMData(QDomDocument& document) :
    mDocument(&document)
{
}

AIXMData::AIXMData(AIXMData* aixm) :
    mDocument(aixm->getDocument())
{
}

QDomElement AIXMData::getRoot()
{
    return mDocument->documentElement();
}

QDomDocument* AIXMData::getDocument(){
    return mDocument;
}

void AIXMData::loadNodes(QString nodeName){
    QDomNodeList nList = mDocument->elementsByTagName(nodeName);
    for(int i = 0; i < nList.size(); i++){
        QDomNode node = nList.at(i).firstChild();
        QString nodeName = node.localName();
        if(nodeName == AIXM_APPROACH || nodeName == AIXM_SID || nodeName == AIXM_STAR){
            mProcedures.append(node);
        } else if (nodeName.contains("Leg")){
            //qDebug() << nodeName;
            mSegmentLegs.append(node);
        } else if (nodeName == AIXM_RUNWAY_A){
            mRunways.append(node);
        } else if (nodeName == AIXM_DESIGNATED_POINT){
            mDesignatedPoints.append(node);
        } else if (nodeName == AIXM_AIRSPACE){
            mAirspaces.append(node);
        } else if (nodeName == AIXM_VERTICAL_STRUCTURE){
            mObstacles.append(node);
        }
    }
    //qDebug() << nList.size();
}

AIXMData::~AIXMData()
{
    mDocument = NULL;
}
