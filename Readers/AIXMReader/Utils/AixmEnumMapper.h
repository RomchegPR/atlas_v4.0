#ifndef AIXMENUMMAPPER_H
#define AIXMENUMMAPPER_H

#include "DataStorage/DataTypes/CodeDesignatedPoint.h"
#include "DataStorage/DataTypes/SegmentLegType.h"
#include "DataStorage/DataTypes/CodeSegmentTermination.h"
#include "DataStorage/DataTypes/CodeTrajectory.h"
#include "DataStorage/DataTypes/CodeSegmentPath.h"
#include "DataStorage/DataTypes/CodeCourse.h"
#include "DataStorage/DataTypes/CodeDirectionReference.h"
#include "DataStorage/DataTypes/CodeDirectionTurn.h"
#include "DataStorage/DataTypes/CodeSpeedReference.h"
#include "DataStorage/DataTypes/CodeAltitudeUse.h"
#include "DataStorage/DataTypes/CodeVerticalReference.h"
#include "DataStorage/DataTypes/CodeProcedureFixRole.h"
#include "DataStorage/DataTypes/CodeATCReporting.h"
#include "DataStorage/DataTypes/CodeBearing.h"
#include "DataStorage/DataTypes/CodeCardinalDirection.h"
#include "DataStorage/DataTypes/CodeAircraft.h"
#include "DataStorage/DataTypes/CodeAircraftEngine.h"
#include "DataStorage/DataTypes/CodeAircraftEngineNumber.h"
#include "DataStorage/DataTypes/CodeAircraftCategory.h"
#include "DataStorage/DataTypes/CodeValueInterpretation.h"
#include "DataStorage/DataTypes/CodeAircraftWingspanClass.h"
#include "DataStorage/DataTypes/CodeWakeTurbulence.h"
#include "DataStorage/DataTypes/CodeNavigationEquipment.h"
#include "DataStorage/DataTypes/CodeNavigationSpecification.h"
#include "DataStorage/DataTypes/CodeEquipmentAntiCollision.h"
#include "DataStorage/DataTypes/CodeCommunicationMode.h"
#include "DataStorage/DataTypes/CodeTransponder.h"
#include "DataStorage/DataTypes/CodeApproachPrefix.h"
#include "DataStorage/DataTypes/CodeApproach.h"
#include "DataStorage/DataTypes/CodeApproachEquipmentAdditional.h"
#include "DataStorage/DataTypes/CodeFinalGuidance.h"
#include "DataStorage/DataTypes/CodeApproachGuidance.h"
#include "DataStorage/DataTypes/CodeSide.h"
#include "DataStorage/DataTypes/CodeRelativePosition.h"
#include "DataStorage/DataTypes/CodeMissedApproach.h"
#include <QString>

// Уже ненужный класс

class AixmEnumMapper
{
public:
    AixmEnumMapper();

    static CodeMissedApproach toCodeMissedApproach(QString value);
    static QString toString(CodeMissedApproach value);

    static CodeRelativePosition toCodeRelativePosition(QString value);
    static QString toString(CodeRelativePosition value);

    static CodeSide toCodeSide(QString value);
    static QString toString(CodeSide value);

    static CodeApproachGuidance toCodeApproachGuidance(QString value);
    static QString toString(CodeApproachGuidance value);

    static CodeFinalGuidance toCodeFinalGuidance(QString value);
    static QString toString(CodeFinalGuidance value);

    static CodeApproachEquipmentAdditional toCodeApproachEquipmentAdditional(QString value);
    static QString toString(CodeApproachEquipmentAdditional value);

    static CodeApproach toCodeApproach(QString value);
    static QString toString(CodeApproach value);

    static CodeApproachPrefix toCodeApproachPrefix(QString value);
    static QString toString(CodeApproachPrefix value);

    static CodeEquipmentAntiCollision toCodeEquipmentAntiCollision(QString value);
    static QString toString(CodeEquipmentAntiCollision value);

    static CodeCommunicationMode toCodeCommunicationMode(QString value);
    static QString toString(CodeCommunicationMode value);

    static CodeTransponder toCodeTransponder(QString value);
    static QString toString(CodeTransponder value);

    static CodeWakeTurbulence toCodeWakeTurbulence(QString value);
    static QString toString(CodeWakeTurbulence value);

    static CodeNavigationEquipment toCodeNavigationEquipment(QString value);
    static QString toString(CodeNavigationEquipment value);

    static CodeNavigationSpecification toCodeNavigationSpecification(QString value);
    static QString toString(CodeNavigationSpecification value);

    static CodeDesignatedPoint toCodeDesignatedPoint(QString value);
    static QString toString(CodeDesignatedPoint value);

    static SegmentLegType toSegmentLegType(QString value);
    static QString toString(SegmentLegType value);

    static CodeSegmentTermination toCodeSegmentTermination(QString value);
    static QString toString(CodeSegmentTermination value);

    static CodeTrajectory toCodeTrajectory(QString value);
    static QString toString(CodeTrajectory value);

    static CodeSegmentPath toCodeSegmentPath(QString value);
    static QString toString(CodeSegmentPath value);

    static CodeCourse toCodeCourse(QString value);
    static QString toString(CodeCourse value);

    static CodeDirectionReference toCodeDirectionReference(QString value);
    static QString toString(CodeDirectionReference value);

    static CodeDirectionTurn toCodeDirectionTurn(QString value);
    static QString toString(CodeDirectionTurn value);

    static CodeSpeedReference toCodeSpeedReference(QString value);
    static QString toString(CodeSpeedReference value);

    static CodeAltitudeUse toCodeAltitudeUse(QString value);
    static QString toString(CodeAltitudeUse value);

    static CodeVerticalReference toCodeVerticalReference(QString value);
    static QString toString(CodeVerticalReference value);

    static CodeProcedureFixRole toCodeProcedureFixRole(QString value);
    static QString toString(CodeProcedureFixRole value);

    static CodeATCReporting toCodeATCReporting(QString value);
    static QString toString(CodeATCReporting value);

    static CodeBearing toCodeBearing(QString value);
    static QString toString(CodeBearing value);

    static CodeCardinalDirection toCodeCardinalDirection(QString value);
    static QString toString(CodeCardinalDirection value);

    static CodeAircraft toCodeAircraft(QString value);
    static QString toString(CodeAircraft value);

    static CodeAircraftEngine toCodeAircraftEngine(QString value);
    static QString toString(CodeAircraftEngine value);

    static CodeAircraftEngineNumber toCodeAircraftEngineNumber(QString value);
    static QString toString(CodeAircraftEngineNumber value);

    static CodeAircraftCategory toCodeAircraftCategory(QString value);
    static QString toString(CodeAircraftCategory value);

    static CodeValueInterpretation toCodeValueInterpretation(QString value);
    static QString toString(CodeValueInterpretation value);

    static CodeAircraftWingspanClass toCodeAircraftWingspanClass(QString value);
    static QString toString(CodeAircraftWingspanClass value);
};

#endif // AIXMENUMMAPPER_H
