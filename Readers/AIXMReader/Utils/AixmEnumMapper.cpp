#include "AixmEnumMapper.h"
#include "Readers/AIXMReader/AIXMNames.h"

/*AixmEnumMapper::AixmEnumMapper()
{

}

CodeMissedApproach AixmEnumMapper::toCodeMissedApproach(QString value){
    CodeMissedApproach enumValue = CodeMissedApproach::OTHER;
    if(value == "PRIMARY"){
        enumValue = CodeMissedApproach::PRIMARY;
    } else if (value == "SECONDARY"){
        enumValue = CodeMissedApproach::SECONDARY;
    } else if (value == "ALTERNATE"){
        enumValue = CodeMissedApproach::ALTERNATE;
    } else if (value == "TACAN"){
        enumValue = CodeMissedApproach::TACAN;
    } else if (value == "TACANALT"){
        enumValue = CodeMissedApproach::TACANALT;
    } else if (value == "ENGINEOUT"){
        enumValue = CodeMissedApproach::ENGINEOUT;
    }
    return enumValue;
}

QString AixmEnumMapper::toString(CodeMissedApproach value){
    QString strValue = "OTHER";
    if(value == CodeMissedApproach::PRIMARY){
        strValue = "PRIMARY";
    } else if (value == CodeMissedApproach::SECONDARY){
        strValue = "SECONDARY";
    } else if (value == CodeMissedApproach::ALTERNATE){
        strValue = "ALTERNATE";
    } else if (value == CodeMissedApproach::TACAN){
        strValue = "TACAN";
    } else if (value == CodeMissedApproach::TACANALT){
        strValue = "TACANALT";
    } else if (value == CodeMissedApproach::ENGINEOUT) {
        strValue = "ENGINEOUT";
    }
    return strValue;
}

CodeDesignatedPoint AixmEnumMapper::toCodeDesignatedPoint(QString value){
    CodeDesignatedPoint enumValue = CodeDesignatedPoint::NONE;
    if(value == "DESIGNED"){
        enumValue = CodeDesignatedPoint::DESIGNED;
    } else if (value == "BRG_DIST"){
        enumValue = CodeDesignatedPoint::BRG_DIST;
    } else if (value == "CNF"){
        enumValue = CodeDesignatedPoint::CNF;
    } else if (value == "COORD"){
        enumValue = CodeDesignatedPoint::COORD;
    } else if (value == "ICAO"){
        enumValue = CodeDesignatedPoint::ICAO;
    } else if (value == "MTR"){
        enumValue = CodeDesignatedPoint::MTR;
    } else if (value == "TERMINAL"){
        enumValue = CodeDesignatedPoint::TERMINAL;
    } else if (value == "OTHER"){
        enumValue = CodeDesignatedPoint::OTHER;
    }
    return enumValue;
}

QString AixmEnumMapper::toString(CodeDesignatedPoint value){
    QString strValue = "";
    if(value == CodeDesignatedPoint::DESIGNED){
        strValue = "DESIGNED";
    } else if (value == CodeDesignatedPoint::BRG_DIST){
        strValue = "BRG_DIST";
    } else if (value == CodeDesignatedPoint::CNF){
        strValue = "CNF";
    } else if (value == CodeDesignatedPoint::COORD){
        strValue = "COORD";
    } else if (value == CodeDesignatedPoint::ICAO){
        strValue = "ICAO";
    } else if (value == CodeDesignatedPoint::MTR){
        strValue = "MTR";
    } else if (value == CodeDesignatedPoint::TERMINAL){
        strValue = "TERMINAL";
    } else if (value == CodeDesignatedPoint::OTHER){
        strValue = "OTHER";
    }
    return strValue;
}

SegmentLegType AixmEnumMapper::toSegmentLegType(QString value){
    SegmentLegType enumValue = SegmentLegType::SEGMENT_LEG;
    if (value == "ApproachLeg") {
        enumValue = SegmentLegType::APPROACH_LEG;
    } else if (value == "ArrivalFeederLeg"){
        enumValue = SegmentLegType::ARRIVAL_FEEDER_LEG;
    } else if (value == "DepartureLeg") {
        enumValue = SegmentLegType::DEPARTURE_LEG;
    } else if (value == "FinalLeg") {
        enumValue = SegmentLegType::FINAL_LEG;
    } else if (value == "InitalLeg") {
        enumValue = SegmentLegType::INITIAL_LEG;
    } else if (value == "IntermediateLeg") {
        enumValue = SegmentLegType::INTERMEDIATE_LEG;
    } else if (value == "MissedApproachLeg") {
        enumValue = SegmentLegType::MISSED_APPROACH_LEG;
    }
    return enumValue;
}

QString AixmEnumMapper::toString(SegmentLegType value){
    QString strValue = "SegmentLeg";
    if (value == SegmentLegType::APPROACH_LEG) {
        strValue = "ApproachLeg";
    } else if (value == SegmentLegType::ARRIVAL_FEEDER_LEG){
        strValue = "ArrivalFeederLeg";
    } else if (value == SegmentLegType::DEPARTURE_LEG) {
        strValue = "DepartureLeg";
    } else if (value == SegmentLegType::FINAL_LEG) {
        strValue = "FinalLeg";
    } else if (value == SegmentLegType::INITIAL_LEG) {
        strValue = "InitalLeg";
    } else if (value == SegmentLegType::INTERMEDIATE_LEG) {
        strValue = "IntermediateLeg";
    } else if (value == SegmentLegType::MISSED_APPROACH_LEG) {
        strValue = "MissedApproachLeg";
    }
    return strValue;
}

CodeSegmentTermination AixmEnumMapper::toCodeSegmentTermination(QString value){
    CodeSegmentTermination enumValue = CodeSegmentTermination::NONE;
    if(value == "ALTITUDE"){
        enumValue = CodeSegmentTermination::ALTITUDE;
    } else if(value == "DISTANCE"){
        enumValue = CodeSegmentTermination::DISTANCE;
    } else if(value == "DURATION"){
        enumValue = CodeSegmentTermination::DURATION;
    } else if(value == "INTERCEPT"){
        enumValue = CodeSegmentTermination::INTERCEPT;
    } else if(value == "OTHER"){
        enumValue = CodeSegmentTermination::OTHER;
    }
    return enumValue;
}

QString AixmEnumMapper::toString(CodeSegmentTermination value){
    QString strValue = "";
    if(value == CodeSegmentTermination::ALTITUDE){
        strValue = "ALTITUDE";
    } else if(value == CodeSegmentTermination::DISTANCE){
        strValue = "DISTANCE";
    } else if(value == CodeSegmentTermination::DURATION){
        strValue = "DURATION";
    } else if(value == CodeSegmentTermination::INTERCEPT){
        strValue = "INTERCEPT";
    } else if(value == CodeSegmentTermination::OTHER){
        strValue = "OTHER";
    }
    return strValue;
}

CodeTrajectory AixmEnumMapper::toCodeTrajectory(QString value){

}

QString AixmEnumMapper::toString(CodeTrajectory value){

}

CodeSegmentPath AixmEnumMapper::toCodeSegmentPath(QString value){

}

QString AixmEnumMapper::toString(CodeSegmentPath value){

}*/
