#include "SegmentLegFactory.h"
#include <DataStorage/Entities/SegmentLeg.h>
#include <DataStorage/Entities/ApproachLeg.h>
#include <DataStorage/Entities/ArrivalFeederLeg.h>
#include <DataStorage/Entities/ArrivalLeg.h>
#include <DataStorage/Entities/DepartureLeg.h>
#include <DataStorage/Entities/FinalLeg.h>
#include <DataStorage/Entities/InitialLeg.h>
#include <DataStorage/Entities/IntermediateLeg.h>
#include <DataStorage/Entities/MissedApproachLeg.h>


SegmentLegFactory::SegmentLegFactory()
{

}

SegmentLeg* SegmentLegFactory::makeLeg(SegmentLegType type){
    SegmentLeg* ptr = NULL;
    if(type == SegmentLegType::ApproachLeg) {
        ptr = new ApproachLeg();
    } else if (type == SegmentLegType::ArrivalFeederLeg) {
        ptr = new ArrivalFeederLeg();
    } else if (type == SegmentLegType::DepartureLeg) {
        ptr = new DepartureLeg();
    } else if (type == SegmentLegType::FinalLeg) {
        ptr = new FinalLeg();
    } else if (type == SegmentLegType::InitialLeg) {
        ptr = new InitialLeg();
    } else if (type == SegmentLegType::IntermediateLeg) {
        ptr = new IntermediateLeg();
    } else if (type == SegmentLegType::MissedApproachLeg) {
        ptr = new MissedApproachLeg();
    } else if (type == SegmentLegType::SegmentLeg) {
        ptr = new SegmentLeg();
    } else if (type == SegmentLegType::ArrivalLeg) {
        ptr = new ArrivalLeg();
    }
    return ptr;
}
