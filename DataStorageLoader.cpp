#include "DataStorageLoader.h"
#include "DataStorage_test.h"
#include <qDebug>

DataStorageLoader::DataStorageLoader()
{
    stopped = false;
}

void DataStorageLoader::run()
{
    for (int i = 0; i < 1; i++){

        RenderObject* object  = new RenderObject();
        object->setState(RenderObjectState::STATE_CREATING);

        this->sleep(1.5);

        m_pDataStorage->addObject(object);
    }

    stopped = false;
}

void DataStorageLoader::stop()
{
    stopped = true;
}

void DataStorageLoader::setDataStorage(DataStorage_test *pDataStorage)
{
    m_pDataStorage = pDataStorage;
}
